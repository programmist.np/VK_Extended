//
//  GlobalConstants.swift
//  VK Tosters
//
//  Created by programmist_np on 05.04.2020.
//  Copyright © 2020 programmist_np. All rights reserved.
//

import Foundation
import UIKit

struct GlobalConstants {
    public static var statusBarHeight: CGFloat {
        let app = UIApplication.shared
        let statusBarHeight: CGFloat = app.statusBarFrame.size.height
        return statusBarHeight
    }
    
    public static var toolbarHeight: CGFloat {
        let toolbarHeight: CGFloat = 52
        return toolbarHeight
    }
    static let transform = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: 0)
    
    public static let kNotificationSendDraftyMessage = "SendDraftyMessage"
    public static let kNotificationSendAttachment = "SendAttachment"
}

public enum Constants {
    static let cardInsets = UIEdgeInsets(top: 4, left: 12, bottom: 4, right: 12)
    static let cardOffset = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8)
    static let postLabelInsets = UIEdgeInsets(top: 10 + Constants.topViewHeight + 8, left: 12, bottom: 12, right: 12)
    static let postLabelRepostInsets = UIEdgeInsets(top: 10 + Constants.topViewHeight + Constants.topRepostViewHeight + 16, left: 12, bottom: 12, right: 12)
    static let postLabelFont = GoogleSansFont.regular(with: 14)
    static let topViewHeight: CGFloat = 48
    static let topRepostViewHeight: CGFloat = 40
    
    static let bottomViewHeight: CGFloat = 50
    
    static let bottomViewViewHeight: CGFloat = 44
    static let bottomViewViewWidth: CGFloat = 80
    static let bottomViewViewsIconSize: CGFloat = 24
    
    static let minifiedPostLimitLines: CGFloat = 8
    static let minifiedPostLines: CGFloat = 6
    
    static let moreTextButtonInsets = UIEdgeInsets(top: 2, left: 12, bottom: 0, right: 12)
    static let moreTextButtonSize = CGSize(width: 170, height: 30)
    /// Size of the avatar in the nav bar in small state.
    static let kNavBarAvatarSmallState: CGFloat = 32

    /// Size of the avatar in group topics.
    static let kAvatarSize: CGFloat = 32

    static let kProgressCircleSize: CGFloat = 40

    // Size of delivery marker (checkmarks etc)
    static let kDeliveryMarkerSize: CGFloat = 8
    // Horizontal space between delivery marker and the edge of the message bubble
    static let kDeliveryMarkerPadding: CGFloat = 0
    // Horizontal space between delivery marker and timestamp
    static let kTimestampPadding: CGFloat = 0
    // Approximate width of the timestamp
    static let kTimestampWidth: CGFloat = 50
    // Progress bar paddings.
    static let kProgressBarLeftPadding: CGFloat = 10
    static let kProgressBarRightPadding: CGFloat = 25

    // Color of "read" delivery marker.
    static let kDeliveryMarkerTint = UIColor(red: 19/255, green: 144/255, blue:255/255, alpha: 0.8)
    // Color of all other markers.
    static let kDeliveryMarkerColor = UIColor.gray.withAlphaComponent(0.7)

    // Light/dark gray color: outgoing messages
    static var kOutgoingBubbleColor: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return .color(from: 0x454647)
                } else {
                    return .color(from: 0xCCE4FF)
                }
            }
        } else {
            return .color(from: 0xCCE4FF)
        }
    }
    static var kTextColor: UIColor {
        return .adaptableBlack
    }
    
    static var kRemovedTextColor: UIColor {
        return .adaptableRed
    }
    // Bright/dark green color
    static var kIncomingBubbleColor: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return .color(from: 0x2C2D2E)
                } else {
                    return .color(from: 0xEBEDF0)
                }
            }
        } else {
            return .color(from: 0xEBEDF0)
        }
    }
    // Meta-messages, such as "Content deleted".
    static let kDeletedMessageBubbleColorLight = UIColor.color(from: 0xe3f2fd)
    static let kDeletedMessageBubbleColorDark = UIColor.color(from: 0x263238)
    static let kDeletedMessageTextColor = UIColor.gray

    static let kContentFont = UIFont.preferredFont(forTextStyle: .body)

    static let kSenderNameFont = UIFont.preferredFont(forTextStyle: .caption2)
    static let kTimestampFont = GoogleSansFont.regular(with: 10)
    static let kSenderNameLabelHeight: CGFloat = 0
    static let kNewDateFont = GoogleSansFont.medium(with: 13)
    static let kNewDateLabelHeight: CGFloat = 48
    static let kNewMessagesLabelHeight: CGFloat = 48
    // Vertical spacing between messages from the same user
    static let kVerticalCellSpacing: CGFloat = 4
    // Additional vertical spacing between messages from different users in P2P topics.
    static let kAdditionalP2PVerticalCellSpacing: CGFloat = 4
    static let kMinimumCellWidth: CGFloat = 36
    static let kMinimumCellHeight: CGFloat = 36
    // This is the space between the other side of the message and the edge of screen.
    // I.e. for incoming messages the space between the message and the *right* edge, for
    // outfoing between the message and the left edge.
    static let kFarSideHorizontalSpacing: CGFloat = 56

    // Insets around collection view, i.e. main view padding
    static let kCollectionViewInset = UIEdgeInsets(top: 52, left: 4, bottom: 8, right: 2)

    // Insets for the message bubble relative to collectionView: bubble should not touch the sides of the screen.
    static let kIncomingContainerPadding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: percent(with: UIScreen.main.bounds.width, from: 8))
    static let kOutgoingContainerPadding = UIEdgeInsets(top: 0, left: percent(with: UIScreen.main.bounds.width, from: 20), bottom: 0, right: 0)

    // Insets around content inside the message bubble.
    static let kIncomingMessageContentInset = UIEdgeInsets(top: 7, left: 12, bottom: 7, right: 12)
    static let kOutgoingMessageContentInset = UIEdgeInsets(top: 7, left: 12, bottom: 7, right: 12)
    static let kDeletedMessageContentInset = UIEdgeInsets(top: 7, left: 12, bottom: 7, right: 12)

    // Carve out for timestamp and delivery marker in the bottom-right corner.
    static let kIncomingMetadataCarveout = ""
    static let kOutgoingMetadataCarveout = ""
    static let kTimeStampCarveout = "        "
}
func percent(with value: CGFloat, from percent: CGFloat) -> CGFloat {
    let val = value * percent
    return val / 100.0
}
