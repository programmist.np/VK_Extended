//
//  VKConstants.swift
//  VK Tosters
//
//  Created by programmist_np on 20/01/2020.
//  Copyright © 2020 programmist_np. All rights reserved.
//

import Foundation

class VKConstants {
    static let shared = VKConstants()
    static let appId = "7288225"
    static let forbiddenUsers: [Int] = []
    static let verifyProfilesInt: [Int] = [565872157]
    static let verifyProfilesNames: [String] = ["#extended_beta_test", "#extended_offtop"]
    static let testingProfiles: [Int] = [340913362, 386246393, 93266796, 228892827]
    static let userFields: String = "counters,photo_id,verified,sex,bdate,city,country,home_town,has_photo,photo_50,photo_100,photo_200_orig,photo_200,photo_400_orig,photo_max,photo_max_orig,domain,has_mobile,contacts,site,education,universities,status,last_seen,followers_count,common_count,occupation,nickname,relatives,relation,personal,connections,exports,activities,interests,music,movies,tv,books,games,about,quotes,can_post,can_see_all_posts,can_see_audio,can_write_private_message,can_send_friend_request,is_favorite,is_hidden_from_feed,timezone,screen_name,maiden_name,crop_photo,is_friend,friend_status,career,military,blacklisted,blacklisted_by_me,can_be_invited_group,status_id,online_info"
    static let pushSettings = """
    {"msg":["on"], "chat":["on"], "friend":["on"], "reply":["on"], "mention":["on"], "story_answered":["on"], "tag_photo":["on"], "birthday":["on"], "content_achievements":["on"], "app_request":["on"], "money":["on"], "comment_commented":["on"], "like":["on"], "vk_apps_open_url":["on"], "friend_accepted":["on"], "interest_post":["on"], "wall_publish":["on"], "sdk_open":["on"], "private_group_post":["on"], "group_invite":["on"], "story_reply":["on"], "podcasts":["on"], "comment":["on"], "wall_post":["on"], "repost":["on"], "missed_call":["on"], "friend_found":["on"], "call":["on"], "group_accepted":["on"], "chat_mention":["on"], "story_asked":["on"], "event_soon":["on"], "live":["on"], "reminder":["on"], "gift":["on"], "new_post":["on"], "associated_events":["on"]}
    """
    static var longPollData: LongPollData?
    static let outMessageFlags: [Int] = [3, 7, 19, 23, 35, 39, 51, 55, 2105395]
    static let inMessageFlags: [Int] = [1, 5, 17, 21, 33, 37, 49, 53, 8227, 65178, 532481, 2629633, 532497]
    static let removeMessageFlags: [Int] = [128, 131072, 131200]
    static let currentUserId: Int = UserDefaults.standard.integer(forKey: "userId")
    static let checkUserId: Int = 93266796
    
    class func verifyingProfile(from userId: Int) -> Bool {
        guard (self.verifyProfilesInt.filter { $0 == userId }.first != nil) else { return false }
        return true
    }
    
    class func verifyingChat(from chatName: String) -> Bool {
        guard (self.verifyProfilesNames.filter { $0 == chatName }.first != nil) else { return false }
        return true
    }
    
    class func testingProfile(from userId: Int) -> Bool {
        guard (self.testingProfiles.filter { $0 == userId }.first != nil) else { return false }
        return true
    }
}
