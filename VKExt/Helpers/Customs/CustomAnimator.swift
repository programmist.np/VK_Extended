//
//  CustomAnimator.swift
//  VKExt
//
//  Created by programmist_NA on 20.07.2020.
//

import Foundation
import UIKit

class Interactive: UIPercentDrivenInteractiveTransition {
    lazy var panGesture: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action:  #selector(self.handlePan(gesture:)))
    var containerView: UIView!
    var dismissedVc: UIViewController! = nil {
        didSet {
            dismissedVc.view.addGestureRecognizer(panGesture)
            containerView = dismissedVc.view
        }
    }
    var isInteracting = false
    
    override init() {
        super.init()
        
    }
    
    @objc func handlePan(gesture: UIPanGestureRecognizer) {
        
        func finishOrCancel() {
            let translation = gesture.translation(in: containerView)
            let percent = translation.x / containerView.bounds.width
            let velocityX = gesture.velocity(in: containerView).x
            let isFinished: Bool
            if velocityX <= 0 {
                isFinished = false
            } else if velocityX > 100 {
                isFinished = true
            } else if percent > 0.3 {
                isFinished = true
            } else {
                isFinished = false
            }
            
            isFinished ? finish() : cancel()
        }
        
        switch gesture.state {

            case .began:
                isInteracting = true
                // dimiss
                dismissedVc.dismiss(animated: true, completion: nil)
            case .changed:
                if isInteracting {// 开始执行交互动画的时候才设置为非nil
                    let translation = gesture.translation(in: containerView)
                    var percent = translation.x / containerView.bounds.width
                    if percent < 0 {
                        percent = 0
                    }
                    update(percent)
                    
                }
            case .cancelled:
                if isInteracting {
                    finishOrCancel()
                    isInteracting = false
                    
                }
            case .ended:
                if isInteracting {
                    finishOrCancel()
                    isInteracting = false
                    
                }
            default:
                break
            
        }
    }
}

class CustomDelegate: NSObject, UIViewControllerTransitioningDelegate {
    private lazy var customAnimator = CustomAnimator()
    private lazy var interactive = Interactive()

    func animationController(forPresentedController presented: UIViewController, presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        interactive.dismissedVc = presented
        return customAnimator
    }

    func animationController(forDismissedController dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return customAnimator
    }

    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactive.isInteracting ? interactive : nil
    }
}

class CustomAnimator:NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration = 0.35
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromVc = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toVc = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let containView = transitionContext.containerView
        
        let toView: UIView
        let fromView: UIView
        
        if transitionContext.responds(to:NSSelectorFromString("viewForKey:")) {
            toView = transitionContext.view(forKey: UITransitionContextViewKey.to)!
            fromView = transitionContext.view(forKey: UITransitionContextViewKey.from)!
        } else {
            toView = toVc.view
            fromView = fromVc.view
        }
        containView.addSubview(toView)
        
        let visibleFrame = transitionContext.initialFrame(for: fromVc)
        let rightHiddenFrame = CGRect(origin: CGPoint(x: visibleFrame.width, y: visibleFrame.origin.y) , size: visibleFrame.size)
        let leftHiddenFrame = CGRect(origin: CGPoint(x: -visibleFrame.width, y: visibleFrame.origin.y) , size: visibleFrame.size)
        
        let isPresenting = toVc.presentingViewController == fromVc
        
        if isPresenting {
            toView.frame = rightHiddenFrame
            fromView.frame = visibleFrame
        } else {
            fromView.frame = visibleFrame
            toView.frame = leftHiddenFrame
        }
        

        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveLinear], animations: {
            if isPresenting {
                toView.frame = visibleFrame
                fromView.frame = leftHiddenFrame
            } else {
                fromView.frame = rightHiddenFrame
                toView.frame = visibleFrame
            }
            
        }) { (_) in
            let cancelled = transitionContext.transitionWasCancelled
            if cancelled {
                toView.removeFromSuperview()
            }
            transitionContext.completeTransition(!cancelled)
        }
    }
}
