//
//  TextSizeHelper.swift
//  VKExt
//
//  Created by programmist_NA on 01.06.2020.
//

import Foundation
import UIKit

class TextSizeHelper {
    private let textStorage: NSTextStorage
    private let textContainer: NSTextContainer
    private let layoutManager: NSLayoutManager

    init() {
        textStorage = NSTextStorage()
        textContainer = NSTextContainer()
        layoutManager = NSLayoutManager()

        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
    }

    public func computeSize(for attributedText: NSAttributedString,
                            within maxWidth: CGFloat) -> CGSize {
        textStorage.setAttributedString(attributedText)
        textContainer.size = CGSize(width: maxWidth, height: .greatestFiniteMagnitude)
        return layoutManager.usedRect(for: textContainer).integral.size
    }
}
class PhotoSizeHepler {
    public func computeSize(for height: CGFloat, within maxWidth: CGFloat) -> CGSize {
        return CGSize(width: maxWidth, height: height)
    }
}
