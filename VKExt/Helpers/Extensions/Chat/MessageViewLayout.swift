//
//  MessageViewLayout.swift
//  VKExt
//
//  Created by programmist_NA on 01.06.2020.
//

import Foundation
import UIKit

class InvertedStackLayout: UICollectionViewFlowLayout {
    override func prepare() {
        super.prepare()
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard super.layoutAttributesForElements(in: rect) != nil else { return nil }
        let attributesArrayNew = [UICollectionViewLayoutAttributes]()

        for attr in attributesArrayNew {
            attr.transform = CGAffineTransform(scaleX: 1, y: -1)
        }

        return attributesArrayNew
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attr = layoutAttributesForItem(at: indexPath)
        attr?.transform = CGAffineTransform(scaleX: 1, y: -1)
        return attr
    }
}

protocol MessageViewLayoutDelegate: class {
    func collectionView(_ collectionView: UICollectionView, fillAttributes: MessageViewLayoutAttributes)
}

class MessageViewLayout: UICollectionViewLayout {

    // MARK: private vars
    weak var delegate: MessageViewLayoutDelegate!

    fileprivate var contentHeight: CGFloat = 0

    fileprivate var attributeCache: [MessageViewLayoutAttributes] = []

    fileprivate var contentWidth: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }

    // MARK: overriden methods
    override class var layoutAttributesClass: AnyClass {
        return MessageViewLayoutAttributes.self
    }

    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }

    override func prepare() {
        guard let collectionView = collectionView else {
            return
        }

        let itemCount = collectionView.numberOfItems(inSection: 0)
        if attributeCache.count == itemCount {
            // Item count did not change. Skip update
            return
        }

        // Calculate and cache cell attributes.
        attributeCache.removeAll(keepingCapacity: true)
        var yOffset: CGFloat = 0 //collectionView.layoutMargins.top
        let leftMargin = collectionView.layoutMargins.left
        for item in 0 ..< itemCount {
            let indexPath = IndexPath(item: item, section: 0)
            let attr = MessageViewLayoutAttributes(forCellWith: indexPath)
            delegate.collectionView(collectionView, fillAttributes: attr)

            // Adjust frame origin: add margin and shift down.
            attr.frame.origin.y += yOffset
            attr.frame.origin.x += leftMargin
            attributeCache.append(attr)
            yOffset = attr.frame.maxY + attr.cellSpacing
            contentHeight = attr.frame.maxY
        }
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleAttributes = [UICollectionViewLayoutAttributes]()
        // Loop through the cache and look for items in the rect
        
        let sectionsCount = self.collectionView!.dataSource!.numberOfSections!(in: self.collectionView!)
        for section in 0 ..< sectionsCount {
            let itemsCount = self.collectionView!.numberOfItems(inSection: section)
            for item in 0 ..< itemsCount {
                let indexPath = IndexPath(item: item, section: section)
                visibleAttributes.append(layoutAttributesForItem(at: indexPath)!)
            }
            visibleAttributes.append(layoutAttributesForSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, at: IndexPath(item: itemsCount, section: 0))!)
        }
        return visibleAttributes
    }
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: elementKind, with: indexPath)
        attributes.frame = CGRect(x: 0, y: self.collectionView!.collectionViewHeight - 8, width: UIScreen.main.bounds.width, height: 36)
        attributes.transform = CGAffineTransform(scaleX: 1, y: -1)
        
        return attributes
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        attributeCache[indexPath.item].transform = CGAffineTransform(scaleX: 1, y: -1)
        return attributeCache[indexPath.item]
    }

    override func invalidateLayout() {
        super.invalidateLayout()

        attributeCache.removeAll(keepingCapacity: true)
    }

    // MARK: helper methods
}

class MessageViewLayoutAttributes: UICollectionViewLayoutAttributes {
    // StickerView position and size
    var bgViewFrame: CGRect = .zero
    // Alpha from cells
    var cellAlpha: CGFloat = 1
    // Avatar position and size
    var avatarFrame: CGRect = .zero
    // Sender name label position and size
    var senderNameFrame: CGRect = .zero

    // Message bubble position and size
    var containerFrame: CGRect = .zero

    // Message content inside the bubble.
    var contentFrame: CGRect = .zero

    // Delivery marker.
    var deliveryMarkerFrame: CGRect = .zero

    // Timestamp.
    var timestampFrame: CGRect = .zero

    // Optional new date label above message bubble
    var newDateFrame: CGRect = .zero
    
    // Optional new date label above message bubble
    var newMessagesFrame: CGRect = .zero

    // Vertical spacing between cells
    var cellSpacing: CGFloat = 0

    // Progress bar.
    var progressViewFrame: CGRect = .zero

    override func copy(with zone: NSZone? = nil) -> Any {
        let copy = super.copy(with: zone) as! MessageViewLayoutAttributes

        copy.bgViewFrame = bgViewFrame
        copy.cellAlpha = cellAlpha
        copy.avatarFrame = avatarFrame
        copy.senderNameFrame = senderNameFrame
        copy.containerFrame = containerFrame
        copy.contentFrame = contentFrame
        copy.deliveryMarkerFrame = deliveryMarkerFrame
        copy.timestampFrame = timestampFrame
        copy.newDateFrame = newDateFrame
        copy.newMessagesFrame = newMessagesFrame
        copy.cellSpacing = cellSpacing
        copy.progressViewFrame = progressViewFrame
        return copy
    }

    override func isEqual(_ object: Any?) -> Bool {
        guard let other = object as? MessageViewLayoutAttributes else { return false }

        return super.isEqual(object) &&
            other.alpha == alpha &&
            other.bgViewFrame == bgViewFrame &&
            other.avatarFrame == avatarFrame &&
            other.senderNameFrame == senderNameFrame &&
            other.containerFrame == containerFrame &&
            other.contentFrame == contentFrame &&
            other.deliveryMarkerFrame == deliveryMarkerFrame &&
            other.timestampFrame == timestampFrame &&
            other.newDateFrame == newDateFrame &&
            other.newMessagesFrame == newMessagesFrame &&
            other.cellSpacing == cellSpacing &&
            other.progressViewFrame == progressViewFrame
    }
}
