//
//  Realm.swift
//  Epic Phrases
//
//  Created by programmist_np on 07.03.2020.
//  Copyright © 2020 Funny Applications. All rights reserved.
//

import Foundation
import RealmSwift

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        return array
    }
}
extension Realm {
    public func safeWrite(_ block: (() throws -> Void)) throws {
        if isInWriteTransaction {
            try block()
        } else {
            try write(block)
        }
    }
}
struct DataAccessObject<O: Object> {
    private let translator: Translator
    
    init(translator: Translator) {
        self.translator = translator
    }
    
    func persist(with any: Any) {
        guard let realm = try? Realm() else { return }
        let object = translator.toObject(with: any)
        try? realm.write { realm.add(object, update: .all) }
    }
    
    func read(by key: String) -> Any? {
        guard let realm = try? Realm() else { return nil }
        if let object = realm.object(ofType: O.self, forPrimaryKey: key) {
            return translator.toAny(with: object)
        } else {
            return nil
        }
    }
}

protocol Translator {
    func toObject(with any: Any) -> Object
    func toAny(with object: Object) -> Any
}

extension Array where Element: Hashable {
    func differenceArrays(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
}
