//
//  UIVIewController.swift
//  VKExt
//
//  Created by programmist_NA on 20.05.2020.
//

import Foundation
import UIKit
import Material
import MaterialComponents
import AudioToolbox

enum InsetView {
    case toolbar
    case statusbar
    case tabbar
    case searchbar
}

enum Vibration {
    case error
    case success
    case warning
    case light
    case medium
    case heavy
    @available(iOS 13.0, *)
    case soft
    @available(iOS 13.0, *)
    case rigid
    case selection
    case oldSchool

    public func vibrate() {
        switch self {
        case .error:
            UINotificationFeedbackGenerator().notificationOccurred(.error)
        case .success:
            UINotificationFeedbackGenerator().notificationOccurred(.success)
        case .warning:
            UINotificationFeedbackGenerator().notificationOccurred(.warning)
        case .light:
            UIImpactFeedbackGenerator(style: .light).impactOccurred()
        case .medium:
            UIImpactFeedbackGenerator(style: .medium).impactOccurred()
        case .heavy:
            UIImpactFeedbackGenerator(style: .heavy).impactOccurred()
        case .soft:
            if #available(iOS 13.0, *) {
                UIImpactFeedbackGenerator(style: .soft).impactOccurred()
            }
        case .rigid:
            if #available(iOS 13.0, *) {
                UIImpactFeedbackGenerator(style: .rigid).impactOccurred()
            }
        case .selection:
            UISelectionFeedbackGenerator().selectionChanged()
        case .oldSchool:
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        }
    }
}

func getCommonInset(by views: [InsetView] = [.statusbar]) -> CGFloat {
    var commonInset: CGFloat = 0
    for view in views {
        switch view {
        case .toolbar:
            commonInset += GlobalConstants.toolbarHeight
        case .statusbar:
            commonInset += GlobalConstants.statusBarHeight
        case .tabbar:
            commonInset += 49
        case .searchbar:
            commonInset += 52
        }
    }
    return commonInset
}

extension UIViewController {
    func prepareSpace() {
        self.view.backgroundColor = .adaptableWhite
    }
    // Показать снэкбар
    func showSnackbar(with message: String, bottomOffset: CGFloat = 0, actionButtonTitle: String? = nil, buttonTitleColor: UIColor? = .extendedOrange, action: Void? = nil, snackbarStyle: MessageViewState) {
        let snackbarMessage = MDCSnackbarMessage()
        snackbarMessage.text = message
        MDCSnackbarManager.setBottomOffset(bottomOffset)
        switch snackbarStyle {
        case .normal:
            MDCSnackbarManager.messageTextColor = .adaptableWhite
            MDCSnackbarManager.snackbarMessageViewBackgroundColor = .adaptableBlack
        case .success:
            MDCSnackbarManager.messageTextColor = .white
            MDCSnackbarManager.snackbarMessageViewBackgroundColor = .extendedGreen
        case .warning:
            MDCSnackbarManager.messageTextColor = .white
            MDCSnackbarManager.snackbarMessageViewBackgroundColor = .extendedOrange
        case .error:
            MDCSnackbarManager.messageTextColor = .white
            MDCSnackbarManager.snackbarMessageViewBackgroundColor = .extendedRed
        }
        MDCSnackbarManager.messageFont = GoogleSansFont.medium(with: 14)
        MDCSnackbarManager.buttonFont = GoogleSansFont.medium(with: 14)
        MDCSnackbarManager.setButtonTitleColor(buttonTitleColor, for: .normal)
        MDCSnackbarManager.show(snackbarMessage)
        
        guard let action = action else { return }
        let actionButton = MDCSnackbarMessageAction()
        let actionHandler = { () in
            action
        }
        actionButton.handler = actionHandler
        actionButton.title = actionButtonTitle
        snackbarMessage.action = actionButton
    }
    // Добавление блюра к View
    func setBlurBackground(style: UIBlurEffect.Style) -> UIVisualEffectView {
        let blurEffect = UIBlurEffect(style: style)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = self.view.bounds
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return blurView
    }
    
    //
    func blurEffect(with style: UIBlurEffect.Style) -> UIVisualEffect? {
        return UIBlurEffect(style: style)
    }
    
    func setLabel(image: UIImage) -> NSMutableAttributedString? {
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = image
        // Set bound to reposition
        imageAttachment.bounds = CGRect(x: 0, y: 0, width: 25, height: 25)
        // Create string with attachment
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        // Initialize mutable string
        let completeText = NSMutableAttributedString(string: " ")
        // Add image to mutable string
        completeText.append(attachmentString)
        
        return completeText
    }
    
    @available(iOS 13.0, *)
    var topbarHeight: CGFloat {
        return (view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0.0) +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
    
    func messageView(message: String, _ style: MessageViewState, with loading: Bool = false) -> UIView {
        let maskView: UIView = {
            let view = UIView()
            switch style {
            case .normal:
                view.backgroundColor = .color(from: 0x2787F5)
            case .success:
                view.backgroundColor = .extendedGreen
            case .warning:
                view.backgroundColor = .adaptableOrange
            case .error:
                view.backgroundColor = .adaptableRed
            }
            return view
        }()
        let messageLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .center
            label.numberOfLines = 0
            return label
        }()
        let progressIndicator: MDCActivityIndicator = {
            let progress = MDCActivityIndicator()
            progress.radius = 6.5
            progress.strokeWidth = 2
            progress.indicatorMode = .indeterminate
            progress.cycleColors = [.white]
            return progress
        }()
        progressIndicator.isHidden = !loading
        progressIndicator.startAnimating()
        maskView.addSubview(messageLabel)
        maskView.addSubview(progressIndicator)
        messageLabel.attributedText = NSAttributedString(string: message, attributes: [.font: GoogleSansFont.medium(with: 13), .foregroundColor: UIColor.white])
        // messageLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 12, left: 12, bottom: 8, right: 12))
        messageLabel.autoCenterInSuperview()
        messageLabel.padding = UIEdgeInsets(top: 12, left: 12, bottom: 8, right: !loading ? 0 : 12)
        progressIndicator.autoPinEdge(.leading, to: .trailing, of: messageLabel, withOffset: -8)
        progressIndicator.autoPinEdge(.top, to: .top, of: maskView, withOffset: 13)
        maskView.autoSetDimensions(to: CGSize(width: Screen.bounds.width, height: message.height(with: Screen.bounds.width, font: GoogleSansFont.medium(with: 14)) + 20))
        return maskView
    }
    
    func getStringByDeclension(number: Int, arrayWords: [String?]) -> String {
        var resultString: String = ""
        let number = number % 100
        if number >= 11 && number <= 19 {
            resultString = arrayWords[2]!
        } else {
            let i: Int = number % 10
            switch i {
            case 1: resultString = arrayWords[0]!
                break
            case 2, 3, 4:
                resultString = arrayWords[1]!
                break
            default:
                resultString = arrayWords[2]!
                break
            }
        }
        return resultString
    }
    
    @objc public func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension NSObject {
    public func getPercentage(from value: Double, with percent: Double) -> Double {
        let val = value * percent
        return val / 100.0
    }
}
extension UINavigationController {
    var invisible: Void {
        get {
            return self.setNavigationBarHidden(true, animated: false)
        }
    }
    
    func invisible<T: UINavigationController>(navigationController: T) {
        self.setNavigationBarHidden(true, animated: false)
    }

    func statusbarColor(from color: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = color
        view.addSubview(statusBarView)
    }
}
extension UIViewController {
    @objc func scrollToTop() {
        func scrollToTop(view: UIView?) {
            guard let view = view else { return }

            switch view {
            case let scrollView as UIScrollView:
                if scrollView.scrollsToTop == true {
                    scrollView.setContentOffset(CGPoint(x: 0.0, y: -scrollView.contentInset.top), animated: true)
                    return
                }
            default:
                break
            }

            for subView in view.subviews {
                scrollToTop(view: subView)
            }
        }

        scrollToTop(view: self.view)
    }

}
