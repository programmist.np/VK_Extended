//
//  NotificationName.swift
//  VKExt
//
//  Created by programmist_NA on 20.05.2020.
//

import Foundation

extension Notification.Name {
    static let onLogin = Notification.Name(rawValue: "VK.Login")
    static let onLogout = Notification.Name(rawValue: "VK.Logout")
    static let onChangeSessionState = Notification.Name(rawValue: "VK.Session.Changed")
    static let onMessagesUpdate = Notification.Name(rawValue: "VK.Messages.Update")
    static let onMessagesReceived = Notification.Name(rawValue: "VK.Messages.Received")
    static let onMessagesEdited = Notification.Name(rawValue: "VK.Messages.Edited")
    static let onSetMessageFlags = Notification.Name(rawValue: "VK.Messages.Delete")
    static let onInMessagesRead = Notification.Name(rawValue: "VK.Messages.Received")
    static let onOutMessagesRead = Notification.Name(rawValue: "VK.Messages.Delete")
    static let onFriendOnline = Notification.Name(rawValue: "VK.Friends.Online")
    static let onFriendOffline = Notification.Name(rawValue: "VK.Friends.Offline")
    static let onTyping = Notification.Name(rawValue: "VK.Messages.Typing")
    static let onRemoveConversation = Notification.Name(rawValue: "VK.Messages.Conversation.Remove")
    static let onCounterChanged = Notification.Name(rawValue: "VK.Counter.Changed")
}
extension Notification.Name {
    static let receivedMessage = Notification.Name(rawValue: "VK.Messages.Receive")
    static let receivedImportantMessage = Notification.Name(rawValue: "VK.Messages.Important.Receive")
    static let receivedMessageFromDialog = Notification.Name(rawValue: "VK.Messages.Receive.From.Dialog")
}
extension Notification.Name {
    static let showMessage = Notification.Name(rawValue: "VKExt.ViewController.ShowMessage")
}
extension Notification.Name {
    static let onSuccessPrepareSpace = Notification.Name(rawValue: "VKExt.Services.onSuccessPrepareSpace")
    static let onFailurePrepareSpace = Notification.Name(rawValue: "VKExt.Services.onFailurePrepareSpace")
    static let onSuccessConnectLongPoll = Notification.Name(rawValue: "VKExt.Services.onSuccessConnectLongPoll")
    static let onSuccessSetCounters = Notification.Name(rawValue: "VKExt.Services.onSuccessSetCounters")
    static let onFailureSetCounters = Notification.Name(rawValue: "VKExt.Services.onFailureSetCounters")
}
