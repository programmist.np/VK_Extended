//
//  UIApplication.swift
//  VKExt
//
//  Created by programmist_NA on 20.05.2020.
//

import Foundation
import UIKit

var iosVersion: String {
    let os = ProcessInfo().operatingSystemVersion
    return String(os.majorVersion) + "." + String(os.minorVersion) + "." + String(os.patchVersion)
}

var osVersion: Double {
    let os = ProcessInfo().operatingSystemVersion
    return os.majorVersion.doubleValue + os.minorVersion.doubleValue / 10
}

extension UIApplication {
    open var keyWindow: UIWindow? {
        get {
            if #available(iOS 13.0, *) {
                return UIApplication.shared.connectedScenes
                    .filter({$0.activationState == .foregroundActive})
                    .map({$0 as? UIWindowScene})
                    .compactMap({$0})
                    .first?.windows
                    .filter({$0.isKeyWindow}).first
            } else {
                return self.keyWindow
            }
        }
    }
    open var rootViewController: UIViewController? {
        if #available(iOS 13.0, *) {
            guard let rootViewController = UIApplication.shared.windows.first?.rootViewController else { return nil }
            return rootViewController
        } else {
            guard let rootViewController = keyWindow?.rootViewController else { return nil }
            return rootViewController
        }
    }
    
    var visibleViewController: UIViewController? {
        guard let rootViewController = rootViewController else { return nil }
        return getVisibleViewController(rootViewController)
    }

    private func getVisibleViewController(_ rootViewController: UIViewController) -> UIViewController? {
        if let presentedViewController = rootViewController.presentedViewController {
            return getVisibleViewController(presentedViewController)
        }

        if let navigationController = rootViewController as? UINavigationController {
            return navigationController.visibleViewController
        }

        if let tabBarController = rootViewController as? BottomNavigationViewController {
            return tabBarController.selectedViewController
        }

        return rootViewController
    }
    
    func statusbarColor(from color: UIColor) {
        if #available(iOS 13, *) {
            let statusBar1 =  UIView()
            statusBar1.frame = UIApplication.shared.statusBarFrame
            statusBar1.backgroundColor = UIColor.red
            UIApplication.shared.keyWindow?.addSubview(statusBar1)
        } else {
           let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
           if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
              statusBar.backgroundColor = color
           }
        }
    }
}
extension UserDefaults {
    // check for is first launch - only true on first invocation after app install, false on all further invocations
    // Note: Store this value in AppDelegate if you have multiple places where you are checking for this flag
    class var isFirstLaunch: Bool {
        let hasBeenLaunchedBeforeFlag = "hasBeenLaunchedBeforeFlag"
        let isFirstLaunch = !UserDefaults.standard.bool(forKey: hasBeenLaunchedBeforeFlag)
        if (isFirstLaunch) {
            UserDefaults.standard.set(true, forKey: hasBeenLaunchedBeforeFlag)
            UserDefaults.standard.synchronize()
        }
        return isFirstLaunch
    }
}
private var firstLaunch: Bool = false
extension UIApplication {

    class var isFirstLaunch: Bool {
        let firstLaunchFlag = "isFirstLaunchFlag"
        let isFirstLaunch = UserDefaults.standard.string(forKey: firstLaunchFlag) == nil
        if (isFirstLaunch) {
            firstLaunch = isFirstLaunch
            UserDefaults.standard.set("false", forKey: firstLaunchFlag)
            UserDefaults.standard.synchronize()
        }
        return firstLaunch || isFirstLaunch
    }
}
extension UIDevice {
    static var isIPad: Bool {
        return UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad
    }
    
    static var isIPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone
    }
    
    static var isIPhone4: Bool {
        return UIDevice.isIPhone && UIScreen.main.bounds.size.height < 568.0
    }
}
