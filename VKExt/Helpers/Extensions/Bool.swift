//
//  Bool.swift
//  VKExt
//
//  Created by programmist_NA on 24.06.2020.
//

import Foundation
import UIKit

extension Bool {
    var intValue: Int {
        return self ? 1 : 0
    }
}
