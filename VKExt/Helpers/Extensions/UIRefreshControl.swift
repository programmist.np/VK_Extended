//
//  UIRefreshControl.swift
//  VKExt
//
//  Created by programmist_NA on 20.05.2020.
//

import Foundation
import UIKit

extension UIRefreshControl {
    func beginRefreshing(in collectionView: UICollectionView) {
        beginRefreshing()
        let offsetPoint = CGPoint.init(x: 0, y: -frame.size.height)
        collectionView.setContentOffset(offsetPoint, animated: true)
    }
    
    func beginRefreshing(in tableView: UITableView) {
        beginRefreshing()
        let offsetPoint = CGPoint.init(x: 0, y: -frame.size.height)
        guard tableView.contentOffset.y < frame.size.height else { return }
        tableView.setContentOffset(offsetPoint, animated: true)
    }
}
