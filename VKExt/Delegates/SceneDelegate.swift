//
//  SceneDelegate.swift
//  VKExt
//
//  Created by programmist_NA on 20.05.2020.
//

import UIKit
import SwiftyVK
import Material

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    var vkDelegateReference: SwiftyVKDelegate?
    let isLogged = UserDefaults.standard.bool(forKey: "logState")
    let defaults = UserDefaults.standard
    let blurEffectView: UIVisualEffectView = {
        let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
        blurEffectView.tag = 221122
        blurEffectView.alpha = 0
        return blurEffectView
    }()
    let imageView: UIImageView = {
        let view = UIImageView(image: UIImage(named: "launch_icon"))
        view.contentMode = .scaleAspectFit
        view.tag = 221123
        view.backgroundColor = .clear
        return view 
    }()

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
        let rootViewController = self.window!.rootViewController as! InitialNavigationController
        brightnessDidChange()
        if isLogged {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "TabBarStartController") as! BottomNavigationViewController
            rootViewController.pushViewController(initialViewController, animated: true)
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            rootViewController.pushViewController(initialViewController, animated: true)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(brightnessDidChange), name: UIScreen.brightnessDidChangeNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground(_:)), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive(_:)), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
    }

    func sceneDidDisconnect(_ scene: UIScene) {
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
    }

    func sceneWillResignActive(_ scene: UIScene) {
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
    }
    
    @objc func brightnessDidChange() {
//        if UIScreen.main.brightness < 0.3 {
//            guard self.window?.overrideUserInterfaceStyle != .dark else { return }
//            UIView.transition(with: self.window!, duration: 0.3, options: [.preferredFramesPerSecond60, .transitionCrossDissolve], animations: { }, completion: { [weak self] _ in
//                guard let self = self else { return }
//                self.window?.overrideUserInterfaceStyle = .dark
//            })
//        } else {
//            guard self.window?.overrideUserInterfaceStyle != .light else { return }
//            UIView.transition(with: self.window!, duration: 0.3, options: [.preferredFramesPerSecond60, .transitionCrossDissolve], animations: { }, completion: { [weak self] _ in
//                guard let self = self else { return }
//                self.window?.overrideUserInterfaceStyle = .light
//            })
//        }
    }
    
    @objc func didEnterBackground(_ notification: Notification) {
        print(#function)
        blurEffectView.frame = self.window?.frame ?? .zero
        self.window?.addSubview(blurEffectView)
        self.window?.addSubview(imageView)
        self.window?.bringSubviewToFront(imageView)
        imageView.autoCenterInSuperview()
        imageView.autoSetDimensions(to: CGSize(width: 96, height: 96))

        UIView.animate(withDuration: 0.2, animations: {
            self.blurEffectView.alpha = 1
            self.imageView.alpha = 1
        })
    }
    
    @objc func didBecomeActive(_ notification: Notification) {
        print(#function)
        UIView.animate(withDuration: 0.2, animations: {
            self.blurEffectView.alpha = 0
            self.imageView.alpha = 0
        }, completion: { _ in
            self.window?.viewWithTag(221122)?.removeFromSuperview()
            self.window?.viewWithTag(221123)?.removeFromSuperview()
        })
    }
}
