//
//  AppDelegate.swift
//  VKExt
//
//  Created by programmist_NA on 20.05.2020.
//

import UIKit
import Reachability
import SwiftyVK
import RealmSwift
import UserNotifications
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var vkDelegateReference: SwiftyVKDelegate?
    let defaults = UserDefaults.standard
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        vkDelegateReference = VKDelegate()
        application.beginReceivingRemoteControlEvents()
        return true
    }
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        let config = Realm.Configuration(schemaVersion: 1, migrationBlock: { migration, oldSchemaVersion in
            if (oldSchemaVersion < 1) {
                ///code
            }
        })
        Realm.Configuration.defaultConfiguration = config
        _ = try! Realm()
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        defaults.set("\(url)", forKey: "sourceUrl")
        defaults.set(sourceApplication, forKey: "sourceApp")
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }
}
extension AppDelegate {
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("didReceiveRemoteNotification \(userInfo)")

        guard let dict = userInfo["aps"] as? [String: Any], let msg = dict["alert"] as? String else {
            print("Notification Parsing Error")
            completionHandler(.failed)
            return
        }
        completionHandler(.newData)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map{ String(format: "%02x", $0) }.joined()
        VK.API.Custom.method(name: "account.registerDevice", parameters: ["token": token,
                                                                          "device_model": UIDevice.current.model,
                                                                          "device_year": "2017",
                                                                          "device_id": UIDevice.current.identifierForVendor!.uuidString,
                                                                          "system_version": UIDevice.current.systemVersion,
                                                                          "settings": VKConstants.pushSettings,
                                                                          "sandbox": "0"])
        .configure(with: Config.init(httpMethod: .GET, language: .default))
            .onSuccess { response in
                print("success")
        }.onError { error in
            print(error.localizedDescription)
        }.send()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
}
