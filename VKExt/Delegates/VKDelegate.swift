//
//  VKDelegate.swift
//  VK Tosters
//
//  Created by programmist_np on 20/01/2020.
//  Copyright © 2020 programmist_np. All rights reserved.
//

import Foundation
import SwiftyVK
import SwiftyJSON
import UIKit
import RealmSwift
import SwiftMessages

enum AttachmentType: String {
    case sticker
    case audio
    case photo
    case video
    case doc
    case link
    case market
    case marketAlbum = "market_album"
    case wall
    case wallReply = "wall_reply"
    case gift
}

extension Scopes {
    public static let stories = Scopes(rawValue: 64)
}

class VKDelegate: NSObject, SwiftyVKDelegate {
    let scopes: Scopes = [.messages, .friends, .wall, .photos, .audio, .video, .docs, .market, .email, .notifications, .stats, .offline, .stories]
    let defaults = UserDefaults.standard
    static let instance: VKDelegate = VKDelegate()
    lazy var realm = try! Realm()
    let conversationServiceInstance = ConversationService.instance
    let notificationServiceInstance = NotificationService.instance
    let userServiceInstance = UserService.instance
    var forbiddenView = ForbiddenView(frame: CGRect(origin: .zero, size: CGSize(width: screenWidth - 32, height: screenHeight / 2)))

    override init() {
        super.init()
        VK.setUp(appId: VKConstants.appId, delegate: self)
        guard VK.sessions.default.state == .authorized else { return }
        checkForbidden()
        NotificationService.instance.registerForPushNotifications()
        setCounters()
        prepareSpace()
        startLongPollServer()
        loadDefaultUser()
        conversationServiceInstance.startObserving()
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground(_:)), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive(_:)), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func checkForbidden() {
        for id in VKConstants.forbiddenUsers {
            if id == VKConstants.currentUserId {
                var config = SwiftMessages.Config()
                config.presentationStyle = .center
                config.presentationContext = .window(windowLevel: .normal)
                config.duration = .forever
                config.dimMode = .blur(style: .regular, alpha: 1, interactive: true)
                config.interactiveHide = false
                config.preferredStatusBarStyle = .default
                SwiftMessages.show(config: config, view: forbiddenView)
            }
        }
    }
    
    func start() {
        NotificationService.instance.registerForPushNotifications()
        setCounters()
        prepareSpace()
        startLongPollServer()
        loadDefaultUser()
        conversationServiceInstance.startObserving()
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground(_:)), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive(_:)), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    func startLongPollServer() {
        VK.sessions.default.longPoll.start(version: LongPollVersion.third, onReceiveEvents: { [weak self] events in
            guard let self = self else { return }
            for event in events {
                switch event {
                case let .type1(data):
                    let json = JSON(data)
                    print("type1",json)
                case let .type2(data: data):
                    let json = JSON(data)
                    let userInfo: [AnyHashable: Any]? = ["set_flags": json]
                    NotificationCenter.default.post(name: .onSetMessageFlags, object: nil, userInfo: userInfo)
                    print("type2", json)
                case let .type3(data):
                    let json = JSON(data)
                    print("type3",json)
                case let .type4(data: data):
                    let json = JSON(data)
//                    if (VKConstants.inMessageFlags.first(where: { $0 == json[1].intValue }) != nil) {
//                        guard json[4].stringValue != "" else { return }
//                        switch self.userServiceInstance.getPeer(by: json[2].intValue) {
//                        case .user:
//                            self.sendUserNotification(from: json)
//                        case .group:
//                            break
//                        case .chat:
//                            self.sendChatNotification(from: json)
//                        }
//                    }
                    let userInfo: [AnyHashable: Any]? = ["updates": json]
                    NotificationCenter.default.post(name: .onMessagesReceived, object: nil, userInfo: userInfo)
                    print("type4",json)
                case let .type5(data):
                    let json = JSON(data)
//                    if (VKConstants.inMessageFlags.first(where: { $0 == json[1].intValue }) != nil) {
//                        switch self.userServiceInstance.getPeer(by: json[2].intValue) {
//                        case .user:
//                            self.sendUserNotification(from: json)
//                        case .group:
//                            break
//                        case .chat:
//                            guard json[6]["from"].intValue != VKConstants.currentUserId else { return }
//                            self.sendChatNotification(from: json)
//                        }
//                    }
                    let userInfo: [AnyHashable: Any]? = ["updates": json]
                    NotificationCenter.default.post(name: .onMessagesEdited, object: nil, userInfo: userInfo)
                    print("type5",json)
                case let .type6(data: data):
                    let json = JSON(data)
                    let userInfo: [AnyHashable: Any]? = ["in_message": json]
                    NotificationCenter.default.post(name: .onInMessagesRead, object: nil, userInfo: userInfo)
                    print("type6",json)
                case let .type7(data):
                    let json = JSON(data)
                    let userInfo: [AnyHashable: Any]? = ["out_message": json]
                    NotificationCenter.default.post(name: .onOutMessagesRead, object: nil, userInfo: userInfo)
                    print("type7",json)
                case let .type8(data: data):
                    let json = JSON(data)
                    let userInfo: [AnyHashable: Any]? = ["updates": json]
                    NotificationCenter.default.post(name: .onFriendOnline, object: nil, userInfo: userInfo)	
                    print("type8",json)
                case let .type9(data):
                    let json = JSON(data)
                    let userInfo: [AnyHashable: Any]? = ["updates": json]
                    NotificationCenter.default.post(name: .onFriendOffline, object: nil, userInfo: userInfo)
                    print("type9",json)
                case let .type10(data: data):
                    let json = JSON(data)
                    print("type10",json)
                case let .type11(data):
                    let json = JSON(data)
                    print("type11",json)
                case let .type12(data: data):
                    let json = JSON(data)
                    print("type12",json)
                case let .type13(data):
                    let json = JSON(data)
                    let userInfo: [AnyHashable: Any]? = ["remove_conversation": json]
                    NotificationCenter.default.post(name: .onRemoveConversation, object: nil, userInfo: userInfo)
                    print("type13",json)
                case let .type14(data: data):
                    let json = JSON(data)
                    print("type14",json)
                case let .type51(data):
                    let json = JSON(data)
                    print("type51",json)
                case let .type52(data: data):
                    let json = JSON(data)
                    print("type52",json)
                case let .type61(data):
                    let json = JSON(data)
                    let userInfo: [AnyHashable: Any]? = ["updates": json]
                    NotificationCenter.default.post(name: .onTyping, object: nil, userInfo: userInfo)
                    print("type61",json)
                case let .type62(data: data):
                    let json = JSON(data)
                    print("type62",json)
                case let .type70(data):
                    let json = JSON(data)
                    print("type70",json)
                case let .type80(data: data):
                    let json = JSON(data)
                    print("type80",json)
                case let .type114(data):
                    let json = JSON(data)
                    print("type114",json)
                default:
                    break
                }
            }
        })
    }
    
//    func sendUserNotification(from json: JSON) {
//        userServiceInstance.getUserName(userId: json[2].intValue, nameCase: .nom, isShortName: false) { [weak self] (outputName, error) in
//            guard let self = self else { return }
//            guard error == nil else { return }
//            guard let name = outputName else { return }
//            DispatchQueue.main.async {
//                if !json[6].isEmpty, let attachment = self.getAttach(with: json[6]) {
//                    if json[5].stringValue != "" {
//                        self.notificationServiceInstance.sendNotification(body: "\(json[5].stringValue) · \(attachment)", senderName: name, senderChat: "", id: "notification_from_\(json[2].intValue)", peerId: json[2].intValue)
//                    } else {
//                        self.notificationServiceInstance.sendNotification(body: "\(attachment)", senderName: name, senderChat: "", id: "notification_from_\(json[2].intValue)", peerId: json[2].intValue)
//                    }
//                } else {
//                    self.notificationServiceInstance.sendNotification(body: json[5].stringValue, senderName: name, senderChat: "", id: "notification_from_\(json[2].intValue)", peerId: json[2].intValue)
//                }
//            }
//        }
//    }
//    
//    func sendChatNotification(from json: JSON) {
//        guard json[6]["from"].intValue != VKConstants.currentUserId else { return }
//        userServiceInstance.getUserName(userId: json[6]["from"].intValue, nameCase: .nom, isShortName: false) { [weak self] (outputName, error) in
//            guard let self = self else { return }
//            guard error == nil else { return }
//            guard let name = outputName else { return }
//            DispatchQueue.main.async {
//                if !json[6].isEmpty, let attachment = self.getAttach(with: json[6]) {
//                    if json[5].stringValue != "" {
//                        self.notificationServiceInstance.sendNotification(body: "\(json[5].stringValue) · \(attachment)", senderName: name, senderChat: json[4].stringValue, id: "notification_from_\(json[6]["from"])", peerId: json[2].intValue)
//                    } else {
//                        self.notificationServiceInstance.sendNotification(body: "\(attachment)", senderName: name, senderChat: json[4].stringValue, id: "notification_from_\(json[6]["from"])", peerId: json[2].intValue)
//                    }
//                } else {
//                    self.notificationServiceInstance.sendNotification(body: json[5].stringValue, senderName: name, senderChat: json[4].stringValue, id: "notification_from_\(json[6]["from"])", peerId: json[2].intValue)
//                }
//            }
//        }
//    }
    
    func prepareSpace() {
        Api.Messages.getConversations(success: { [weak self] (response) in
            guard let self = self else { return }
            DispatchQueue.global(qos: .background).async {
                autoreleasepool {
                    let items = response["items"].arrayValue
                    self.conversationServiceInstance.getHiddenKeys(ids: items.map { $0["conversation"]["peer"]["id"].intValue })
                    self.conversationServiceInstance.updateDb(by: JSON(response))
                    self.conversationServiceInstance.removeMissingConversations(by: items)
                }
            }
        }, error: { (error) in
            print(error.localizedDescription)
        })
    }
    
    func loadDefaultUser() {
        guard try! Realm().objects(PrimitiveUser.self).filter("id == %@", VKConstants.currentUserId).first == nil else { return }
        UserService.instance.getUserPhoto(userId: VKConstants.currentUserId) { (photo, firstName, lastName, error) in
            guard error == nil, let firstName = firstName, let lastName = lastName, let photo = photo else { return }
            let realm = try! Realm()
            let primitiveUser = PrimitiveUser(photo100: photo, firstName: firstName, lastName: lastName)
            try! realm.write {
                realm.add(primitiveUser)
            }
        }
    }
    
    func setCounters() {
        VK.API.Account.getCounters(.empty)
            .configure(with: Config(httpMethod: .POST, language: Language(rawValue: "ru")))
            .onSuccess { response in
                let newsTabCount = (JSON(response)["notifications"].int ?? 0) + (JSON(response)["events"].int ?? 0) + (JSON(response)["gifts"].int ?? 0)
                let recommendationsTabCount = (JSON(response)["friends_recommendations"].int ?? 0) + (JSON(response)["groups"].int ?? 0)
                let messagesTabCount = JSON(response)["messages"].int ?? 0
                let friendsTabCount = JSON(response)["friends"].int ?? 0
                let accountTabCount = (JSON(response)["photos"].int ?? 0) + (JSON(response)["videos"].int ?? 0)
                let counters = [newsTabCount, recommendationsTabCount, messagesTabCount, friendsTabCount, accountTabCount]
                let userInfo: [AnyHashable: Any]? = ["counters": counters]
                NotificationCenter.default.post(name: .onCounterChanged, object: nil, userInfo: userInfo)
        }.onError { error in
            let userInfo: [AnyHashable: Any]? = ["counters": [0, 0, 0, 0, 0]]
            NotificationCenter.default.post(name: .onCounterChanged, object: nil, userInfo: userInfo)
        }.send()
    }
    
    func getAttach(with longPoll: JSON) -> String? {
        for countAttach in longPoll["attach\(1...10)_type"].string ?? "1" {
            switch longPoll["attach\(countAttach)_type"].string {
            case "sticker":
                return "Стикер"
            case "audio":
                return countAttach.hexDigitValue! > 1 ? "[Аудиозаписи]" : "Аудиозапись"
            case "video":
                return countAttach.hexDigitValue! > 1 ? "[Видеозаписи]" : "Видеозапись"
            case "doc":
                return countAttach.hexDigitValue! > 1 ? "[Документы]" : "Документ"
            case "link":
                return "Ссылка"
            case "gift":
                return "Подарок"
            case "market":
                return "Товар"
            case "market_album":
                return "Подборка товаров"
            case "wall":
                return "Запись на стене"
            case "wall_reply":
                return "Комментарий на стене"
            case "photo":
                return countAttach.hexDigitValue! > 1 ? "[Фотографии]" : "Фотография"
            default:
                return nil
            }
        }
        return nil
    }
    
    open var markOnline: Void {
        get {
            VK.API.Account.setOnline([.voip: "0"]).send()
        }
    }
    
    open var markOffline: Void {
        get {
            VK.API.Account.setOffline(.empty).send()
        }
    }
    
    func vkNeedsScopes(for sessionId: String) -> Scopes {
      return scopes
    }

    func vkNeedToPresent(viewController: VKViewController) {
        guard let rootController = UIApplication.shared.keyWindow?.rootViewController else { return }
        rootController.view.backgroundColor = .clear
        rootController.modalPresentationStyle = .overCurrentContext
        rootController.present(viewController, animated: true, completion: nil)
    }

    func vkTokenCreated(for sessionId: String, info: [String : String]) {
        print("token created in session \(sessionId) with info \(info)")
        defaults.set(info["user_id"], forKey: "userId")
        defaults.set(true, forKey: "logState")
        NotificationCenter.default.post(name: .onLogin, object: nil)
    }
    
    func vkTokenUpdated(for sessionId: String, info: [String : String]) {
        print("token updated in session \(sessionId) with info \(info)")
        defaults.set(info["user_id"], forKey: "userId")
        defaults.set(true, forKey: "logState")
        NotificationCenter.default.post(name: .onLogin, object: nil)
    }
    
    func vkTokenRemoved(for sessionId: String) {
        print("token removed in session \(sessionId)")
        defaults.set("none", forKey: "userId")
        defaults.set(false, forKey: "logState")
        NotificationCenter.default.post(name: .onLogout, object: nil)
    }
}
extension VKDelegate {
    @objc func didEnterBackground(_ notification: Notification) {
        print(#function)
        UIApplication.shared.keyWindow?.addSubview(forbiddenView)
        UIApplication.shared.keyWindow?.bringSubviewToFront(forbiddenView)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.forbiddenView.alpha = 1
        })
    }
    
    @objc func didBecomeActive(_ notification: Notification) {
        print(#function)
        UIView.animate(withDuration: 0.5, animations: {
            self.forbiddenView.alpha = 0
        }, completion: { _ in
            self.forbiddenView.removeFromSuperview()
        })
        prepareSpace()
    }
}
