//
//  CommonService.swift
//  VKExt
//
//  Created by programmist_NA on 20.05.2020.
//

import Foundation
import UIKit
import Alamofire
import VKAudioToken
import SwiftyJSON
import ObjectMapper
import SwiftyVK

class CommonService: NSObject {
    static let instance: CommonService = CommonService()
    private var params: CommonParams! = CommonParams()
    
    static func getLastSeen(sex: Int, time: Date) -> String {
        let currentTime: NSNumber = NSNumber(value: Date().timeIntervalSince1970)
        let timestamp: NSNumber = NSNumber(value: time.timeIntervalSince1970)
        let seconds = timestamp.doubleValue
        let timestampDate = Date(timeIntervalSince1970: seconds)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        
        if timestamp.doubleValue < currentTime.doubleValue - 86401 && timestamp.doubleValue > currentTime.doubleValue - 172802 {
            dateFormatter.dateFormat = "H:mm"
            let formatDate = dateFormatter.string(from: timestampDate)
            return sex == 1 ? "Была в сети вчера в \(formatDate)" : "Был в сети вчера в \(formatDate)"
        } else if timestamp.doubleValue < currentTime.doubleValue - 172802 {
            dateFormatter.dateFormat = "d MMMM в H:mm"
            let formatDate = dateFormatter.string(from: timestampDate)
            return sex == 1 ? "Была в сети \(formatDate)" : "Был в сети \(formatDate)"
        } else {
            dateFormatter.dateFormat = "H:mm"
            let formatDate = dateFormatter.string(from: timestampDate)
            return sex == 1 ? "Была в сети в \(formatDate)" : "Был в сети в \(formatDate)"
        }
    }
    
    func getSecretData(login: String, password: String, success: @escaping(_ token: String, _ secret: String, _ deviceId: String) -> Void, fail: @escaping(_ error: Any?) -> Void) {
        TokenReceiverOfficial(login: login, pass: password).getToken(success: { (token, secretId, deviceId) in
            success(token, secretId, deviceId)
        }) { (error) in
            fail(error)
        }
    }
    
    func performRequest(method: String, postFields: Alamofire.Parameters, secret: String, success: @escaping(_ model: Any) -> Void, fail: @escaping(_ error: String) -> Void) {
        params.setCommonVK()
        
        let headers = [
            "User-Agent": params.ua
        ]
        
        let url = "https://api.vk.com/method/" + method
        
        var parameters: Alamofire.Parameters = postFields
        let sortedKeys = Array(parameters.keys).sorted(by: <)

        var stringForMD5 = "/method/\(method)?"
        for key in sortedKeys {
            stringForMD5 = stringForMD5 + key + "=\(parameters[key] ?? "")&"
        }
        if stringForMD5.last! == "&" {
            stringForMD5.remove(at: stringForMD5.index(before: stringForMD5.endIndex))
        }
        stringForMD5 = stringForMD5 + secret
        parameters["sig"] = MD5.MD5(stringForMD5)
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            if JSON(response.data!)["error"] != JSON.null {
                fail(ResponseError.getError(at: JSON(response.data!)["error"]["error_code"].intValue).rawValue)
                return
            } else if response.error != nil {
                fail(response.error!.localizedDescription)
            } else {
                success(response.data!)
            }
        }
    }
}
