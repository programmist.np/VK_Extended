//
//  ProfileService.swift
//  VKExt
//
//  Created by programmist_NA on 15.07.2020.
//

import Foundation

class ProfileService {
    private var revealedPostIds = [Int]()
    private var profileResponse: ProfileResponse?
    private var photosResponse: PhotoResponse?
    private var countOffset: Int = 0
    private var wallResponse: WallResponse?

    func getProfileInfo(userId: Int, completion: @escaping (ProfileResponse) -> Void, failed: @escaping (String) -> Void) {
        Api.Users.get(userId: userId, response: { [weak self] (profile) in
            self?.profileResponse = profile
            guard let profileResponse = self?.profileResponse else { return }
            completion(profileResponse)
        }) { error in
            failed(error)
        }
    }
    
    func getPhotos(userId: Int, completion: @escaping (PhotoResponse) -> Void, failed: @escaping (String) -> Void) {
        Api.Photos.get(ownerId: userId, response: { [weak self] (photo) in
            self?.photosResponse = photo
            guard let photosResponse = self?.photosResponse else { return }
            completion(photosResponse)
        }) { (error) in
            failed(error)
        }
    }

    func getWall(userId: Int, completion: @escaping ([Int], WallResponse) -> Void, failed: @escaping (String) -> Void) {
        Api.Wall.get(ownerId: userId, response: { [weak self] (feed) in
            self?.wallResponse = feed
            guard let wallResponse = self?.wallResponse else { return }
            completion(self!.revealedPostIds, wallResponse)
            },failed: { error in
                failed(error)
        })
    }
    
    func revealPostIds(forPostId postId: Int, completion: @escaping ([Int], WallResponse) -> Void) {
        revealedPostIds.append(postId)
        guard let wallResponse = self.wallResponse else { return }
        completion(revealedPostIds, wallResponse)
    }
    
    func getNextBatch(userId: Int, completion: @escaping ([Int], WallResponse) -> Void, failed: @escaping (String) -> Void) {
        countOffset = wallResponse?.count ?? 0
        Api.Wall.get(ownerId: userId, offset: countOffset, response: { [weak self] (feed) in
            guard let feed = feed else { return }
            guard self?.wallResponse?.items.count != feed.count else { return }
            
            if self?.wallResponse == nil {
                self?.wallResponse = feed
            } else {
                self?.wallResponse?.items.append(contentsOf: feed.items)
                
                var profiles = feed.profiles
                if let oldProfiles = self?.wallResponse?.profiles {
                    let oldProfilesFiltered = oldProfiles.filter({ (oldProfile) -> Bool in
                        !feed.profiles.contains(where: { $0.id == oldProfile.id })
                    })
                    profiles.append(contentsOf: oldProfilesFiltered)
                }
                self?.wallResponse?.profiles = profiles
                
                var groups = feed.groups
                if let oldGroups = self?.wallResponse?.groups {
                    let oldGroupsFiltered = oldGroups.filter({ (oldGroup) -> Bool in
                        !feed.groups.contains(where: { $0.id == oldGroup.id })
                    })
                    groups.append(contentsOf: oldGroupsFiltered)
                }
                self?.wallResponse?.groups = groups
            }
            
            guard let wallResponse = self?.wallResponse else { return }
            completion(self!.revealedPostIds, wallResponse)
        },failed: { error in
                failed(error)
        })
    }
    
    func addLikePost(type: String, ownerId: Int, itemId: Int, completion: (() -> ())? = nil) {
        Api.Likes.add(type: type, ownerId: ownerId, itemId: itemId) {
            guard let completion = completion else { return }
            completion()
        }
    }
    
    func deleteLikePost(type: String, ownerId: Int, itemId: Int, completion: (() -> ())? = nil) {
        Api.Likes.delete(type: type, ownerId: ownerId, itemId: itemId) {
            guard let completion = completion else { return }
            completion()
        }
    }
}
