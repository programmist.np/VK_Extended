//
//  FriendService.swift
//  VKExt
//
//  Created by programmist_NA on 22.07.2020.
//

import Foundation

class FriendsService {
    private var friendResponse: FriendResponse?
    private var countOffset: Int = 0

    func getFriends(userId: Int, completion: @escaping (FriendResponse) -> Void, failed: @escaping (String) -> Void) {
        Api.Friends.get(userId: userId, response: { [weak self] (friends) in
            self?.friendResponse = friends
            guard let friendResponse = self?.friendResponse else { return }
            completion(friendResponse)
        }) { error in
            failed(error)
        }
    }
    
    func getFollowers(userId: Int, completion: @escaping (FriendResponse) -> Void, failed: @escaping (String) -> Void) {
        Api.Users.getFollowers(userId: userId, response: { [weak self] (friends) in
            self?.friendResponse = friends
            guard let friendResponse = self?.friendResponse else { return }
            completion(friendResponse)
        }) { error in
            failed(error)
        }
    }
    
    func getNextFollowers(userId: Int, completion: @escaping (FriendResponse) -> Void, failed: @escaping (String) -> Void) {
        guard friendResponse?.items?.count ?? 0 < friendResponse?.count ?? 0 else {
            failed(getStringByDeclension(number: friendResponse?.count ?? 0, arrayWords: Localization.instance.followersString))
            return
        }
        countOffset = friendResponse?.items?.count ?? 0
        Api.Users.getFollowers(userId: userId, offset: countOffset, response: { [weak self] (friends) in
            guard let friends = friends else { return }
            guard self?.friendResponse?.items?.count != friends.count else { return }
            
            if self?.friendResponse == nil {
                self?.friendResponse = friends
            } else {
                self?.friendResponse?.items?.append(contentsOf: friends.items ?? [])
            }
            
            guard let friendResponse = self?.friendResponse else { return }
            completion(friendResponse)
        },failed: { error in
            failed(error)
        })
    }
}
