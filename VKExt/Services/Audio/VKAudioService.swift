//
//  VKAudioURLService.swift
//  VKExt
//
//  Created by programmist_NA on 27.05.2020.
//

import Foundation
import Alamofire
import SwiftyJSON
import VKAudioToken
import ObjectMapper
import KDEAudioPlayer
import UIKit
import Material
import SwiftMessages

typealias startPlayer = (_ item: AudioItem) -> Void

class VKAudioService: NSObject {
    static let instance: VKAudioService = VKAudioService()
    var player: VKAudioPlayer!
    var miniPlayer: MiniPlayer = MiniPlayer(frame: CGRect(origin: .zero, size: CGSize(width: Screen.bounds.width, height: 42)))
    var isActive: Bool = false
    var statusConfig = SwiftMessages.defaultConfig
    
    func initPlayer(itemsUrl: [URL]) {
        self.player = VKAudioPlayer(delegate: self, items: itemsUrl.map { VKAudioPlayerItem(URL: $0) })
        self.miniPlayer.cancelButton.addTarget(self, action: #selector(closePlayer), for: .touchUpInside)
        self.miniPlayer.playButton.addTarget(self, action: #selector(playPause), for: .touchUpInside)
    }
    
    func getAudio(token: String, secret: String, ownerId: Int, success: @escaping(_ model: JSON) -> Void, fail: @escaping(_ code: Int) -> Void) {
        CommonService.instance.performRequest(method: "audio.get", postFields: ["owner_id" : "\(ownerId)", "access_token" : token, "v" : "5.90"], secret: secret, success: { (response) in
            let json = (response as! NSDictionary).bv_jsonString(withPrettyPrint: true)
            if let dataFromString = json?.data(using: .utf8, allowLossyConversion: false) {
                let jsonArray = try? JSON(data: dataFromString)
                if jsonArray?["response"] != JSON.null {
                    success(jsonArray!["response"])
                } else {
                    guard let errorJSON = jsonArray?["error"] else { return }
                    fail(errorJSON["error_code"].intValue)
                }
            }
        }) { (error) in
            print(error)
        }
    }
}
extension VKAudioService: VKAudioPlayerDelegate {
    func vkAudioPlayerStateDidChange(_ vkAudioPlayer: VKAudioPlayer) {
        
    }
    
    func vkAudioPlayerPlaybackProgressDidChange(_ vkAudioPlayer: VKAudioPlayer) {
        
    }
    
    func vkAudioPlayerDidLoadItem(_ vkAudioPlayer: VKAudioPlayer, item: VKAudioPlayerItem) {
        miniPlayer.titleSong.text = item.meta.title
        print("VKAudioPlayer did load: \(item.URL.lastPathComponent)")
    }
    
    func vkAudioPlayerDidUpdateMetadata(_ vkAudioPlayer: VKAudioPlayer, forItem: VKAudioPlayerItem) {
        print("Item updated:\n\(forItem)")
    }
    
    func play() {
        isActive = true
        player.play()
    }
    
    func pause() {
        isActive = false
        player.pause()
    }
    
    @objc func closePlayer(_ sender: Any?) {
        if !isActive {
            player.stop()
            SwiftMessages.hideAll()
            self.miniPlayer.playButton.image = UIImage(named: "pause_48")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableDarkGrayVK)?.resize(toWidth: 20)?.resize(toHeight: 20)
            self.miniPlayer.cancelButton.image = UIImage(named: "skip_next_48")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableBlue)?.resize(toWidth: 20)?.resize(toHeight: 20)
            BottomNavigationViewController.playerView.isUserInteractionEnabled = false
            NotificationCenter.default.post(name: NSNotification.Name("playerStop"), object: nil)
        } else {
            player.playNext()
        }
    }
    
    @objc func playPause(_ sender: Any?) {
        if !isActive {
            play()
            self.miniPlayer.playButton.image = UIImage(named: "pause_48")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableDarkGrayVK)?.resize(toWidth: 20)?.resize(toHeight: 20)
            self.miniPlayer.cancelButton.image = UIImage(named: "skip_next_48")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableBlue)?.resize(toWidth: 32)?.resize(toHeight: 32)
        } else {
            pause()
            self.miniPlayer.playButton.image = UIImage(named: "play_48")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableBlue)?.resize(toWidth: 20)?.resize(toHeight: 20)
            self.miniPlayer.cancelButton.image = UIImage(named: "cancel_outline_36")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableDarkGrayVK)?.resize(toWidth: 32)?.resize(toHeight: 32)
        }
    }
    
    func startMiniPlayer(title: String, artist: String) {
        NotificationCenter.default.post(name: NSNotification.Name("playerStart"), object: nil)
        statusConfig.presentationContext = .view(BottomNavigationViewController.playerView)
        statusConfig.presentationStyle = .bottom
        statusConfig.duration = .forever
        statusConfig.interactiveHide = false
        miniPlayer.setup(title: title, artist: artist)
        SwiftMessages.show(config: statusConfig, view: miniPlayer)
    }
}

enum TestAudioItem {
    enum Model {
        struct Request {
            enum RequestType {
                case getAudio
                case getNextBatch
            }
        }
        struct Response {
            enum ResponseType {
                case presentAudio(audio: AudioResponse)
                case presentFooterLoader
                case presentFooterError(message: String)
            }
        }
        struct ViewModel {
            enum ViewModelData {
                case displayAudio(audioViewModel: AudioViewModel)
                case displayFooterLoader
                case displayFooterError(message: String)
            }
        }
    }
}

struct AudioViewModel {
    struct Cell: AudioCellViewModel {
        let title: String
        let subtitle: String
        let id: Int
        let date: Int
        let url: String
        let artist: String
        let ownerId: Int
        let duration: Int
        let album: AudioAlbum
        let isExplicit: Bool?
        let isLicensed: Bool?
    }
    var cells: [Cell]
    let footerTitle: String?
}

protocol AudioCellViewModel {
    var title: String { get }
    var subtitle: String { get }
    var id: Int { get }
    var date: Int { get }
    var url: String { get }
    var artist: String { get }
    var ownerId: Int { get }
    var duration: Int { get }
    var album: AudioAlbum { get }
    var isExplicit: Bool? { get }
    var isLicensed: Bool? { get }
}

class AudioLoaderService {
    private var audioResponse: AudioObject?
    private var newFromInProcess: String?

    func getAudio(token: String, secret: String, ownerId: Int, completion: @escaping (AudioObject) -> Void, failed: @escaping (String) -> Void) {
        Api.Audio.get(token: token, secret: secret, ownerId: ownerId, response: { (response) in
            self.audioResponse = response
            guard let audioResponse = self.audioResponse else { return }
            completion(audioResponse)
        }, failed: { error in
            failed(error)
        })
    }
}
