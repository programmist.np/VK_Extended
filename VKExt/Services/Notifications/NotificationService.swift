//
//  NotificationService.swift
//  VKExt
//
//  Created by programmist_NA on 22.05.2020.
//

import Foundation
import UIKit
import UserNotifications
import SwiftyVK
import SwiftyJSON

class NotificationService: NSObject {
    static let instance = NotificationService()
    let notificationCenter = UNUserNotificationCenter.current()

    func registerForPushNotifications() {
        notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { [weak self] (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else {
                print("Please enable \"Notifications\" from App Settings.")
                self?.showPermissionAlert()
                return
            }
            self?.getNotificationSettings()
        }

        let answerAction = UNTextInputNotificationAction(identifier: "answer", title: "Ответить", textInputButtonTitle: "Отправить", textInputPlaceholder: "Сообщение")
        let readAction = UNNotificationAction(identifier: "read", title: "Прочитать", options: [])
        
        let category = UNNotificationCategory(identifier: "Push Message Action", actions: [answerAction, readAction], intentIdentifiers: [], options: [])
        notificationCenter.setNotificationCategories([category])
        notificationCenter.delegate = self
    }
    
    func getNotificationSettings() {
        notificationCenter.getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                guard !UIApplication.shared.isRegisteredForRemoteNotifications else { return }
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func showPermissionAlert() {
        let alert = UIAlertController(title: "WARNING", message: "Please enable access to Notifications in the Settings app.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { [weak self] (alertAction) in
            self?.gotoAppSettings()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    private func gotoAppSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.canOpenURL(settingsUrl)
        }
    }
}
extension NotificationService: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print(notification.request.content)
        completionHandler([.alert, .sound, .badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
            print(response.notification.request.content.body, response.notification.request.content.title, response.notification.request.content.subtitle)
        case UNNotificationDefaultActionIdentifier:
            print("Default")
            print(response.notification.request.content.body, response.notification.request.content.title, response.notification.request.content.subtitle)
        case "answer":
            if let textResponse =  response as? UNTextInputNotificationResponse {
                let sendText =  textResponse.userText
                let peerId = response.notification.request.content.userInfo["peerId"] as! Int
                let randomId = Int64.random(in: 0...Int64.max)
                VK.API.Messages.send([.message: sendText, .peerId: "\(peerId)", .randomId: "\(randomId)"])
                .configure(with: Config.init(httpMethod: .POST, language: Language(rawValue: "ru")))
                .onError { vkError in
                    print("error: \(vkError.localizedDescription)")
                }
                .send()
            }
        case "read":
            print("Delete")
        default:
            print("Unknown action")
        }
        completionHandler()
    }
}
