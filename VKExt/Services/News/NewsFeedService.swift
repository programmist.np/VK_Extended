//
//  NewsFeedService.swift
//  VKExt
//
//  Created by programmist_NA on 11.07.2020.
//

import Foundation

class NewsfeedService {
    private var revealedPostIds = [Int]()
    private var feedResponse: FeedResponse?
    private var suggestionsResponse: FeedResponse?
    private var storiesResponse: HistoryResponse?
    private var newFromInProcess: String?

    func getFeed(completion: @escaping ([Int], FeedResponse) -> Void, failed: @escaping (String) -> Void) {
        Api.NewsFeed.get(nextBatchFrom: nil, response: { [weak self] (feed) in
            self?.feedResponse = feed
            guard let feedResponse = self?.feedResponse else { return }
            completion(self!.revealedPostIds, feedResponse)
            },failed: { error in
                failed(error)
        })
    }
    
    func getSuggestions(completion: @escaping ([Int], FeedResponse) -> Void, failed: @escaping (String) -> Void) {
        Api.NewsFeed.getRecommended(nextBatchFrom: nil, response: { [weak self] (feed) in
            self?.suggestionsResponse = feed
            guard let suggestionsResponse = self?.suggestionsResponse else { return }
            completion(self!.revealedPostIds, suggestionsResponse)
        },failed: { error in
                failed(error)
        })
    }
    
    func revealPostIds(forPostId postId: Int, completion: @escaping ([Int], FeedResponse) -> Void) {
        revealedPostIds.append(postId)
        guard let feedResponse = self.feedResponse else { return }
        completion(revealedPostIds, feedResponse)
    }
    
    func getNextBatch(completion: @escaping ([Int], FeedResponse) -> Void, failed: @escaping (String) -> Void) {
        newFromInProcess = feedResponse?.nextFrom
        Api.NewsFeed.get(nextBatchFrom: newFromInProcess, response: { [weak self] (feed) in
            guard let feed = feed else { return }
            guard self?.feedResponse?.nextFrom != feed.nextFrom else { return }
            
            if self?.feedResponse == nil {
                self?.feedResponse = feed
            } else {
                self?.feedResponse?.items.append(contentsOf: feed.items)
                
                var profiles = feed.profiles
                if let oldProfiles = self?.feedResponse?.profiles {
                    let oldProfilesFiltered = oldProfiles.filter({ (oldProfile) -> Bool in
                        !feed.profiles.contains(where: { $0.id == oldProfile.id })
                    })
                    profiles.append(contentsOf: oldProfilesFiltered)
                }
                self?.feedResponse?.profiles = profiles
                
                var groups = feed.groups
                if let oldGroups = self?.feedResponse?.groups {
                    let oldGroupsFiltered = oldGroups.filter({ (oldGroup) -> Bool in
                        !feed.groups.contains(where: { $0.id == oldGroup.id })
                    })
                    groups.append(contentsOf: oldGroupsFiltered)
                }
                self?.feedResponse?.groups = groups
                self?.feedResponse?.nextFrom = feed.nextFrom
            }
            
            guard let feedResponse = self?.feedResponse else { return }
            completion(self!.revealedPostIds, feedResponse)
        },failed: { error in
                failed(error)
        })
    }
    
    func getNextBatchRecomendations(completion: @escaping ([Int], FeedResponse) -> Void, failed: @escaping (String) -> Void) {
        newFromInProcess = feedResponse?.nextFrom
        Api.NewsFeed.getRecommended(nextBatchFrom: newFromInProcess, response: { [weak self] (feed) in
            guard let feed = feed else { return }
            guard self?.suggestionsResponse?.nextFrom != feed.nextFrom else { return }
            
            if self?.suggestionsResponse == nil {
                self?.suggestionsResponse = feed
            } else {
                self?.suggestionsResponse?.items.append(contentsOf: feed.items)
                
                var profiles = feed.profiles
                if let oldProfiles = self?.suggestionsResponse?.profiles {
                    let oldProfilesFiltered = oldProfiles.filter({ (oldProfile) -> Bool in
                        !feed.profiles.contains(where: { $0.id == oldProfile.id })
                    })
                    profiles.append(contentsOf: oldProfilesFiltered)
                }
                self?.suggestionsResponse?.profiles = profiles
                
                var groups = feed.groups
                if let oldGroups = self?.suggestionsResponse?.groups {
                    let oldGroupsFiltered = oldGroups.filter({ (oldGroup) -> Bool in
                        !feed.groups.contains(where: { $0.id == oldGroup.id })
                    })
                    groups.append(contentsOf: oldGroupsFiltered)
                }
                self?.suggestionsResponse?.groups = groups
                self?.suggestionsResponse?.nextFrom = feed.nextFrom
            }
            
            guard let suggestionsResponse = self?.suggestionsResponse else { return }
            completion(self!.revealedPostIds, suggestionsResponse)
        },failed: { error in
            failed(error)
        })
    }
    
    func addLikePost(type: String, ownerId: Int, itemId: Int, completion: (() -> ())? = nil) {
        Api.Likes.add(type: type, ownerId: ownerId, itemId: itemId) {
            guard let completion = completion else { return }
            completion()
        }
    }
    
    func deleteLikePost(type: String, ownerId: Int, itemId: Int, completion: (() -> ())? = nil) {
        Api.Likes.delete(type: type, ownerId: ownerId, itemId: itemId) {
            guard let completion = completion else { return }
            completion()
        }
    }
    
    func getStories(completion: @escaping ([Int], HistoryResponse) -> Void) {
        Api.Stories.get() { [weak self] (stories) in
            self?.storiesResponse = stories
            guard let storiesResponse = self?.storiesResponse else { return }
            completion(self!.revealedPostIds, storiesResponse)
        }
    }
}
