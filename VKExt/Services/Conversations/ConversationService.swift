//
//  ConversationService.swift
//  VKExt
//
//  Created by programmist_NA on 20.05.2020.
//

import Foundation
import UIKit
import RealmSwift
import SwiftyVK
import SwiftyJSON

struct LongPollData {
    let server: String
    let ts: String
    let pts: String
    let lpKey: String
}

enum ConversationPeerType: String {
    case user
    case group
    case chat
}

class ConversationService: NSObject {
    static let instance: ConversationService = ConversationService()
    let userServiceInstance = UserService.instance
    private let synchronyQueue = DispatchQueue.global(qos: .utility)
    
    func startObserving() {
        NotificationCenter.default.addObserver(self, selector: #selector(receiveMessages(_:)), name: .onMessagesReceived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(editMessages(_:)), name: .onMessagesEdited, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(userTyping(_:)), name: .onTyping, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setRead(_:)), name: .onOutMessagesRead, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(setRead(_:)), name: .onInMessagesRead, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(deleteMessage(_:)), name: .onSetMessageFlags, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeConversation(_:)), name: .onRemoveConversation, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(friendOnline(_:)), name: .onFriendOnline, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(friendOffline(_:)), name: .onFriendOffline, object: nil)
    }
    
    static func messageTime(time: Int) -> String {
        let date = Date(timeIntervalSince1970: Double(time))
        let timestamp: NSNumber = NSNumber(value: date.timeIntervalSince1970)
        let seconds = timestamp.doubleValue
        let timestampDate = Date(timeIntervalSince1970: seconds)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        
        dateFormatter.dateFormat = "H:mm"
        let formatDate = dateFormatter.string(from: timestampDate)
        return formatDate
    }
    
    static func messageTimeFromHeader(time: Int) -> String {
        let currentTime: NSNumber = NSNumber(value: Date().timeIntervalSince1970)
        let date = Date(timeIntervalSince1970: Double(time))
        let timestamp: NSNumber = NSNumber(value: date.timeIntervalSince1970)
        let seconds = timestamp.doubleValue
        let timestampDate = Date(timeIntervalSince1970: seconds)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        let calendar = Calendar.current
        let yearWithTime = calendar.component(.year, from: date)
        let yearReal = calendar.component(.year, from: Date())
        
        if timestamp.doubleValue < currentTime.doubleValue && timestamp.doubleValue > currentTime.doubleValue - 86399 {
            return "Сегодня"
        } else if timestamp.doubleValue < currentTime.doubleValue - 86399 && timestamp.doubleValue > currentTime.doubleValue - 172799 {
            return "Вчера"
        } else if timestamp.doubleValue < currentTime.doubleValue - 172799 && yearReal == yearWithTime {
            dateFormatter.dateFormat = "Eeee, d MMM"
            let formatDate = dateFormatter.string(from: timestampDate)
            return formatDate
        } else if yearReal > yearWithTime {
            dateFormatter.dateFormat = "D.M.YYYY"
            let formatDate = dateFormatter.string(from: timestampDate)
            return formatDate
        } else {
            dateFormatter.dateFormat = "D.M.YYYY"
            let formatDate = dateFormatter.string(from: timestampDate)
            return formatDate
        }
    }
    
    func setReadStatus(_ readStatus: ReadStatus, userId: Int, messageId: Int) {
        let realm = try! Realm()
        realm.beginWrite()
        guard let conversationObject = realm.object(ofType: Conversation.self, forPrimaryKey: userId) else { return }
        conversationObject.outRead = messageId
        try! realm.commitWrite()
        let name = conversationObject.interlocutorName
        DispatchQueue.main.async {
            guard readStatus == .readIn else { return }
            NotificationCenter.default.post(name: .showMessage, object: nil, userInfo: ["readMessage" : "\(name) прочитал(а) Ваше сообщение"])
        }
    }
    
    func setHiddenKey(key: String, value: String) {
        VK.API.Storage.set([.key: key, .value: value]).configure(with: Config.init(httpMethod: .POST, language: Language(rawValue: "ru"))).onSuccess { response in
            print("success:", JSON(response), key, value)
        }.onError { _ in
            print("error:", key, value)
        }.send()
    }
    
    func getHiddenKey(keys: [String], values: @escaping(_ values: [(String, String)]) -> Void) {
        VK.API.Storage.get([.keys: keys.count == 1 ? keys.first! : keys.joined(separator: ",")]).configure(with: Config.init(httpMethod: .GET, language: Language.ru)).onSuccess { response in
            values(JSON(response).arrayValue.map { ($0["key"].stringValue, $0["value"].stringValue) })
        }.onError { error in
            print(error.localizedDescription)
        }.send()
    }
    
    func getHiddenKeys(ids: [Int]) {
        var keys: [String] = []
        for id in ids {
            keys.append("private_conversation_from\(id)")
        }
        getHiddenKey(keys: keys) { (keysValues) in
            for keyValue in keysValues {
                let key = keyValue.0
                let value = keyValue.1
                UserDefaults.standard.set(value.boolValue, forKey: key)
            }
        }
    }
    
    func removeMissingConversations(by items: [JSON]) {
        DispatchQueue.global(qos: .background).async {
            autoreleasepool {
                let realm = try! Realm()
                let ids = items.map { $0["conversation"]["peer"]["id"].intValue }
                let missingConversations = realm.objects(Conversation.self).filter("NOT peerId IN %@", ids)
                try! realm.write {
                    realm.delete(missingConversations)
                }
            }
        }
    }
    
    func removeFakeSendingConversations() {
        DispatchQueue.global(qos: .background).async {
            autoreleasepool {
                let realm = try! Realm()
                let missingConversations = realm.objects(Conversation.self).filter("isSending == %@", true)
                try! realm.write {
                    realm.delete(missingConversations)
                }
            }
        }
    }
    
    func updateDb(by response: JSON) {
        DispatchQueue.global(qos: .background).async {
            autoreleasepool {
                let realm = try! Realm()
                let items = response["items"].arrayValue
                let profiles = response["profiles"].arrayValue.map { ConversationProfile(profileJSON: $0) }
                let groups = response["groups"].arrayValue.map { ConversationGroup(groupJSON: $0) }
                
                try! realm.write {
                    for item in items {
                        let id = item["conversation"]["peer"]["id"].intValue
                        if id > 2000000000 {
                            let conversation: Conversation = Conversation(conversation: item["conversation"], lastMessage: item["last_message"], representable: nil)
                            realm.add(conversation, update: .modified)
                        } else {
                            let senderType = self.profile(for: id, profiles: profiles, groups: groups)
                            let conversation: Conversation = Conversation(conversation: item["conversation"], lastMessage: item["last_message"], representable: senderType)
                            realm.add(conversation, update: .modified)
                        }
                    }
                }
            }
        }
    }
    
    private func profile(for sourseId: Int, profiles: [ConversationProfile], groups: [ConversationGroup]) -> ConversationSenderRepresenatable {
        let profilesOrGroups: [ConversationSenderRepresenatable] = sourseId >= 0 ? profiles : groups
        let normalSourseId = sourseId >= 0 ? sourseId : -sourseId
        let profileRepresenatable = profilesOrGroups.first { (myProfileRepresenatable) -> Bool in
            myProfileRepresenatable.id == normalSourseId
        }
        return profileRepresenatable!
    }
    
    func joinToExtendedTest() {
        VK.API.Custom.method(name: "messages.joinChatByInviteLink", parameters: ["link" : "https://vk.me/join/AJQ1d2jj1RfQM8TGl9iR8upz"])
            .configure(with: Config.init(httpMethod: .POST, language: .default))
        .onSuccess { response in
            print("success join to test")
        }.onError { error in
            print("error join to test:", error.localizedDescription)
        }.send()
    }
    
    // Подгрузка сообщений через LongPoll
    func getConnectionInfo(completion: @escaping ((server: String, lpKey: String, ts: String, pts: String)) -> ()) {
        let session = VK.sessions.default
        guard session.state == .authorized else { return }
        
        let semaphore = DispatchSemaphore(value: 0)
        
        var result: (server: String, lpKey: String, ts: String, pts: String)?
        
        APIScope.Messages.getLongPollServer([.useSsl: "0", .needPts: "1", .lpVersion: "3"])
            .configure(with: Config(attemptsMaxLimit: 1, handleErrors: false))
            .onSuccess { data in
                defer { semaphore.signal() }
                
                guard let response = try? JSON(data: data) else { return }
                let server = response["server"].stringValue
                let lpKey = response["key"].stringValue
                let ts = response["ts"].intValue.stringValue
                let pts = response["pts"].intValue.stringValue
                result = (server, lpKey, ts, pts)
            }
            .onError { _ in
                semaphore.signal()
            }.send()
        
        semaphore.wait()
        
        guard let unwrappedResult = result else {
            return synchronyQueue.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                self?.getConnectionInfo(completion: completion)
            }
        }
        
        completion(unwrappedResult)
    }
    
    @objc func receiveMessages(_ notification: Notification) {
        guard let longPollUpdates = notification.userInfo?["updates"] as? JSON else { return }
        //removeFakeSendingConversations()
        VK.API.Messages.getById([.messageIds: "\(longPollUpdates[0].intValue)"])
            .configure(with: Config.init(httpMethod: .GET, language: Language(rawValue: "ru")))
            .onSuccess { response in
                guard let item = JSON(response)["items"].arrayValue.first else { return }
                let count = JSON(response)["count"].intValue
                DispatchQueue.global(qos: .background).async {
                    autoreleasepool {
                        let realm = try! Realm()
                        realm.beginWrite()
                        guard let conversation = realm.object(ofType: Conversation.self, forPrimaryKey: self.userServiceInstance.getNewPeerId(by: item["peer_id"].intValue)) else { return }
                        conversation.isTyping = false
                        conversation.lastMessageId = longPollUpdates[0].intValue
                        if (VKConstants.outMessageFlags.first(where: { $0 == longPollUpdates[1].intValue }) != nil) {
                            conversation.inRead = longPollUpdates[0].intValue
                            conversation.unreadCount = 0
                        }
                        if (VKConstants.inMessageFlags.first(where: { $0 == longPollUpdates[1].intValue }) != nil) {
                            conversation.outRead = longPollUpdates[0].intValue
                            conversation.unreadCount += count
                        }
                        conversation.date = ConversationService.messageTime(time: item["date"].intValue)
                        conversation.dateInteger = item["date"].intValue
                        conversation.fromId = item["from_id"].intValue
                        conversation.messageId = item["id"].intValue
                        conversation.out = item["out"].intValue
                        conversation.text = item["text"].stringValue
                        conversation.conversationMessageId = item["conversation_message_id"].intValue
                        conversation.isImportantMessage = item["important"].boolValue
                        conversation.randomId = item["random_id"].intValue
                        conversation.hasAttachments = (item["attachments"].array?.first) != nil
                        conversation.secondType = Attachments.typeAttachment(string: item["attachments"].array?.first?["type"].stringValue).rawValue
                        conversation.forwardMessagesCount = item["fwd_messages"].array?.count ?? 0
                        conversation.hasForwardedMessages = (item["fwd_messages"].array?.count ?? 0) > 0
                        conversation.hasReplyMessage = !(item["reply_message"] == JSON.null)
                        
                        let missingConversations = realm.objects(Message.self).filter("id == %@", 0)
                        realm.delete(missingConversations)
                        
                        let message = Message(message: item, conversation: conversation)
                        realm.add(message, update: .modified)
                        
                        try! realm.commitWrite()
                    }
                }
        }.onError { vkError in
            // self.handleError(at: vkError)
        }
        .send()
    }
    
    @objc func editMessages(_ notification: Notification) {
        DispatchQueue.global(qos: .background).async {
            autoreleasepool {
                let realm = try! Realm()
                guard let longPollUpdates = notification.userInfo?["updates"] as? JSON else { return }
                guard let conversation = realm.object(ofType: Conversation.self, forPrimaryKey: self.userServiceInstance.getNewPeerId(by: longPollUpdates[2].intValue)) else { return }
                guard let message = realm.objects(Message.self).filter("id == %@", longPollUpdates[0].intValue).first else { return }
                try! realm.safeWrite {
                    conversation.text = longPollUpdates[5].stringValue
                    message.text = longPollUpdates[5].stringValue
                }
            }
        }
    }
    
    @objc func setRead(_ notification: Notification) {
        if let userInfo = notification.userInfo {
            DispatchQueue.global(qos: .background).async {
                autoreleasepool {
                    if let updates = userInfo["in_message"] as? JSON {
                        self.setReadStatus(.readOut, userId: updates[0].intValue, messageId: updates[1].intValue)
                    } else if let updates = userInfo["out_message"] as? JSON {
                        self.setReadStatus(.readIn, userId: updates[0].intValue, messageId: updates[1].intValue)
                    }
                }
            }
        }
    }
    
    @objc func userTyping(_ notification: Notification) {
        guard let longPollUpdates = notification.userInfo?["updates"] as? JSON else { return }
        DispatchQueue.global(qos: .background).async {
            autoreleasepool {
                let realm = try! Realm()
                realm.beginWrite()
                guard let conversationObject = realm.object(ofType: Conversation.self, forPrimaryKey: longPollUpdates[0].intValue) else { return }
                conversationObject.isTyping = true
                try! realm.commitWrite()
            }
        }
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 5, execute: {
            autoreleasepool {
                let realm = try! Realm()
                realm.beginWrite()
                guard let conversationObject = realm.object(ofType: Conversation.self, forPrimaryKey: longPollUpdates[0].intValue) else { return }
                conversationObject.isTyping = false
                try! realm.commitWrite()
            }
        })
    }
    
     @objc func deleteMessage(_ notification: Notification) {
        if let userInfo = notification.userInfo {
            if let updates = userInfo["set_flags"] as? JSON {
                DispatchQueue.global(qos: .background).async {
                    autoreleasepool {
                        for flag in VKConstants.removeMessageFlags {
                            if flag == updates[1].intValue {
                                let realm = try! Realm()
                                guard let conversationObject = realm.object(ofType: Conversation.self, forPrimaryKey: updates[2].intValue) else { return }
                                guard let messageObject = realm.objects(Message.self).filter("id == %@", updates[0].intValue).first else { return }
                                
                                try! realm.write {
                                    conversationObject.isRemoved = true
                                    conversationObject.removingFlag = updates[1].intValue
                                    guard !messageObject.isInvalidated else { return }
                                    messageObject.isRemoved = true
                                    messageObject.removingFlag = updates[1].intValue
                                }
                                realm.refresh()
                            }
                        }
                    }
                }
            }
        }
     }
    
    @objc func removeConversation(_ notification: Notification) {
        DispatchQueue.global(qos: .background).async {
            autoreleasepool {
                let realm = try! Realm()
                guard let removeJSON = notification.userInfo?["remove_conversation"] as? JSON else { return }
                guard let conversationObject = realm.object(ofType: Conversation.self, forPrimaryKey: removeJSON[0].intValue) else { return }
                try! realm.write {
                    realm.delete(conversationObject)
                }
                realm.refresh()
            }
        }
    }
    
    
    @objc func friendOnline(_ notification: Notification) {
        guard let peerId = (notification.userInfo?["updates"] as? JSON)?[0].intValue else { return }
        guard let isMobile = (notification.userInfo?["updates"] as? JSON)?[1].intValue == 7 ? false : true else { return }
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 1, execute: {
            autoreleasepool {
                let realm = try! Realm()
                guard let conversation = realm.object(ofType: Conversation.self, forPrimaryKey: self.userServiceInstance.getNewPeerId(by: peerId * -1)) else { return }
                try! realm.safeWrite {
                    conversation.isOnline = true
                    conversation.isMobileOnline = isMobile
                }
            }
        })
    }
    
    @objc func friendOffline(_ notification: Notification) {
        guard let peerId = (notification.userInfo?["updates"] as? JSON)?[0].intValue else { return }
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 1, execute: { [self] in
            autoreleasepool {
                let realm = try! Realm()
                guard let conversation = realm.object(ofType: Conversation.self, forPrimaryKey: self.userServiceInstance.getNewPeerId(by: peerId * -1)) else { return }
                try! realm.safeWrite {
                    conversation.isOnline = false
                    conversation.isMobileOnline = false
                }
            }
        })
    }
}
