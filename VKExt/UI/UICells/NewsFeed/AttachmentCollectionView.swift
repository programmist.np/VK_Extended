//
//  AttachmentCollectionView.swift
//  VKExt
//
//  Created by programmist_NA on 11.07.2020.
//

import Foundation
import UIKit
import Material
import Serrata

class GalleryCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var photos = [AttachmentCellViewModel]()
    
    init() {
        super.init(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        delegate = self
        dataSource = self
        
        backgroundColor = .adaptableWhite
        isScrollEnabled = false
        
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
        
        register(GalleryCollectionViewCell.self, forCellWithReuseIdentifier: GalleryCollectionViewCell.reuseId)
    }
    
    func generateLayout() -> UICollectionViewLayout {
        // Full
        let fullPhotoItem = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalWidth(2/3)),
            supplementaryItems: [])
        fullPhotoItem.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)

        // Main with pair
        let mainItem = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(2/3),
                heightDimension: .fractionalHeight(1.0)))
        mainItem.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)
        
        let pairItem = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalHeight(0.5)))
        pairItem.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)
        
        let trailingGroup = NSCollectionLayoutGroup.vertical(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1/3),
                heightDimension: .fractionalHeight(1.0)),
            subitem: pairItem,
            count: 2)
        
        let mainWithPairGroup = NSCollectionLayoutGroup.horizontal(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalWidth(4/9)),
            subitems: [mainItem, trailingGroup])
        
        // Triplet
        let tripletItem = NSCollectionLayoutItem(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1/3),
                heightDimension: .fractionalHeight(1.0)))
        tripletItem.contentInsets = NSDirectionalEdgeInsets(top: 2, leading: 2, bottom: 2, trailing: 2)
        
        let tripletGroup = NSCollectionLayoutGroup.horizontal(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalWidth(2/9)),
            subitems: [tripletItem, tripletItem, tripletItem])
        
        // Reversed main with pair
        let mainWithPairReversedGroup = NSCollectionLayoutGroup.horizontal(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1.0),
                heightDimension: .fractionalWidth(4/9)),
            subitems: [trailingGroup, mainItem])
        
        let nestedGroup: NSCollectionLayoutGroup
        if photos.count == 1 {
            nestedGroup = NSCollectionLayoutGroup.vertical(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalWidth(16/9)),
                subitems: [fullPhotoItem])
        } else if photos.count == 2 {
            nestedGroup = NSCollectionLayoutGroup.vertical(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalWidth(16/9)),
                subitems: [fullPhotoItem, fullPhotoItem])
        } else if photos.count == 3 {
            nestedGroup = NSCollectionLayoutGroup.vertical(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalWidth(16/9)),
                subitems: [mainWithPairGroup])
        } else if photos.count == 4 {
            nestedGroup = NSCollectionLayoutGroup.vertical(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalWidth(16/9)),
                subitems: [fullPhotoItem, mainWithPairGroup])
        } else if photos.count == 5 {
            nestedGroup = NSCollectionLayoutGroup.vertical(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalWidth(16/9)),
                subitems: [fullPhotoItem, fullPhotoItem, tripletGroup])
        }
        else if photos.count == 6 {
            nestedGroup = NSCollectionLayoutGroup.vertical(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalWidth(16/9)),
                subitems: [mainWithPairGroup, tripletGroup])
        }
        else if photos.count == 7 {
            nestedGroup = NSCollectionLayoutGroup.vertical(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalWidth(16/9)),
                subitems: [fullPhotoItem, mainWithPairGroup, tripletGroup])
        }
        else if photos.count == 8 {
            nestedGroup = NSCollectionLayoutGroup.vertical(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalWidth(16/9)),
                subitems: [fullPhotoItem, fullPhotoItem, tripletGroup, fullPhotoItem])
        }
        else if photos.count == 9 {
            nestedGroup = NSCollectionLayoutGroup.vertical(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalWidth(16/9)),
                subitems: [mainWithPairGroup, tripletGroup, mainWithPairReversedGroup])
        } else {
            nestedGroup = NSCollectionLayoutGroup.vertical(
                layoutSize: NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalWidth(16/9)),
                subitems: [fullPhotoItem, mainWithPairGroup, tripletGroup, tripletGroup])
        }

        let section = NSCollectionLayoutSection(group: nestedGroup)
        let layout = UICollectionViewCompositionalLayout(section: section)
        return layout
    }
    
    func set(photos: [AttachmentCellViewModel]) {
        self.photos = photos
        contentOffset = CGPoint.zero
        reloadData()
        collectionViewLayout = generateLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dequeueReusableCell(withReuseIdentifier: GalleryCollectionViewCell.reuseId, for: indexPath) as! GalleryCollectionViewCell
        cell.set(imageUrl: photos[indexPath.item].photoUrlString)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selectedCell = collectionView.cellForItem(at: indexPath) as? GalleryCollectionViewCell else { return }
        guard let image = selectedCell.myImageView.image else { return }
        let slideLeafs = photos.enumerated().map { SlideLeaf(image: image, imageUrlString: $0.element.photoMaxUrl, title: "Фотография \($0.offset) из \(photos.count)", caption: "") }

        let slideImageViewController = SlideLeafViewController.make(leafs: slideLeafs, startIndex: indexPath.item, fromImageView: selectedCell.myImageView)
        slideImageViewController.delegate = self
        slideImageViewController.modalPresentationStyle = .fullScreen
        parentViewController?.present(slideImageViewController, animated: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
extension GalleryCollectionView: SlideLeafViewControllerDelegate {
    func slideLeafViewControllerDismissed(slideLeaf: SlideLeaf, pageIndex: Int) {
        print(slideLeaf)
        print(pageIndex)
    }
}

extension GalleryCollectionView: RowLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, photoAtIndexPath indexPath: IndexPath) -> CGSize {
        let width = photos[indexPath.row].width / 2
        let height = photos[indexPath.row].height / 2
        return CGSize(width: width, height: height)
    }
}
import RealmSwift

class StoriesCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var senders = [StoriesCellViewModel]()
    var primitiveUser = try! Realm().object(ofType: PrimitiveUser.self, forPrimaryKey: VKConstants.currentUserId)
    
    init() {
        let rowLayout = UICollectionViewFlowLayout()
        super.init(frame: .zero, collectionViewLayout: rowLayout)
        
        rowLayout.scrollDirection = .horizontal
        contentInset.left = 8
        
        delegate = self
        dataSource = self
        
        backgroundColor = .adaptableWhite
        
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
        
        register(StoryCollectionViewCell.self, forCellWithReuseIdentifier: StoryCollectionViewCell.reuseId)
        register(CreateStoryCollectionViewCell.self, forCellWithReuseIdentifier: CreateStoryCollectionViewCell.reuseId)
    }
    
    func set(senders: [StoriesCellViewModel]) {
        self.senders = senders
        contentInset.left = 8
        reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return senders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            let cell = dequeueReusableCell(withReuseIdentifier: CreateStoryCollectionViewCell.reuseId, for: indexPath) as! CreateStoryCollectionViewCell
            cell.set(dataUser: (primitiveUser?.photo100 ?? "", primitiveUser?.firstName ?? "Fuck :("))
            return cell
        } else {
            let cell = dequeueReusableCell(withReuseIdentifier: StoryCollectionViewCell.reuseId, for: indexPath) as! StoryCollectionViewCell
            cell.set(viewModel: senders[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 92)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ProfilePhotosCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var photos = [ProfilePhotosViewModel]()
    
    init() {
        let rowLayout = UICollectionViewFlowLayout()
        super.init(frame: .zero, collectionViewLayout: rowLayout)
        
        delegate = self
        dataSource = self
        
        backgroundColor = .adaptableWhite
        
        showsHorizontalScrollIndicator = false
        showsVerticalScrollIndicator = false
        
        rowLayout.scrollDirection = .horizontal
        
        register(GalleryCollectionViewCell.self, forCellWithReuseIdentifier: GalleryCollectionViewCell.reuseId)
    }
    
    func set(photos: [ProfilePhotosViewModel]) {
        self.photos = photos
        contentOffset = CGPoint.zero
        reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dequeueReusableCell(withReuseIdentifier: GalleryCollectionViewCell.reuseId, for: indexPath) as! GalleryCollectionViewCell
        cell.set(imageUrl: photos[indexPath.item].photoUrlString)
        cell.setCorners(radius: 10)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selectedCell = collectionView.cellForItem(at: indexPath) as? GalleryCollectionViewCell else { return }
        guard let image = selectedCell.myImageView.image else { return }
        let slideLeafs = photos.enumerated().map { SlideLeaf(image: image, imageUrlString: $0.element.photoMaxUrl, title: "Фотография \($0.offset) из \(photos.count)", caption: "") }

        let slideImageViewController = SlideLeafViewController.make(leafs: slideLeafs, startIndex: indexPath.item, fromImageView: selectedCell.myImageView)
        slideImageViewController.modalPresentationStyle = .fullScreen
        parentViewController?.present(slideImageViewController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.width / 3) - 4, height: (collectionView.bounds.width / 3) - 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 6
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
