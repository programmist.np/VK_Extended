//
//  AttachmentCollectionViewCell.swift
//  VKExt
//
//  Created by programmist_NA on 11.07.2020.
//

import Foundation
import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    static let reuseId = "GalleryCollectionViewCell"
    
    let myImageView: WebImageView = {
       let imageView = WebImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .adaptableBackground
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(myImageView)
        // myImageView constraints
        myImageView.fillSuperview()
    }
    
    override func prepareForReuse() {
        myImageView.image = nil
    }
    
    func set(imageUrl: String?) {
        myImageView.set(imageURL: imageUrl)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        myImageView.layer.masksToBounds = true
        myImageView.layer.cornerRadius = 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class StoryCollectionViewCell: UICollectionViewCell {
    
    static let reuseId = "StoryCollectionViewCell"
    
    let storyView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    let senderImageView: WebImageView = {
        let imageView = WebImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .adaptableBackground
        return imageView
    }()
    
    let selectorView: UIView = {
        let firstView = UIView()
        firstView.translatesAutoresizingMaskIntoConstraints = false
        firstView.backgroundColor = .clear
        firstView.autoSetDimensions(to: CGSize(width: 64, height: 64))
        firstView.layer.masksToBounds = true
        firstView.drawBorder(32, width: 2, color: .adaptableBlue, isOnlyTopCorners: false)
        let secondView = UIView()
        secondView.translatesAutoresizingMaskIntoConstraints = false
        secondView.backgroundColor = .clear
        secondView.autoSetDimensions(to: CGSize(width: 61, height: 61))
        secondView.layer.masksToBounds = true
        secondView.drawBorder(30.5, width: 2, color: .adaptableWhite, isOnlyTopCorners: false)
        firstView.addSubview(secondView)
        secondView.autoCenterInSuperview()
        return firstView
    }()
    
    let senderNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .adaptableDarkGrayVK
        label.font = GoogleSansFont.medium(with: 12)
        label.numberOfLines = 1
        label.textAlignment = .center
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupStoryView()
    }
    
    func setupStoryView() {
        addSubview(storyView)
        storyView.addSubview(senderImageView)
        storyView.addSubview(senderNameLabel)
        storyView.addSubview(selectorView)
        
        storyView.autoPinEdge(.top, to: .top, of: self, withOffset: 6)
        storyView.autoPinEdge(.trailing, to: .trailing, of: self)
        storyView.autoPinEdge(.leading, to: .leading, of: self)
        storyView.autoPinEdge(.bottom, to: .bottom, of: self, withOffset: -2)
        
        senderImageView.autoPinEdge(.top, to: .top, of: storyView)
        senderImageView.autoPinEdge(.trailing, to: .trailing, of: storyView, withOffset: -3)
        senderImageView.autoPinEdge(.leading, to: .leading, of: storyView, withOffset: 3)
        senderImageView.autoSetDimensions(to: CGSize(width: 64, height: 64))
        
        senderNameLabel.autoPinEdge(.top, to: .bottom, of: senderImageView)
        senderNameLabel.autoPinEdge(.trailing, to: .trailing, of: storyView, withOffset: -3)
        senderNameLabel.autoPinEdge(.leading, to: .leading, of: storyView, withOffset: 3)
        senderNameLabel.autoPinEdge(.bottom, to: .bottom, of: storyView)
        
        selectorView.autoPinEdge(.top, to: .top, of: storyView)
        selectorView.autoPinEdge(.trailing, to: .trailing, of: storyView, withOffset: -3)
        selectorView.autoPinEdge(.leading, to: .leading, of: storyView, withOffset: 3)
        selectorView.autoSetDimensions(to: CGSize(width: 64, height: 64))
    }
    
    override func prepareForReuse() {
        senderImageView.image = nil
        senderNameLabel.text = nil
    }
    
    func set(viewModel: StoriesCellViewModel) {
        senderImageView.set(imageURL: viewModel.photoUrl)
        senderNameLabel.text = viewModel.name
        if viewModel.seen == 0 {
            senderNameLabel.textColor = .adaptableBlue
            selectorView.drawBorder(32, width: 2, color: .adaptableBlue, isOnlyTopCorners: false)
        } else {
            senderNameLabel.textColor = .adaptableDarkGrayVK
            selectorView.drawBorder(32, width: 2, color: .adaptableWhite, isOnlyTopCorners: false)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        senderImageView.layer.masksToBounds = true
        senderImageView.layer.cornerRadius = 32
        
        selectorView.layer.masksToBounds = true
        selectorView.layer.cornerRadius = 32
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CreateStoryCollectionViewCell: UICollectionViewCell {
    
    static let reuseId = "CreateStoryCollectionViewCell"
    
    let storyView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    let senderImageView: WebImageView = {
        let imageView = WebImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = .adaptableBackground
        return imageView
    }()
    
    let senderNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .adaptableDarkGrayVK
        label.font = GoogleSansFont.medium(with: 12)
        label.numberOfLines = 1
        label.textAlignment = .center
        return label
    }()
    
    let addView: UIView = {
        let view = UIView()
        let imageView = UIImageView(image: UIImage(named: "add_16-1"))
        imageView.contentMode = .scaleAspectFit
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .extendedBlue
        view.addSubview(imageView)
        imageView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4))
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupStoryView()
    }
    
    func setupStoryView() {
        addSubview(storyView)
        storyView.addSubview(senderImageView)
        storyView.addSubview(senderNameLabel)
        storyView.addSubview(addView)
        
        storyView.autoPinEdge(.top, to: .top, of: self, withOffset: 6)
        storyView.autoPinEdge(.trailing, to: .trailing, of: self)
        storyView.autoPinEdge(.leading, to: .leading, of: self)
        storyView.autoPinEdge(.bottom, to: .bottom, of: self, withOffset: -2)
        
        senderImageView.autoPinEdge(.top, to: .top, of: storyView)
        senderImageView.autoPinEdge(.trailing, to: .trailing, of: storyView, withOffset: -3)
        senderImageView.autoPinEdge(.leading, to: .leading, of: storyView, withOffset: 3)
        senderImageView.autoSetDimensions(to: CGSize(width: 64, height: 64))
        
        senderNameLabel.autoPinEdge(.top, to: .bottom, of: senderImageView)
        senderNameLabel.autoPinEdge(.trailing, to: .trailing, of: storyView, withOffset: -3)
        senderNameLabel.autoPinEdge(.leading, to: .leading, of: storyView, withOffset: 3)
        senderNameLabel.autoPinEdge(.bottom, to: .bottom, of: storyView)
        
        addView.autoPinEdge(.trailing, to: .trailing, of: senderImageView)
        addView.autoPinEdge(.bottom, to: .bottom, of: senderImageView)
        addView.autoSetDimensions(to: CGSize(width: 18, height: 18))
    }
    
    override func prepareForReuse() {
        senderImageView.image = nil
        senderNameLabel.text = nil
    }
    
    func set(dataUser: (String, String)) {
        if dataUser.0 != "" && dataUser.1 != "" {
            senderImageView.set(imageURL: dataUser.0)
            senderNameLabel.text = dataUser.1
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        senderImageView.layer.masksToBounds = true
        senderImageView.layer.cornerRadius = 32
        
        addView.drawBorder(9, width: 2.5, color: .adaptableWhite, isOnlyTopCorners: false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
