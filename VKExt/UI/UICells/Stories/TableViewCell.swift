//
//  TableViewCell.swift
//  VKExt
//
//  Created by programmist_NA on 13.07.2020.
//

import UIKit

class TableViewCell: UITableViewCell {
    static let reuseId = "TableViewCell"
    let storiesCollectionView = StoriesCollectionView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(storiesCollectionView)
        backgroundColor = .adaptableWhite
        selectionStyle = .none
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        storiesCollectionView.drawBorder(10, width: 0.5, color: .adaptableDivider)
    }
    
    func set(viewModel: [StoriesCellViewModel]) {
        storiesCollectionView.isHidden = false
        storiesCollectionView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12))
        storiesCollectionView.set(senders: viewModel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
