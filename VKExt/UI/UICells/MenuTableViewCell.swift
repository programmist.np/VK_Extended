//
//  MenuTableViewCell.swift
//  VKExt
//
//  Created by programmist_NA on 25.05.2020.
//

import UIKit
import MaterialComponents

class MenuTableViewCell: UITableViewCell {
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var menuTitleLabel: UILabel!
    var rippleTouchController: MDCRippleTouchController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupLayer(title: String, image: UIImage, tint: UIColor) {
        self.backgroundColor = .adaptableWhite
        menuImageView.image = image.withRenderingMode(.alwaysTemplate).tint(with: tint)
        menuTitleLabel.textColor = .adaptableBlack
        menuTitleLabel.font = GoogleSansFont.regular(with: 17)
        menuTitleLabel.text = title
        
        rippleTouchController = MDCRippleTouchController(view: self)
        setupRipple(by: rippleTouchController)
    }
}
extension UITableViewCell: MDCRippleTouchControllerDelegate {
    public func rippleTouchController(_ rippleTouchController: MDCRippleTouchController, shouldProcessRippleTouchesAtTouchLocation location: CGPoint) -> Bool {
        return true
    }
    
    public func rippleTouchController(_ rippleTouchController: MDCRippleTouchController, insert rippleView: MDCRippleView, into view: UIView) {
        view.insertSubview(rippleView, at: 0)
    }
    
    public func rippleTouchController(_ rippleTouchController: MDCRippleTouchController, didProcessRippleView rippleView: MDCRippleView, atTouchLocation location: CGPoint) {
        print("Did process ripple view!")
    }
}
