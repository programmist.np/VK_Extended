//
//  Messageswift
//  VKExt
//
//  Created by programmist_NA on 01.06.2020.
//

import Foundation
import UIKit
import Material
import Lottie

class ChatLogMessageCell: BaseCell {
    let messageTextView: UITextView = {
        let textView = UITextView()
        textView.contentInset = UIEdgeInsets(top: -1, left: 8, bottom: 1, right: -8)
        textView.isScrollEnabled = false
        textView.font = GoogleSansFont.medium(with: 17)
        textView.text = " "
        textView.backgroundColor = .clear
        return textView
    }()
    
    let timeStampLabel: UILabel = {
        let label = UILabel()
        label.font = GoogleSansFont.regular(with: 12)
        label.textColor = .adaptableGrayVK
        return label
    }()
    
    let textBubbleView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 18
        view.layer.masksToBounds = true
        return view
    }()
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 16
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    static let grayBubbleImage = UIImage(named: "bubble_gray")!.resizableImage(withCapInsets: UIEdgeInsets(top: 22, left: 26, bottom: 22, right: 26)).withRenderingMode(.alwaysTemplate)
    static let blueBubbleImage = UIImage(named: "bubble_blue")!.resizableImage(withCapInsets: UIEdgeInsets(top: 22, left: 26, bottom: 22, right: 26)).withRenderingMode(.alwaysTemplate)
    
    let bubbleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = UIColor(white: 0.90, alpha: 1)
        imageView.setCorners(radius: 18)
        return imageView
    }()
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(textBubbleView)
        addSubview(messageTextView)
        addSubview(timeStampLabel)
        
        addSubview(profileImageView)
        addConstraintsWithFormat(format: "H:|-8-[v0(32)]", views: profileImageView)
        addConstraintsWithFormat(format: "V:[v0(32)]|", views: profileImageView)
        profileImageView.backgroundColor = .adaptableBackground
    }
    
}
class BaseCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        transform = GlobalConstants.transform
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
        transform = GlobalConstants.transform
    }
    
    func setupViews() {
    }
}
