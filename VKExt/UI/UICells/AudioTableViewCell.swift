//
//  AudioTableViewCell.swift
//  VKExt
//
//  Created by programmist_NA on 23.05.2020.
//

import UIKit
import MaterialComponents
import Kingfisher
import Material

class AudioTableViewCell: UITableViewCell {
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var imagePlaceholder: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var downloadView: UIView!
    let progressAudioDownload: MDCActivityIndicator = {
        let progressAudioDownload = MDCActivityIndicator()
        progressAudioDownload.radius = 16
        progressAudioDownload.strokeWidth = 3
        progressAudioDownload.indicatorMode = .determinate
        progressAudioDownload.cycleColors = [.white]
        progressAudioDownload.progress = 0
        progressAudioDownload.backgroundColor = UIColor.adaptableBlack.withAlphaComponent(0.3)
        return progressAudioDownload
    }()
    var rippleTouchController: MDCRippleTouchController!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = nil
        nameLabel.attributedText = nil
        artistLabel.text = nil
        durationLabel.text = nil
        imagePlaceholder.image = nil
    }
    
    func setupCell(audio: AudioCellViewModel) {
        setupLayer()
        if audio.isExplicit ?? false {
            if audio.subtitle != "" {
                nameLabel.attributedText = NSAttributedString(string: audio.title) + NSAttributedString(string: " \(audio.subtitle)", attributes: [.foregroundColor: UIColor.adaptableDarkGrayVK, .font: GoogleSansFont.regular(with: 16)]) + setLabelImage(image: "explicit_12")!
            } else {
                nameLabel.attributedText = NSAttributedString(string: audio.title) + setLabelImage(image: "explicit_12")!
            }
        } else {
            nameLabel.attributedText = NSAttributedString(string: audio.title) + NSAttributedString(string: " \(audio.subtitle)", attributes: [.foregroundColor: UIColor.adaptableDarkGrayVK, .font: GoogleSansFont.regular(with: 16)])
        }
        
        artistLabel.text = audio.artist
        nameLabel.sizeToFit()
        artistLabel.sizeToFit()
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute, .second]
        formatter.unitsStyle = .positional

        let formattedString = formatter.string(from: TimeInterval(Double(audio.duration)))
        durationLabel.text = formattedString
        durationLabel.sizeToFit()
        if audio.album.thumb.photo135 == "" {
            imagePlaceholder.image = UIImage(named: "music_note_24")?.withAlignmentRectInsets(UIEdgeInsets(top: -12, left: -12, bottom: -12, right: -12))
        } else {
            imagePlaceholder.kf.setImage(with: URL(string: audio.album.thumb.photo135))
        }
    }
    
    func setupLayer() {
        self.backgroundColor = .adaptableWhite
        imageContainerView.setCorners(radius: 4)
        imageContainerView.backgroundColor = UIColor.adaptableSeparator.withAlphaComponent(0.05)
        imagePlaceholder.setCorners(radius: 4)
        nameLabel.font = GoogleSansFont.regular(with: 16)
        artistLabel.font = GoogleSansFont.regular(with: 13)
        durationLabel.font = GoogleSansFont.regular(with: 13)
        nameLabel.textColor = .adaptableBlack
        artistLabel.textColor = .adaptableDarkGrayVK
        durationLabel.textColor = .adaptableGrayVK
        self.imageContainerView.addSubview(progressAudioDownload)
        progressAudioDownload.autoPinEdgesToSuperviewEdges()
        progressAudioDownload.setCorners(radius: 4)
        progressAudioDownload.isHidden = true
        downloadView.backgroundColor = .adaptableWhite
        downloadView.setRounded()
        downloadView.isHidden = true
    }
    
    func setProgressHidden(from value: Bool) {
        if !value {
            progressAudioDownload.startAnimating()
            progressAudioDownload.isHidden = false
            UIView.animate(withDuration: 0.4, delay: 0, options: .transitionCrossDissolve, animations: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.progressAudioDownload.alpha = 1.0
                strongSelf.progressAudioDownload.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: nil)
        } else {
            downloadView.isHidden = false
            UIView.animate(withDuration: 0.4, delay: 0, options: .transitionCrossDissolve, animations: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.progressAudioDownload.alpha = 0
                strongSelf.progressAudioDownload.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                strongSelf.downloadView.alpha = 1.0
                strongSelf.downloadView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: { _ in
                self.progressAudioDownload.isHidden = true
                self.progressAudioDownload.stopAnimating()
            })
        }
    }
    
    func setProgress(at value: Float) {
        progressAudioDownload.setProgress(value, animated: true)
    }
}
