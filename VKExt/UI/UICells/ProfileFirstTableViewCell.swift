//
//  ProfileFirstTableViewCell.swift
//  VKExt
//
//  Created by programmist_NA on 27.05.2020.
//

import UIKit
import Material
import MaterialComponents

class ProfileFirstTableViewCell: UITableViewCell {
    let profileName: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = GoogleSansFont.medium(with: 18)
        label.textColor = .adaptableBlack
        label.lineBreakMode = .byTruncatingTail
        return label
    }()
    let profileAvatar: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .adaptableSeparator
        return imageView
    }()
    let onlineView: OnlineView = OnlineView()
    let statusLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = GoogleSansFont.regular(with: 14)
        label.textColor = .adaptableGrayVK
        label.lineBreakMode = .byTruncatingTail
        return label
    }()
    let avatarButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setTitle("", for: .normal)
        return button
    }()
    let buttonPanel: ButtonPanelView = ButtonPanelView()
    
    var userId: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.backgroundColor = .adaptableWhite

        self.contentView.addSubview(profileAvatar)
        self.contentView.addSubview(avatarButton)
        profileAvatar.setRounded()
        self.contentView.addSubview(profileName)
        self.contentView.addSubview(statusLabel)
        self.contentView.addSubview(onlineView)
        self.contentView.addSubview(buttonPanel)

        profileAvatar.autoPinEdge(.top, to: .top, of: self.contentView, withOffset: 16)
        profileAvatar.autoPinEdge(.leading, to: .leading, of: self.contentView, withOffset: 16)
        profileAvatar.autoSetDimensions(to: CGSize(width: 72, height: 72))
        profileAvatar.contentMode = .scaleAspectFill
        
        avatarButton.autoPinEdge(.top, to: .top, of: self.contentView, withOffset: 16)
        avatarButton.autoPinEdge(.leading, to: .leading, of: self.contentView, withOffset: 16)
        avatarButton.autoSetDimensions(to: CGSize(width: 72, height: 72))
        
        profileName.autoPinEdge(.top, to: .top, of: self.contentView, withOffset: statusLabel.text == "" ? 32 : 24)
        profileName.autoPinEdge(.leading, to: .trailing, of: self.profileAvatar, withOffset: 16)
        profileName.autoPinEdge(.trailing, to: .trailing, of: self, withOffset: -16)
        
        statusLabel.autoPinEdge(.top, to: .bottom, of: self.profileName, withOffset: 0)
        statusLabel.autoPinEdge(.leading, to: .trailing, of: self.profileAvatar, withOffset: 16)
        statusLabel.autoPinEdge(.trailing, to: .trailing, of: self, withOffset: -16)
        
        onlineView.autoPinEdge(.top, to: .bottom, of: statusLabel.text == "" ? self.profileName : self.statusLabel, withOffset: -4)
        onlineView.autoPinEdge(.leading, to: .trailing, of: self.profileAvatar, withOffset: 16)
        onlineView.autoSetDimension(.height, toSize: 24)
        
        buttonPanel.autoPinEdge(.top, to: .bottom, of: self.profileAvatar, withOffset: 16)
        buttonPanel.autoPinEdge(.leading, to: .leading, of: self.contentView, withOffset: 16)
        buttonPanel.autoPinEdge(.trailing, to: .trailing, of: self.contentView, withOffset: -16)
        buttonPanel.autoPinEdge(.bottom, to: .bottom, of: self.contentView, withOffset: -8)
        buttonPanel.autoSetDimension(.height, toSize: 36)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupHeader(header: ProfileHeader) {
        //avatarButton.addTarget(self, action: #selector(openPhoto(_:)), for: .touchUpInside)
        if VKConstants.verifyingProfile(from: header.id) {
            let attributedText = NSAttributedString(string: String(htmlEncodedString: "&#10004;") ?? "", attributes: [
              .foregroundColor: UIColor.adaptableBlue,
              .font: GoogleSansFont.medium(with: 20)
            ])
            attributedText.attributes(at: 0, longestEffectiveRange: nil, in: NSRange(location: 0, length: attributedText.length))
            profileName.attributedText = NSAttributedString(string: "\(header.name) ") + attributedText
        } else {
            profileName.attributedText = NSAttributedString(string: "\(header.name) ")
        }
        if VKConstants.verifyingProfile(from: header.id) {
            profileName.attributedText = NSAttributedString(string: "\(header.name) ") + setLabelImage(image: "done_16")!
        } else {
            profileName.attributedText = NSAttributedString(string: "\(header.name)")
        }
        onlineView.setupOnline(onlineApp: header.onlineInfo.onlineApp,
                               isOnline: header.onlineInfo.isOnline,
                               timeLastOnline: header.lastSeenTime,
                               sex: header.sex)
        UIView.animate(withDuration: 0.3, animations: {
            if !header.isCurrentProfile {
                self.statusLabel.text = header.status != "" ? header.status : ""
            } else {
                self.statusLabel.text = header.status != "" ? header.status : "Расскажите о себе :)"
            }
            self.statusLabel.layoutIfNeeded()
        })
        statusLabel.textColor = header.status != "" ? .adaptableGrayVK : .adaptableBlue
        profileAvatar.kf.setImage(with: URL(string: header.photo100))
        let action: FriendAction?
        switch header.friendStatus {
        case 0:
            action = .add
            //buttonPanel.friendActionButton.addTarget(self, action: #selector(addFriend(_:)), for: .touchUpInside)
        case 1:
            action = .unfollow
            //buttonPanel.friendActionButton.addTarget(self, action: #selector(deleteFriend(_:)), for: .touchUpInside)
        case 2:
            action = .add
            //buttonPanel.friendActionButton.addTarget(self, action: #selector(addFriend(_:)), for: .touchUpInside)
        case 3:
            action = .delete
            //buttonPanel.friendActionButton.addTarget(self, action: #selector(deleteFriend(_:)), for: .touchUpInside)
        default:
            return
        }
        //buttonPanel.messageButton.addTarget(self, action: #selector(sendMessage(_:)), for: .touchUpInside)
        buttonPanel.setupButtons(isCurrentProfile: header.isCurrentProfile, writeMessageState: header.canWriteMessage, friendAction: action!)
    }
}
