//
//  ConversationNodeCell.swift
//  VKExt
//
//  Created by programmist_NA on 24.07.2020.
//

import UIKit
import AsyncDisplayKit

class ConversationNodeCell: ASCellNode {
    let avatarSenderNode: ASNetworkImageNode = {
        let imageView = ASNetworkImageNode()
        return imageView
    }()
    var nameSenderNode: ASTextNode = {
        let label = ASTextNode()
        label.tintColor = .adaptableBlack
        label.truncationMode = .byTruncatingTail
        label.maximumNumberOfLines = 1
        return label
    }()
    var lastMessageNode: ASTextNode = {
        let label = ASTextNode()
        label.tintColor = .adaptableBlack
        label.truncationMode = .byTruncatingTail
        label.maximumNumberOfLines = 1
        return label
    }()
    var messageTimeNode: ASTextNode = {
        let label = ASTextNode()
        label.tintColor = .adaptableBlack
        label.maximumNumberOfLines = 1
        return label
    }()
    var unreadCountNode: ASTextNode = {
        let label = ASTextNode()
        label.tintColor = .adaptableWhite
        label.maximumNumberOfLines = 1
        label.backgroundColor = .adaptableBlue
        return label
    }()

    let attributesName: [NSAttributedString.Key : Any] = [.font: GoogleSansFont.medium(with: 17), .foregroundColor: UIColor.adaptableBlack]
    let attributesTime: [NSAttributedString.Key : Any] = [.font: GoogleSansFont.regular(with: 14), .foregroundColor: UIColor.color(from: 0x99A2AD)]
    let attributesLastMessage: [NSAttributedString.Key : Any] = [.font: GoogleSansFont.regular(with: 15), .foregroundColor: UIColor.color(from: 0x6D7885)]
    let attributesUnreadCount: [NSAttributedString.Key : Any] = [.font: GoogleSansFont.medium(with: 15), .foregroundColor: UIColor.adaptableWhite]

    let unreadStatus: ReadStatus
    let unreadCount: Int
    
    init(by conversation: Conversation) {
        self.unreadStatus = conversation.unreadStatus
        self.unreadCount = conversation.unreadCount
        super.init()
        updateInterface()
        setupSenderName(from: conversation.interlocutorName)
        if let photoUrl = conversation.interlocutorPhoto100 {
            setupAvatarSender(from: photoUrl)
        }
        setupLastMessage(from: conversation.text)
        setupDifferenceTime(at: conversation.dateInteger)
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        avatarSenderNode.style.preferredSize = CGSize(width: 56, height: 56)
        let avatarInsets = UIEdgeInsets(top: 8, left: 12, bottom: 8, right: 12)
        let avatarInset = ASInsetLayoutSpec(insets: avatarInsets, child: avatarSenderNode)
        
        nameSenderNode.style.maxWidth = .init(unit: .points, value: screenWidth - getInset(with: unreadStatus, from: unreadCount))
        lastMessageNode.style.maxWidth = .init(unit: .points, value: screenWidth - (getInset(with: unreadStatus, from: unreadCount) + messageTimeNode.attributedText!.string.width(with: 17, font: GoogleSansFont.regular(with: 14))))

        let textsSubStack = ASStackLayoutSpec.vertical()
        textsSubStack.spacing = 6
        let lastMessageSubStack = ASStackLayoutSpec.horizontal()
        lastMessageSubStack.children = [lastMessageNode, messageTimeNode]
        
        lastMessageSubStack.style.maxWidth = .init(unit: .points, value: screenWidth - getInset(with: unreadStatus, from: unreadCount))
                
        textsSubStack.children = [nameSenderNode, lastMessageSubStack]

        let spacer = ASLayoutSpec()
        spacer.style.flexGrow = 1.0
        
        unreadCountNode.style.minHeight = .init(unit: .points, value: 24)
        unreadCountNode.style.minWidth = .init(unit: .points, value: 24)
        unreadCountNode.style.flexShrink = 1.0
        
        let buttonStack = ASStackLayoutSpec.horizontal()
        buttonStack.child = unreadCountNode
        buttonStack.alignItems = .center
        buttonStack.justifyContent = .center
        let buttonInsets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        let buttonInset = ASInsetLayoutSpec(insets: buttonInsets, child: unreadCountNode)
        
        let nodeLayoutStack: ASStackLayoutSpec = .horizontal()
        nodeLayoutStack.alignItems = .center
        nodeLayoutStack.justifyContent = .start
        nodeLayoutStack.alignContent = .stretch
        nodeLayoutStack.children = [avatarInset, textsSubStack, spacer, buttonInset]

        return nodeLayoutStack
    }
    
    func setupSenderName(from name: String) {
        nameSenderNode.attributedText = NSAttributedString(string: name, attributes: attributesName)
    }
    
    func setupAvatarSender(from photoUrl: String) {
        avatarSenderNode.url = URL(string: photoUrl)
    }
    
    func setupLastMessage(from text: String) {
        lastMessageNode.attributedText = NSAttributedString(string: text, attributes: attributesLastMessage)
        unreadCountNode.attributedText = NSAttributedString(string: "\(unreadCount > 0 ? unreadCount.stringValue : " ")", attributes: attributesUnreadCount)
    }
    
    func setupDifferenceTime(at messageTime: Int) {
        let currentTime = Date()
        let timeInterval = currentTime.timeIntervalSince1970
        let doubleTimeInterval = Double(timeInterval)
        let differenceTimes = doubleTimeInterval - Double(messageTime)
        let formatter = DateComponentsFormatter()
        var calendar = Calendar.current
        calendar.locale = Locale(identifier: "ru_RU_POSIX")
        formatter.calendar = calendar
        let formattedString: String

        if differenceTimes < 60 {
            formatter.allowedUnits = []
            formatter.unitsStyle = .brief
            formattedString = formatter.string(from: TimeInterval(differenceTimes))!
            messageTimeNode.attributedText = NSAttributedString(string: "", attributes: attributesTime)
            return
        } else if differenceTimes > 60 && differenceTimes < 3600 {
            formatter.allowedUnits = [.minute]
            formatter.unitsStyle = .brief
            formattedString = formatter.string(from: TimeInterval(differenceTimes))!
            messageTimeNode.attributedText = NSAttributedString(string: " · \(formattedString)", attributes: attributesTime)
            return
        } else if differenceTimes > 3600 && differenceTimes < 86400 {
            formatter.allowedUnits = [.hour]
            formatter.unitsStyle = .brief
            formattedString = formatter.string(from: TimeInterval(differenceTimes))!
            messageTimeNode.attributedText = NSAttributedString(string: " · \(formattedString)", attributes: attributesTime)
            return
        } else if differenceTimes > 86400 && differenceTimes < 604800 {
            formatter.allowedUnits = [.day]
            formatter.unitsStyle = .short
            formattedString = formatter.string(from: TimeInterval(differenceTimes))!
            messageTimeNode.attributedText = NSAttributedString(string: " · \(formattedString)", attributes: attributesTime)
            return
        } else if differenceTimes > 604800 && differenceTimes < 2419200 {
            formatter.allowedUnits = [.weekOfMonth]
            formatter.unitsStyle = .brief
            formattedString = formatter.string(from: TimeInterval(differenceTimes))!
            messageTimeNode.attributedText = NSAttributedString(string: " · \(formattedString)", attributes: attributesTime)
            return
        } else if differenceTimes > 2419200 {
            formatter.allowedUnits = [.month]
            formatter.unitsStyle = .short
            formattedString = formatter.string(from: TimeInterval(differenceTimes))!
            messageTimeNode.attributedText = NSAttributedString(string: " · \(formattedString)", attributes: attributesTime)
            return
        } else {
            formatter.allowedUnits = [.month, .day, .year]
            formatter.unitsStyle = .short
            formattedString = formatter.string(from: TimeInterval(differenceTimes))!
            messageTimeNode.attributedText = NSAttributedString(string: " · \(formattedString)", attributes: attributesTime)
            return
        }
    }
    
    func updateInterface() {
        self.backgroundColor = .adaptableWhite
        self.addSubnode(avatarSenderNode)
        self.addSubnode(nameSenderNode)
        self.addSubnode(lastMessageNode)
        self.addSubnode(messageTimeNode)
        self.addSubnode(unreadCountNode)
        avatarSenderNode.cornerRadius = 28
        unreadCountNode.cornerRadius = 12
        unreadCountNode.clipsToBounds = true
        unreadCountNode.textContainerInset = unreadCount > 1 ? UIEdgeInsets(top: 2.5, left: 7.5, bottom: 1.5, right: 6.5) : UIEdgeInsets(top: 3.5, left: 8.5, bottom: 0.5, right: 5.5)
        
        if unreadCount > 0 || unreadStatus == .markedUnread {
            unreadCountNode.isHidden = false
        } else {
            unreadCountNode.isHidden = true
        }
    }
    
    func getInset(with unreadStatus: ReadStatus, from unreadCount: Int = 0) -> CGFloat {
        let avatarSizeInset = CGFloat(80)
        switch unreadStatus {
        case .unreadIn:
            let countWidth = unreadCount.stringValue.width(with: 20, font: GoogleSansFont.medium(with: 15)) + 14
            return countWidth + 24 + avatarSizeInset
        case .unreadOut:
            return 32 + avatarSizeInset
        case .markedUnread:
            return 56 + avatarSizeInset
        default:
            return 12 + avatarSizeInset
        }
    }
}
