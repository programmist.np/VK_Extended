//
//  FriendCell.swift
//  VK Tosters
//
//  Created by programmist_np on 21/01/2020.
//  Copyright © 2020 programmist_np. All rights reserved.
//

import UIKit
import Material
import MaterialComponents
import AsyncDisplayKit

protocol FriendCellProtocol: NSObjectProtocol {
    func onTapToChat(_ button: IconButton, peerId: Int)
}

class FriendCellNode: ASCellNode {
    private let avatarFriend: ASNetworkImageNode = {
        let imageView = ASNetworkImageNode()
        return imageView
    }()
    var etcLabel: ASTextNode = {
        let label = ASTextNode()
        return label
    }()
    var onlineImageView: ASNetworkImageNode = {
        let imageView = ASNetworkImageNode()
        return imageView
    }()
    var nameLabel: ASTextNode = {
        let label = ASTextNode()
        label.tintColor = .adaptableBlack
        return label
    }()
    var messageNode: ASButtonNode = {
        let node = ASButtonNode()
        node.setImage(UIImage(named: "message_outline_28")?.withRenderingMode(.alwaysTemplate).tint(with: .extendedBlue), for: .normal)
        return node
    }()
    let attributesName: [NSAttributedString.Key : Any] = [.font: GoogleSansFont.medium(with: 16), .foregroundColor: UIColor.adaptableBlack]
    let attributesEtc: [NSAttributedString.Key : Any] = [.font: GoogleSansFont.medium(with: 13), .foregroundColor: UIColor.adaptableGrayVK]
    
    weak var delegate: FriendCellProtocol?
    var peerId: Int?

    init(by friend: FriendCellViewModel) {
        super.init()
        backgroundColor = .adaptableWhite
        view.backgroundColor = .adaptableWhite
        peerId = friend.id
        setupLayer()
        onlineImageView.isHidden = true
        if VKConstants.testingProfile(from: friend.id!) {
            nameLabel.attributedText = NSAttributedString(string: "\(friend.firstName!) \(friend.lastName!)", attributes: attributesName) + setLabelImage(image: "fire_verified_16")!
        } else {
            nameLabel.attributedText = NSAttributedString(string: "\(friend.firstName!) \(friend.lastName!)", attributes: attributesName)
        }
        if VKConstants.verifyingProfile(from: friend.id!) {
            nameLabel.attributedText = nameLabel.attributedText! + setLabelImage(image: "done_16")!
        } else {
            nameLabel.attributedText = nameLabel.attributedText!
        }
        setupEtc(homeTown: friend.homeTown, universities: friend.school)
        self.avatarFriend.url = URL(string: friend.photo100!)
        
        automaticallyManagesSubnodes = true
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        avatarFriend.style.preferredSize = CGSize(width: 48, height: 48)
        let avatarInsets = UIEdgeInsets(top: 6, left: 12, bottom: 6, right: 12)
        let avatarInset = ASInsetLayoutSpec(insets: avatarInsets, child: avatarFriend)
        
        onlineImageView.style.preferredSize = CGSize(width: 24, height: 28)
        let onlineInsets = UIEdgeInsets(top: 30, left: -36, bottom: 2, right: 12)
        let onlineInset = ASInsetLayoutSpec(insets: onlineInsets, child: onlineImageView)
        
        nameLabel.style.flexShrink = 1.0
        etcLabel.style.flexShrink = 1.0
        
        let spacer = ASLayoutSpec()
        spacer.style.flexGrow = 1.0
        
        let textSubStack = ASStackLayoutSpec.vertical()
        textSubStack.children = etcLabel.attributedText != NSAttributedString(string: "") ? [nameLabel, etcLabel] : [nameLabel]
        
        messageNode.style.preferredSize = CGSize(width: 28, height: 28)
        let buttonStack = ASStackLayoutSpec.horizontal()
        buttonStack.child = messageNode
        let buttonInsets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        let buttonInset = ASInsetLayoutSpec(insets: buttonInsets, child: messageNode)
        
        let nodeLayoutStack = ASStackLayoutSpec.horizontal()
        nodeLayoutStack.alignItems = ASStackLayoutAlignItems.center
        nodeLayoutStack.justifyContent = ASStackLayoutJustifyContent.start
        nodeLayoutStack.children = [avatarInset, onlineInset, textSubStack, spacer, buttonInset]
        
        return nodeLayoutStack
    }
    
    override func asyncTraitCollectionDidChange(withPreviousTraitCollection previousTraitCollection: ASPrimitiveTraitCollection) {
        super.asyncTraitCollectionDidChange(withPreviousTraitCollection: previousTraitCollection)
        self.backgroundColor = .adaptableWhite
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        setHighlighted()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        setUnhighlighted()
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>?, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        setUnhighlighted()
    }
    
    func setHighlighted() {
        UIView.animate(withDuration: 0.2, delay: 0, options: [.preferredFramesPerSecond60, .transitionCrossDissolve], animations: {
            self.backgroundColor = UIColor.adaptableBlue.withAlphaComponent(0.2)
            self.layoutIfNeeded()
        })
    }
    
    func setUnhighlighted() {
        UIView.animate(withDuration: 0.2, delay: 0, options: [.preferredFramesPerSecond60, .transitionCrossDissolve], animations: {
            self.backgroundColor = .adaptableWhite
            self.layoutIfNeeded()
        })
    }
    
    func setupEtc(homeTown: String?, universities: String?) {
        if let homeTown = homeTown, homeTown != "" {
            etcLabel.attributedText = NSAttributedString(string: homeTown, attributes: attributesEtc)
        } else if let universities = universities, universities != "" {
            etcLabel.attributedText = NSAttributedString(string: universities, attributes: attributesEtc)
        } else {
            etcLabel.attributedText = NSAttributedString(string: "")
        }
    }
    
    func setupLayer() {
        self.selectionStyle = .none
        self.backgroundColor = .adaptableWhite
        /// setup Avatar
        self.addSubnode(avatarFriend)
        self.addSubnode(nameLabel)
        self.addSubnode(etcLabel)
        avatarFriend.addSubnode(onlineImageView)
        self.addSubnode(messageNode)
        
        self.avatarFriend.cornerRadius = 24
        self.avatarFriend.clipsToBounds = true
        
        self.messageNode.addTarget(self, action: #selector(onTapToChat(_:)), forControlEvents: .touchUpInside)
    }
}
extension FriendCellNode {
    @objc func onTapToChat(_ sender: IconButton) {
        guard let peerId = peerId else { return }
        delegate?.onTapToChat(sender, peerId: peerId)
    }
}

class FollowerCellNode: ASCellNode {
    private let avatarFriend: ASNetworkImageNode = {
        let imageView = ASNetworkImageNode()
        return imageView
    }()
    var onlineImageView: ASNetworkImageNode = {
        let imageView = ASNetworkImageNode()
        return imageView
    }()
    var nameLabel: ASTextNode = {
        let label = ASTextNode()
        label.tintColor = .adaptableBlack
        return label
    }()
    let attributesName: [NSAttributedString.Key : Any] = [.font: GoogleSansFont.medium(with: 16), .foregroundColor: UIColor.adaptableBlack]
    let attributesEtc: [NSAttributedString.Key : Any] = [.font: GoogleSansFont.medium(with: 13), .foregroundColor: UIColor.adaptableGrayVK]
    
    weak var delegate: FriendCellProtocol?
    var peerId: Int?

    init(by friend: FriendCellViewModel) {
        super.init()
        backgroundColor = .adaptableWhite
        view.backgroundColor = .adaptableWhite
        peerId = friend.id
        setupLayer()
        onlineImageView.isHidden = true
        if VKConstants.testingProfile(from: friend.id!) {
            nameLabel.attributedText = NSAttributedString(string: "\(friend.firstName!) \(friend.lastName!)", attributes: attributesName) + setLabelImage(image: "fire_verified_16")!
        } else {
            nameLabel.attributedText = NSAttributedString(string: "\(friend.firstName!) \(friend.lastName!)", attributes: attributesName)
        }
        if VKConstants.verifyingProfile(from: friend.id!) {
            nameLabel.attributedText = nameLabel.attributedText! + setLabelImage(image: "done_16")!
        } else {
            nameLabel.attributedText = nameLabel.attributedText!
        }
        self.avatarFriend.url = URL(string: friend.photo100!)
        
        automaticallyManagesSubnodes = true
    }
    
    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        avatarFriend.style.preferredSize = CGSize(width: 36, height: 36)
        let avatarInsets = UIEdgeInsets(top: 4, left: 12, bottom: 4, right: 12)
        let avatarInset = ASInsetLayoutSpec(insets: avatarInsets, child: avatarFriend)
        
        onlineImageView.style.preferredSize = CGSize(width: 24, height: 28)
        let onlineInsets = UIEdgeInsets(top: 28, left: -36, bottom: 2, right: 12)
        let onlineInset = ASInsetLayoutSpec(insets: onlineInsets, child: onlineImageView)
        
        nameLabel.style.flexShrink = 1.0
        
        let spacer = ASLayoutSpec()
        spacer.style.flexGrow = 1.0
        
        let textSubStack = ASStackLayoutSpec.vertical()
        textSubStack.children = [nameLabel]
        
        let nodeLayoutStack = ASStackLayoutSpec.horizontal()
        nodeLayoutStack.alignItems = ASStackLayoutAlignItems.center
        nodeLayoutStack.justifyContent = ASStackLayoutJustifyContent.start
        nodeLayoutStack.children = [avatarInset, onlineInset, textSubStack, spacer]
        
        return nodeLayoutStack
    }
    
    override func asyncTraitCollectionDidChange(withPreviousTraitCollection previousTraitCollection: ASPrimitiveTraitCollection) {
        super.asyncTraitCollectionDidChange(withPreviousTraitCollection: previousTraitCollection)
        self.backgroundColor = .adaptableWhite
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        setHighlighted()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        setUnhighlighted()
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>?, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        setUnhighlighted()
    }
    
    func setHighlighted() {
        UIView.animate(withDuration: 0.2, delay: 0, options: [.preferredFramesPerSecond60, .transitionCrossDissolve], animations: {
            self.backgroundColor = UIColor.adaptableBlue.withAlphaComponent(0.2)
            self.layoutIfNeeded()
        })
    }
    
    func setUnhighlighted() {
        UIView.animate(withDuration: 0.2, delay: 0, options: [.preferredFramesPerSecond60, .transitionCrossDissolve], animations: {
            self.backgroundColor = .adaptableWhite
            self.layoutIfNeeded()
        })
    }
    
    func setupLayer() {
        self.selectionStyle = .none
        self.backgroundColor = .adaptableWhite
        /// setup Avatar
        self.addSubnode(avatarFriend)
        self.addSubnode(nameLabel)
        avatarFriend.addSubnode(onlineImageView)
        
        self.avatarFriend.cornerRadius = 18
        self.avatarFriend.clipsToBounds = true
    }
}
