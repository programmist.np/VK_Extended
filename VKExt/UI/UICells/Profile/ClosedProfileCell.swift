//
//  ClosedProfileCell.swift
//  VKExt
//
//  Created by programmist_NA on 24.07.2020.
//

import UIKit

class ClosedProfileCell: UITableViewCell {
    @IBOutlet weak var closedImageView: UIImageView!
    @IBOutlet weak var closedDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        backgroundColor = .adaptableWhite
        contentView.drawBorder(13, width: 0.5, color: .adaptableDivider)
        closedImageView.image = UIImage(named: "lock_outline_56")?.withRenderingMode(.alwaysTemplate).withTintColor(.adaptableDarkGrayVK)
        
        closedDescriptionLabel.font = GoogleSansFont.medium(with: 15)
        closedDescriptionLabel.textColor = .adaptableDarkGrayVK
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setDescription(name: String, sex: Int) {
        closedDescriptionLabel.attributedText = NSAttributedString(string: "Это закрытый профиль", attributes: [.font: GoogleSansFont.medium(with: 16), .foregroundColor: UIColor.adaptableBlack]) + NSAttributedString(string: "\n") + NSAttributedString(string: "\(name) \(sex == 0 ? "закрыла" : "закрыл") доступ к своей странице.", attributes: [.font: GoogleSansFont.medium(with: 14), .foregroundColor: UIColor.adaptableDarkGrayVK]) + NSAttributedString(string: "\n") + NSAttributedString(string: "Добавьте \(sex == 0 ? "её" : "его") в друзья, чтобы увидеть записи и фотографии", attributes: [.font: GoogleSansFont.medium(with: 14), .foregroundColor: UIColor.adaptableDarkGrayVK])
        closedDescriptionLabel.sizeToFit()
    }
}
