//
//  ProfileFirstAnotherCell.swift
//  VKExt
//
//  Created by programmist_NA on 22.07.2020.
//

import Foundation
import UIKit
import Material
import MaterialComponents

class ProfileFirstAnotherCell: Material.TableViewCell {
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var profileStatusLabel: UILabel!
    @IBOutlet weak var profileLastTimeLabel: UILabel!
    
    @IBOutlet weak var actionFriend: IconButton!
    @IBOutlet weak var messageButton: IconButton!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var connectButtonsConstraint: NSLayoutConstraint!
    
    weak var delegate: ProfileDelegate?

    let onlineAttributes: [NSAttributedString.Key: Any] = [.font: GoogleSansFont.regular(with: 14), .foregroundColor: UIColor.adaptableGrayVK]
    let accessFirstAttributes: [NSAttributedString.Key: Any] = [.font: GoogleSansFont.regular(with: 12), .foregroundColor: UIColor.adaptableDarkGrayVK]
    let accessSecondAttributes: [NSAttributedString.Key: Any] = [.font: GoogleSansFont.regular(with: 12), .foregroundColor: UIColor.extendedBlue]
    var friendAction: FriendAction?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .adaptableWhite
        avatarImageView.setRounded()
        
        profileNameLabel.textColor = .adaptableBlack
        profileNameLabel.font = GoogleSansFont.bold(with: 18)
        
        profileLastTimeLabel.textColor = .adaptableGrayVK
        profileLastTimeLabel.font = GoogleSansFont.regular(with: 14)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func set(viewModel: ProfileCellViewModel) {
        friendAction = viewModel.friendActionType
        setupUI()
        setupButtons(with: viewModel.type, from: viewModel.friendActionType)
        
        avatarImageView.kf.setImage(with: URL(string: viewModel.photo100!))
        
        profileNameLabel.text = "\(viewModel.firstName!) \(viewModel.lastName!)"
        
        profileStatusLabel.text = viewModel.status
        profileStatusLabel.textColor = .adaptableDarkGrayVK
        profileStatusLabel.font = GoogleSansFont.regular(with: 15)
        
        if viewModel.isOnline ?? false {
            if viewModel.isMobile ?? false {
                profileLastTimeLabel.attributedText = NSAttributedString(string: "online ") + setLabelImage(image: "online_mobile_composite_foreground_20")!
            } else {
                profileLastTimeLabel.text = "online"
            }
        } else {
            profileLastTimeLabel.text = "offline"
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        actionFriend.drawBorder(18, width: 0.5, color: .adaptableDivider)
        actionFriend.image = UIImage(named: friendAction!.setImage())?.withRenderingMode(.alwaysTemplate).withTintColor(.adaptableBlue).resize(toWidth: 24)?.resize(toHeight: 24)
        
        messageButton.drawBorder(18, width: 0.5, color: .adaptableDivider)
        messageButton.image = UIImage(named: "mail_outline_28")?.withRenderingMode(.alwaysTemplate).withTintColor(.adaptableBlue).resize(toWidth: 24)?.resize(toHeight: 24)
    }
    
    func setupUI() {
        let tapRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onOpenPhoto))
        tapRecognizer.numberOfTapsRequired = 1
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.addGestureRecognizer(tapRecognizer)
    }
    
    func setupButtons(with type: ProfileActionType, from actionType: FriendAction) {
        switch type {
        case .actionFriendWithMessage:
            bottomConstraint.isActive = true
            connectButtonsConstraint.isActive = false
            messageButton.isHidden = false
        case .actionFriend:
            bottomConstraint.isActive = false
            connectButtonsConstraint.isActive = true
            messageButton.isHidden = true
        }
    }
    
    @objc func onOpenPhoto() {
        delegate?.onOpenPhotoAnother(for: self)
    }
}
