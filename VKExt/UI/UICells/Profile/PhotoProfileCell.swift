//
//  PhotoProfileCell.swift
//  VKExt
//
//  Created by programmist_NA on 17.07.2020.
//

import UIKit
import Material

class PhotoProfileCell: Material.TableViewCell {
    static let reuseId = "PhotoProfileCell"
    
    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    let countLabel: UILabel = {
        let label = UILabel()
        label.textColor = .adaptableBlack
        label.font = GoogleSansFont.medium(with: 15)
        return label
    }()
    
    let galleryCollectionView = ProfilePhotosCollectionView()
    var firstAttributes: [NSAttributedString.Key : Any] = [.font: GoogleSansFont.bold(with: 14), .foregroundColor: UIColor.adaptableBlack]
    var secondAttributes: [NSAttributedString.Key : Any] = [.font: GoogleSansFont.medium(with: 13), .foregroundColor: UIColor.adaptableGrayVK]
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(viewModel: PhotoCellViewModel) {
        if viewModel.photos?.count ?? 0 > 0 {
            let attrText = NSAttributedString(string: "Фотографии   ".uppercased(), attributes: firstAttributes) + NSAttributedString(string: "\(viewModel.photos!.count)", attributes: secondAttributes)
            countLabel.attributedText = attrText
            galleryCollectionView.isHidden = false
            galleryCollectionView.set(photos: viewModel.photos ?? [])
        }
        else {
            countLabel.text = ""
            galleryCollectionView.isHidden = true
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.drawBorder(10, width: 0.5, color: .adaptableDivider)
    }
    
    func setupUI() {
        backgroundColor = .adaptableWhite
        countLabel.backgroundColor = .adaptableWhite
        galleryCollectionView.backgroundColor = .adaptableWhite

        addSubview(containerView)
        containerView.addSubview(countLabel)
        containerView.addSubview(galleryCollectionView)
        
        containerView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 4, left: 12, bottom: 4, right: 12))
        
        countLabel.autoPinEdge(.top, to: .top, of: containerView, withOffset: 8)
        countLabel.autoPinEdge(.leading, to: .leading, of: containerView, withOffset: 12)
        countLabel.autoPinEdge(.trailing, to: .trailing, of: containerView, withOffset: -12)
        
        galleryCollectionView.autoPinEdge(.top, to: .bottom, of: countLabel, withOffset: 4)
        galleryCollectionView.autoPinEdge(.leading, to: .leading, of: containerView, withOffset: 12)
        galleryCollectionView.autoPinEdge(.trailing, to: .trailing, of: containerView, withOffset: -12)
        galleryCollectionView.autoPinEdge(.bottom, to: .bottom, of: containerView, withOffset: -4)
        galleryCollectionView.autoSetDimensions(to: CGSize(width: screenWidth / 3, height: screenWidth / 3))
    }
}
