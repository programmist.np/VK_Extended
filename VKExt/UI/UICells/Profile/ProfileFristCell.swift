//
//  ProfileFristCell.swift
//  VKExt
//
//  Created by programmist_NA on 15.07.2020.
//

import UIKit
import Material
import MaterialComponents

protocol ProfileDelegate: NSObjectProtocol {
    func onOpenPhoto(for cell: ProfileFristCell)
    func onOpenPhotoAnother(for cell: ProfileFirstAnotherCell)
}

class ProfileFristCell: Material.TableViewCell {
    @IBOutlet weak var closedProfileView: View!
    @IBOutlet weak var closeProfileLabel: UILabel!
    @IBOutlet weak var closedProfileViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var profileStatusLabel: UILabel!
    @IBOutlet weak var profileLastTimeLabel: UILabel!
    
    @IBOutlet weak var editProfileButton: IconButton!
    
    weak var delegate: ProfileDelegate?

    let onlineAttributes: [NSAttributedString.Key: Any] = [.font: GoogleSansFont.regular(with: 14), .foregroundColor: UIColor.adaptableGrayVK]
    let accessFirstAttributes: [NSAttributedString.Key: Any] = [.font: GoogleSansFont.regular(with: 12), .foregroundColor: UIColor.adaptableDarkGrayVK]
    let accessSecondAttributes: [NSAttributedString.Key: Any] = [.font: GoogleSansFont.regular(with: 12), .foregroundColor: UIColor.extendedBlue]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = .adaptableWhite
        closedProfileView.drawBorder(8, width: 0.0, color: .adaptableMutedButtonColor)
        closedProfileView.backgroundColor = .adaptableMutedButtonColor
        closeProfileLabel.textColor = .adaptableGrayVK
        closeProfileLabel.font = GoogleSansFont.medium(with: 12)
        closeProfileLabel.attributedText = NSAttributedString(string: "У Вас закрытый профиль ", attributes: accessFirstAttributes) + NSAttributedString(string: " ·  Изменить", attributes: accessSecondAttributes)
        
        avatarImageView.setRounded()
        
        profileNameLabel.textColor = .adaptableBlack
        profileNameLabel.font = GoogleSansFont.bold(with: 18)
        
        profileLastTimeLabel.textColor = .adaptableGrayVK
        profileLastTimeLabel.font = GoogleSansFont.regular(with: 14)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func set(viewModel: ProfileCellViewModel) {
        setupUI()
        if viewModel.isClosed! {
            closedProfileViewHeightConstraint.constant = 36
            closedProfileView.isHidden = false
        } else {
            closedProfileViewHeightConstraint.constant = 0
            closedProfileView.isHidden = true
        }
        
        avatarImageView.kf.setImage(with: URL(string: viewModel.photo100!))
        
        profileNameLabel.text = "\(viewModel.firstName!) \(viewModel.lastName!)"
        
        profileStatusLabel.text = viewModel.status == "" ? "Расскажите о себе" : viewModel.status
        profileStatusLabel.textColor = viewModel.status == "" ? .adaptableBlue : .adaptableDarkGrayVK
        profileStatusLabel.font = viewModel.status == "" ? GoogleSansFont.medium(with: 15) : GoogleSansFont.regular(with: 15)
        
        if viewModel.isOnline ?? false {
            if viewModel.isMobile ?? false {
                profileLastTimeLabel.attributedText = NSAttributedString(string: "online ") + setLabelImage(image: "online_mobile_composite_foreground_20")!
            } else {
                profileLastTimeLabel.text = "online"
            }
        } else {
            profileLastTimeLabel.text = "offline"
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        editProfileButton.drawBorder(18, width: 0.5, color: .adaptableDivider)
        editProfileButton.image = UIImage(named: "settings_outline_28")?.withRenderingMode(.alwaysTemplate).withTintColor(.adaptableBlue).resize(toWidth: 24)?.resize(toHeight: 24)
    }
    
    func setupUI() {
        let tapRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onOpenPhoto))
        tapRecognizer.numberOfTapsRequired = 1
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func onOpenPhoto() {
        delegate?.onOpenPhoto(for: self)
    }
}
