//
//  ProfileSecondCell.swift
//  VKExt
//
//  Created by programmist_NA on 15.07.2020.
//

import UIKit
import Material

class ProfileSecondCell: Material.TableViewCell {
    @IBOutlet weak var infoImageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .adaptableWhite
        containerView.backgroundColor = .adaptableWhite
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func set(image: UIImage?, attrTextInfo: NSAttributedString) {
        setupUI()
        infoImageView.image = image
        infoLabel.attributedText = attrTextInfo
    }
    
    func setupUI() {
        infoLabel.backgroundColor = .adaptableWhite
    }
}
