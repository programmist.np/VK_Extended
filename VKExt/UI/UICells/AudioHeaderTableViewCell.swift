//
//  AudioHeaderTableViewCell.swift
//  VKExt
//
//  Created by programmist_NA on 25.05.2020.
//

import UIKit
import MaterialComponents

class AudioHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var playAllButton: MDCButton!
    @IBOutlet weak var shuffleAllButton: MDCButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupLayer() {
        self.contentView.backgroundColor = .adaptableWhite

        playAllButton.isUppercaseTitle = false
        playAllButton.setCorners(radius: 10)
        playAllButton.setBackgroundColor(UIColor.adaptableSeparator.withAlphaComponent(0.05))
        playAllButton.setImage(UIImage(named: "play_16"), for: .normal)
        playAllButton.setImageTintColor(.extendedBlue, for: .normal)
        playAllButton.setTitle("  Слушать все", for: .normal)
        playAllButton.setTitleFont(GoogleSansFont.medium(with: 15), for: .normal)
        playAllButton.setTitleColor(.extendedBlue, for: .normal)
        playAllButton.inkColor = UIColor.color(from: 0x001C3D).withAlphaComponent(0.5)

        shuffleAllButton.isUppercaseTitle = false
        shuffleAllButton.setCorners(radius: 10)
        shuffleAllButton.setBackgroundColor(UIColor.adaptableSeparator.withAlphaComponent(0.05))
        shuffleAllButton.setImage(UIImage(named: "shuffle_24"), for: .normal)
        shuffleAllButton.setImageTintColor(.extendedBlue, for: .normal)
        shuffleAllButton.setTitle("  Перемешать", for: .normal)
        shuffleAllButton.setTitleFont(GoogleSansFont.medium(with: 15), for: .normal)
        shuffleAllButton.setTitleColor(.extendedBlue, for: .normal)
        shuffleAllButton.inkColor = UIColor.color(from: 0x001C3D).withAlphaComponent(0.5)
    }
}
