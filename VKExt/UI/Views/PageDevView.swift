//
//  PageDevView.swift
//  VKExt
//
//  Created by programmist_NA on 25.05.2020.
//

import Foundation
import MaterialComponents
import Material
import UIKit
import Lottie

class PageDevView: UIView {
    var isConstraintInstall: Bool = false
    let view: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableWhite
        return view
    }()
    let containerImageView: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableWhite
        return view
    }()
    let devImage: UIImageView = {
        let image = UIImageView()
        return image
    }()
    let devAnimationImage: AnimationView = {
        let animation = AnimationView()
        return animation
    }()
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .adaptableDarkGrayVK
        label.font = GoogleSansFont.regular(with: 16)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    var heightConstraint: NSLayoutConstraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.addSubview(view)
        view.addSubview(containerImageView)
        containerImageView.addSubview(devImage)
        containerImageView.addSubview(devAnimationImage)
        view.addSubview(descriptionLabel)
    }
    
    override func updateConstraints() {
        if !isConstraintInstall {
            isConstraintInstall = true
            view.autoCenterInSuperview()
            view.autoSetDimension(.width, toSize: screenWidth - 32)
            
            containerImageView.autoPinEdge(.top, to: .top, of: view, withOffset: 16)
            containerImageView.autoAlignAxis(toSuperviewAxis: .vertical)
            containerImageView.autoSetDimensions(to: CGSize(width: 128, height: 128))
            
            devImage.autoPinEdgesToSuperviewEdges()
            devImage.autoSetDimensions(to: CGSize(width: 128, height: 128))
            
            devAnimationImage.autoPinEdgesToSuperviewEdges()
            devAnimationImage.autoSetDimensions(to: CGSize(width: 128, height: 128))
            
            descriptionLabel.autoPinEdge(.top, to: .bottom, of: containerImageView, withOffset: 12)
            descriptionLabel.autoPinEdge(.trailing, to: .trailing, of: view, withOffset: -16)
            descriptionLabel.autoPinEdge(.leading, to: .leading, of: view, withOffset: 16)
            descriptionLabel.autoAlignAxis(toSuperviewAxis: .vertical)
            descriptionLabel.autoPinEdge(.bottom, to: .bottom, of: view, withOffset: -12)
            
            view.sizeToFit()
            
        }
        super.updateConstraints()
    }
    
    func setup(page: String, image: UIImage) {
        descriptionLabel.text = """
        Раздел \(page) находится в разработке
        Ожидайте весточку от разработчика
        """
        devImage.image = image.withRenderingMode(.alwaysTemplate).tint(with: .adaptableDarkGrayVK)
    }
    
    func setup(page: String, animation: String) {
        descriptionLabel.text = """
        Раздел \(page) находится в разработке
        Ожидайте весточку от разработчика
        """
        descriptionLabel.sizeToFit()
        devAnimationImage.animation = Lottie.Animation.named(animation)
        devAnimationImage.contentMode = .scaleAspectFit
        devAnimationImage.loopMode = .playOnce
        devAnimationImage.animationSpeed = 1
        devAnimationImage.frame = devImage.bounds
        heightConstraint?.constant = 128 + descriptionLabel.requiredHeight
        updateConstraints()
        self.layoutIfNeeded()
    }
    
    func setup(message: String, image: UIImage?) {
        descriptionLabel.text = message
        guard let image = image else { return }
        devImage.image = image.withRenderingMode(.alwaysTemplate).tint(with: .adaptableDarkGrayVK)
    }
    
    func setup(message: String, animation: String) {
        devAnimationImage.animation = Lottie.Animation.named(animation)
        devAnimationImage.contentMode = .scaleAspectFit
        devAnimationImage.loopMode = .playOnce
        devAnimationImage.animationSpeed = 1
        devAnimationImage.frame = devImage.bounds
        heightConstraint?.constant = 128 + descriptionLabel.requiredHeight
        updateConstraints()
        self.layoutIfNeeded()
    }
    
    func setupDescription(message: String) {
        descriptionLabel.attributedText = descriptionLabel.attributedText ?? NSAttributedString(string: "") + NSAttributedString(string: message, attributes: [.font: GoogleSansFont.regular(with: 16), .foregroundColor: UIColor.adaptableDarkGrayVK])
        descriptionLabel.sizeToFit()
    }
}
