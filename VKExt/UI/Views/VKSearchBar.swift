//
//  VKSearchBar.swift
//  VK Tosters
//
//  Created by programmist_np on 24.04.2020.
//  Copyright © 2020 programmist_np. All rights reserved.
//

import Foundation
import Material
import UIKit

class VKSearchBar: UIView {
    var isConstraintInstall: Bool = false
    let searchView: UIView = {
        let fake = UIView()
        return fake
    }()
    let searchButton: IconButton = {
        let button = IconButton(image: UIImage(named: "search_outline_16")?.withRenderingMode(.alwaysTemplate), tintColor: .adaptableDarkGrayVK)
        return button
    }()
    let textView: TextField = {
        let textView = TextField()
        textView.placeholderLabel.textColor = .adaptableDarkGrayVK
        textView.textColor = .adaptableBlack
        textView.font = GoogleSansFont.regular(with: 16)
        textView.placeholderLabel.font = GoogleSansFont.regular(with: 16)
        textView.textInsets = UIEdgeInsets(top: 2, left: 34, bottom: 0, right: 28)
        textView.dividerActiveHeight = 0
        textView.dividerNormalHeight = 0
        textView.returnKeyType = .search
        return textView
    }()
    let clearButton: IconButton = {
        let button = IconButton(image: UIImage(named: "clear_16")?.withRenderingMode(.alwaysTemplate), tintColor: .adaptableDarkGrayVK)
        return button
    }()
    let cancelButton: FlatButton = {
        let button = FlatButton()
        button.title = "Отменить"
        button.titleLabel?.font = GoogleSansFont.regular(with: 17)
        button.titleLabel?.autoSetDimension(.height, toSize: 22)
        button.titleColor = .extendedBlue
        return button
    }()
    var trailingConstraint: NSLayoutConstraint?

    override func layoutSubviews() {
        super.layoutSubviews()
        searchView.setBorder(10, width: 0.5, color: .adaptableDivider)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.backgroundColor = .adaptableWhite
        self.addSubview(searchView)
        self.addSubview(cancelButton)
        searchView.addSubview(textView)
        searchView.addSubview(clearButton)
        searchView.addSubview(searchButton)
        clearButton.isHidden = true
        clearButton.addTarget(self, action: #selector(didClear), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(didCancel), for: .touchUpInside)
    }
    
    override func updateConstraints() {
        if !isConstraintInstall {
            isConstraintInstall = true
            self.translatesAutoresizingMaskIntoConstraints = true
            textView.autoPinEdge(.top, to: .top, of: searchView)
            textView.autoPinEdge(.bottom, to: .bottom, of: searchView)
            textView.autoPinEdge(.leading, to: .leading, of: searchView)
            textView.autoPinEdge(.trailing, to: .trailing, of: searchView)
            clearButton.autoPinEdge(.trailing, to: .trailing, of: searchView)
            clearButton.autoSetDimensions(to: CGSize(width: 36, height: 36))
            clearButton.autoAlignAxis(toSuperviewAxis: .horizontal)
            searchButton.autoPinEdge(.leading, to: .leading, of: searchView)
            searchButton.autoSetDimensions(to: CGSize(width: 36, height: 24))
            searchButton.autoAlignAxis(toSuperviewAxis: .horizontal)
            searchView.autoPinEdge(.leading, to: .leading, of: self, withOffset: 12)
            trailingConstraint = searchView.autoPinEdge(.trailing, to: .trailing, of: self, withOffset: -12)
            searchView.autoPinEdge(.bottom, to: .bottom, of: self, withOffset: -8)
            searchView.autoPinEdge(.top, to: .top, of: self, withOffset: 8)
            cancelButton.autoPinEdge(.leading, to: .trailing, of: searchView, withOffset: 12)
            cancelButton.autoSetDimension(.height, toSize: 36)
            cancelButton.autoAlignAxis(toSuperviewAxis: .horizontal)
        }
        super.updateConstraints()
    }
    
    func setup(placeholder: String) {
        textView.placeholderLabel.text = placeholder
    }
    
    @objc func didClear() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .transitionCrossDissolve, animations: {
            self.textView.text?.removeAll()
            self.clearButton.alpha = 0
            self.layoutIfNeeded()
        }) { (isSuccess) in
            self.clearButton.isHidden = true
        }
    }
    
    @objc func didCancel() {
        UIView.animate(withDuration: 0.3, delay: 0, options: .transitionCrossDissolve, animations: {
            self.textView.resignFirstResponder()
            self.textView.text?.removeAll()
            self.layoutIfNeeded()
        })
    }
}
