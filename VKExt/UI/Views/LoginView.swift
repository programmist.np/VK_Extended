//
//  LoginView.swift
//  VKExt
//
//  Created by programmist_NA on 27.05.2020.
//

import UIKit
import MaterialComponents
import Material

class LoginView: UIView {
    var isConstraintInstall: Bool = false
    var needAuthLabel: UILabel = {
        let label = UILabel()
        label.text = "Необходима авторизация"
        label.textColor = .adaptableBlack
        label.font = GoogleSansFont.bold(with: 20)
        label.textAlignment = .center
        label.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 40)
        return label
    }()
    var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "dismiss_24"), for: .normal)
        return button
    }()
    var containter: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableWhite
        return view
    }()
    var loginField: UITextField = {
        let field = UITextField()
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: 12))
        field.backgroundColor = .adaptableField
        field.textColor = .adaptableBlack
        field.font = GoogleSansFont.regular(with: 16)
        field.placeholder = "E-mail или телефон"
        field.textContentType = .username
        field.leftView = paddingView
        field.leftViewMode = .always
        return field
    }()
    var passwordField: UITextField = {
        let field = UITextField()
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: 12))
        field.backgroundColor = .adaptableField
        field.textColor = .adaptableBlack
        field.font = GoogleSansFont.regular(with: 16)
        field.textContentType = .password
        field.placeholder = "Пароль"
        field.isSecureTextEntry = true
        field.leftView = paddingView
        field.leftViewMode = .always
        return field
    }()
    var loginButton: MDCButton = {
        let button = MDCButton()
        button.setTitle("Войти", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.setTitleFont(GoogleSansFont.medium(with: 17), for: .normal)
        button.autoSetDimension(.height, toSize: 44)
        button.isUppercaseTitle = false
        button.backgroundColor = .extendedBlue
        return button
    }()
    let progress: MDCActivityIndicator = {
        let progress = MDCActivityIndicator()
        progress.radius = 12
        progress.strokeWidth = 3
        progress.indicatorMode = .indeterminate
        progress.cycleColors = [.white]
        return progress
    }()
    var keyboardWatcher: MDCKeyboardWatcher = MDCKeyboardWatcher()
    let transformIdentity = CGAffineTransform.identity
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        loginField.drawBorder(10, width: 0.5, color: .adaptableBorder)
        passwordField.drawBorder(10, width: 0.5, color: .adaptableBorder)
    }
    
    func commonInit() {
        self.addSubview(containter)
        containter.addSubview(dismissButton)
        containter.addSubview(needAuthLabel)
        containter.addSubview(loginButton)
        containter.addSubview(passwordField)
        containter.addSubview(loginField)
        loginButton.addSubview(progress)
        containter.drawBorder(18, width: 0, color: .clear, isOnlyTopCorners: false)
        loginButton.drawBorder(10, width: 0, color: .clear, isOnlyTopCorners: false)
        loginButton.alpha = 0.4
    }
    
    override func updateConstraints() {
        if !isConstraintInstall {
            isConstraintInstall = true
            self.autoSetDimensions(to: CGSize(width: Screen.bounds.width, height: Screen.bounds.height))
            //containter.autoSetDimensions(to: CGSize(width: Screen.bounds.width - 16, height: (Screen.bounds.height / 2) - 24))
            containter.autoAlignAxis(toSuperviewAxis: .horizontal)
            containter.autoPinEdge(.trailing, to: .trailing, of: self, withOffset: -8)
            containter.autoPinEdge(.leading, to: .leading, of: self, withOffset: 8)
            
            dismissButton.autoSetDimensions(to: CGSize(width: 48, height: 48))
            dismissButton.autoPinEdge(.top, to: .top, of: containter, withOffset: 4)
            dismissButton.autoPinEdge(.trailing, to: .trailing, of: containter, withOffset: -4)
            
            needAuthLabel.autoPinEdge(.top, to: .top, of: containter, withOffset: 16)
            needAuthLabel.autoPinEdge(.trailing, to: .trailing, of: containter, withOffset: -16)
            needAuthLabel.autoPinEdge(.leading, to: .leading, of: containter, withOffset: 16)

            loginButton.autoPinEdge(.bottom, to: .bottom, of: containter, withOffset: -16)
            loginButton.autoPinEdge(.leading, to: .leading, of: containter, withOffset: 16)
            loginButton.autoPinEdge(.trailing, to: .trailing, of: containter, withOffset: -16)
            
            passwordField.autoPinEdge(.bottom, to: .top, of: loginButton, withOffset: -12)
            passwordField.autoPinEdge(.leading, to: .leading, of: containter, withOffset: 16)
            passwordField.autoPinEdge(.trailing, to: .trailing, of: containter, withOffset: -16)
            passwordField.autoSetDimension(.height, toSize: 44)
            
            loginField.autoPinEdge(.top, to: .bottom, of: needAuthLabel, withOffset: 16)
            loginField.autoPinEdge(.bottom, to: .top, of: passwordField, withOffset: -12)
            loginField.autoPinEdge(.leading, to: .leading, of: containter, withOffset: 16)
            loginField.autoPinEdge(.trailing, to: .trailing, of: containter, withOffset: -16)
            loginField.autoSetDimension(.height, toSize: 44)
            
            progress.autoCenterInSuperview()
            progress.autoSetDimensions(to: CGSize(width: 24, height: 24))
        }
        super.updateConstraints()
    }
}

class ForbiddenView: UIView {
    var isConstraintInstall: Bool = false
    var forbiddenLabel: UILabel = {
        let label = UILabel()
        label.text = "Доступ закрыт"
        label.textColor = .adaptableBlack
        label.font = GoogleSansFont.bold(with: 20)
        label.textAlignment = .center
        return label
    }()
    var descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = """
        К сожалению, Вам запрещено пользоваться
        данным приложением, в связи
        с нарушением правил использования
        
        """
        label.numberOfLines = 0
        label.textColor = .adaptableDarkGrayVK
        label.font = GoogleSansFont.medium(with: 15)
        label.textAlignment = .center
        return label
    }()
    var containter: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableWhite
        return view
    }()
    let transformIdentity = CGAffineTransform.identity
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func commonInit() {
        self.addSubview(containter)
        containter.addSubview(forbiddenLabel)
        containter.addSubview(descriptionLabel)
        containter.drawBorder(18, width: 0, color: .clear, isOnlyTopCorners: false)
    }
    
    override func updateConstraints() {
        if !isConstraintInstall {
            isConstraintInstall = true
            autoSetDimensions(to: CGSize(width: screenWidth - 32, height: screenHeight))
            //containter.autoSetDimensions(to: CGSize(width: Screen.bounds.width - 16, height: (Screen.bounds.height / 2) - 24))
            containter.autoAlignAxis(toSuperviewAxis: .horizontal)
            containter.autoPinEdge(.trailing, to: .trailing, of: self, withOffset: -8)
            containter.autoPinEdge(.leading, to: .leading, of: self, withOffset: 8)
            
            forbiddenLabel.autoPinEdge(.top, to: .top, of: containter, withOffset: 16)
            forbiddenLabel.autoPinEdge(.trailing, to: .trailing, of: containter, withOffset: -16)
            forbiddenLabel.autoPinEdge(.leading, to: .leading, of: containter, withOffset: 16)
            
            descriptionLabel.autoPinEdge(.top, to: .bottom, of: forbiddenLabel, withOffset: 16)
            descriptionLabel.autoPinEdge(.leading, to: .leading, of: containter, withOffset: 16)
            descriptionLabel.autoPinEdge(.trailing, to: .trailing, of: containter, withOffset: -16)
            descriptionLabel.autoPinEdge(.bottom, to: .bottom, of: containter, withOffset: -16)
        }
        super.updateConstraints()
    }
}
