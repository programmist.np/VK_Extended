//
//  TextInputContainer.swift
//  Epic Phrases
//
//  Created by programmist_np on 15.03.2020.
//  Copyright © 2020 Funny Applications. All rights reserved.
//

import Foundation
import Material
import UIKit

class TextInputContainer: UIView {
    var isConstraintInstall: Bool = false
    var isEditingMode: Bool = false
    var isKeyboardShow: Bool = false
    var isFullFieldOpen: Bool = false
    var keyboardHeight: CGFloat = 0
    let fakeTextView: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableTextView
        return view
    }()
    let errorView: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableTextView
        return view
    }()
    let errorImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "block_48")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableOrange)?.resize(toHeight: 28)?.resize(toWidth: 28)
        return imageView
    }()
    let errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = .extendedPlaceholderText
        label.font = GoogleSansFont.regular(with: 13.5)
        label.numberOfLines = 0
        return label
    }()
    let addButton: IconButton = {
        let image = UIImage(named: "add_circle_outline_28")?.withRenderingMode(.alwaysTemplate).tint(with: .extendedBlue)
        let button = IconButton(image: image, tintColor: .extendedBlue)
        return button
    }()
    let editingModeButton: IconButton = {
        let image = UIImage(named: "text_16")?.withRenderingMode(.alwaysTemplate).tint(with: .extendedBlue)
        let button = IconButton(image: image, tintColor: .extendedBlue)
        return button
    }()
    let textView: TextView = {
        let textView = TextView()
        textView.placeholderLabel.textColor = .extendedPlaceholderText
        textView.textColor = .adaptableBlack
        textView.font = GoogleSansFont.regular(with: 17)
        return textView
    }()
    let emojiButton: IconButton = {
        let image = UIImage(named: "smile_outline_28")?.withRenderingMode(.alwaysTemplate).tint(with: .extendedBlue)
        let button = IconButton(image: image, tintColor: .extendedBlue)
        return button
    }()
    let sendButton: IconButton = {
        let image = UIImage(named: "send_icon")?.withRenderingMode(.alwaysTemplate).tint(with: .extendedBlue)
        let button = IconButton(image: image, tintColor: .extendedBlue)
        return button
    }()
    var selfHeight: NSLayoutConstraint?
    var textContainerHeight: NSLayoutConstraint?
    var textContainerWidth: NSLayoutConstraint?
    var textViewWidth: NSLayoutConstraint?

    override func layoutSubviews() {
        super.layoutSubviews()
        fakeTextView.drawBorder(18, width: 1, color: .adaptableSmokeBlue, isOnlyTopCorners: false)
        sendButton.setBorder(sendButton.roundedSize, width: 0)
        addButton.setBorder(addButton.roundedSize, width: 0, color: .extendedBlue)
        errorView.drawBorder(0, width: 0.5, color: .adaptableSeparator)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.backgroundColor = .adaptableWhite
        self.addSubview(fakeTextView)
        self.addSubview(addButton)
        self.addSubview(sendButton)
        self.addSubview(emojiButton)
        self.addSubview(errorView)
        errorView.addSubview(errorImageView)
        errorView.addSubview(errorLabel)
        fakeTextView.addSubview(textView)
        fakeTextView.addSubview(editingModeButton)
    }
    
    override func updateConstraints() {
        if !isConstraintInstall {
            isConstraintInstall = true
            self.translatesAutoresizingMaskIntoConstraints = true
            
            textView.autoPinEdge(.top, to: .top, of: fakeTextView)
            textView.autoPinEdge(.bottom, to: .bottom, of: fakeTextView)
            textView.autoPinEdge(.leading, to: .leading, of: fakeTextView)
            textViewWidth = textView.autoPinEdge(.trailing, to: .trailing, of: fakeTextView)

            editingModeButton.autoPinEdge(.trailing, to: .trailing, of: fakeTextView, withOffset: -8)
            editingModeButton.autoPinEdge(.bottom, to: .bottom, of: fakeTextView, withOffset: -4)
            editingModeButton.autoSetDimensions(to: CGSize(width: 24, height: 24))
            
            selfHeight = self.autoSetDimension(.height, toSize: 52)
            
            addButton.autoPinEdge(.leading, to: .leading, of: self, withOffset: 10)
            addButton.autoPinEdge(.bottom, to: .bottom, of: self, withOffset: -12)
            addButton.autoSetDimensions(to: CGSize(width: 28, height: 28))
            
            sendButton.autoPinEdge(.trailing, to: .trailing, of: self, withOffset: -12)
            sendButton.autoPinEdge(.bottom, to: .bottom, of: self, withOffset: -9)
            sendButton.autoSetDimensions(to: CGSize(width: 34, height: 34))
            
            emojiButton.autoPinEdge(.trailing, to: .leading, of: sendButton, withOffset: -14)
            emojiButton.autoPinEdge(.bottom, to: .bottom, of: self, withOffset: -12)
            emojiButton.autoSetDimensions(to: CGSize(width: 28, height: 28))
            
            fakeTextView.autoPinEdge(.leading, to: .trailing, of: addButton, withOffset: 10)
            textContainerWidth = fakeTextView.autoPinEdge(.trailing, to: .leading, of: emojiButton, withOffset: -12)
            fakeTextView.autoPinEdge(.bottom, to: .bottom, of: self, withOffset: -8)
            fakeTextView.autoPinEdge(.top, to: .top, of: self, withOffset: 8)
            
            textContainerHeight = textView.autoSetDimension(.height, toSize: 36)
            
            errorView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0, left: -1, bottom: -1, right: -1))
            
            errorImageView.autoPinEdge(.leading, to: .leading, of: errorView, withOffset: 12)
            errorImageView.autoAlignAxis(toSuperviewAxis: .horizontal)
            errorImageView.autoSetDimensions(to: CGSize(width: 28, height: 28))
            
            errorLabel.autoPinEdge(.leading, to: .trailing, of: errorImageView, withOffset: 12)
            errorLabel.autoPinEdge(.trailing, to: .trailing, of: errorView, withOffset: -12)
            errorLabel.autoPinEdge(.top, to: .top, of: errorView, withOffset: 4)
            errorLabel.autoPinEdge(.bottom, to: .bottom, of: errorView, withOffset: -4)
        }
        super.updateConstraints()
    }
    
    func setup(placeholder: String = "Cообщение", canWriteAllow: Bool, writeNotAllowReason: String) {
        errorLabel.text = writeNotAllowReason
        fakeTextView.translatesAutoresizingMaskIntoConstraints = true
        self.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.backgroundColor = .adaptableWhite
        textView.placeholderLabel.text = placeholder
        textView.placeholderLabel.textColor = .extendedPlaceholderText
        textView.textContainerInsets = UIEdgeInsets(top: 6, left: 8, bottom: 6, right: 8)
        sendButton.alpha = 0.5
        sendButton.isEnabled = false
        editingModeButton.isHidden = !isEditingMode
        if canWriteAllow {
            errorView.isHidden = true
            selfHeight?.constant = 52
        } else {
            errorView.isHidden = false
            selfHeight?.constant = 72
        }
    }
    
    func setupEditingMode() {
        editingModeButton.isHidden = !isEditingMode
        textViewWidth?.constant = isEditingMode ? -32 : 0
    }
    
    func textViewHeight(from stringHeight: CGFloat) {
        switch stringHeight {
        case 36.666666666666664:
            selfHeight?.constant = 52
        case 57.333333333333336:
            selfHeight?.constant = 72
        case 78:
            selfHeight?.constant = 92
        case 98.666666666666671:
            selfHeight?.constant = 112
        case 97.333333333333328:
            selfHeight?.constant = 132
        case 119.33333333333333:
            selfHeight?.constant = 132
        default:
            break
        }
    }
}
