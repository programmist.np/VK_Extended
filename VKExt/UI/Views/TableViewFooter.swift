//
//  TableViewFooter.swift
//  Epic Phrases
//
//  Created by programmist_np on 18.03.2020.
//  Copyright © 2020 Funny Applications. All rights reserved.
//

import Foundation
import UIKit
import Material
import MaterialComponents

class FooterView: UIView {
    
    private var myLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = .adaptableDarkGrayVK
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var loader: ActivityIndicatorView = {
        let image = UIImage(named: "progress_24")
        return ActivityIndicatorView(image: image)
    }()
    
    private var activityIndicator: MDCActivityIndicator = {
        let activityIndicator = MDCActivityIndicator()
        activityIndicator.radius = 10
        activityIndicator.strokeWidth = 2
        activityIndicator.indicatorMode = .indeterminate
        activityIndicator.cycleColors = [.adaptableGrayVK]
        return activityIndicator
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(myLabel)
        addSubview(loader)
        
        myLabel.anchor(top: topAnchor,
                       leading: leadingAnchor,
                       bottom: nil,
                       trailing: trailingAnchor,
                       padding: UIEdgeInsets(top: 8, left: 20, bottom: 777, right: 20))
        
        loader.autoAlignAxis(toSuperviewAxis: .vertical)
        loader.autoSetDimensions(to: CGSize(width: 24, height: 24))
        loader.topAnchor.constraint(equalTo: myLabel.bottomAnchor, constant: 0).isActive = true
    }
    
    func showLoader() {
        loader.startAnimating()
    }
    
    func hideLoader() {
        loader.stopAnimating()
    }
    
    func setTitle(_ title: String?) {
        loader.stopAnimating()
        myLabel.text = title
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class TableFooterView: UIView {
    var isConstraintInstall: Bool = false
    
    let textView: UILabel = {
        let label = UILabel(frame: CGRect(origin: .zero, size: CGSize(width: Screen.bounds.width, height: 56)))
        label.font = GoogleSansFont.regular(with: 14)
        label.textColor = .adaptableGrayVK
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    let progressView: UIActivityIndicatorView = {
        let progress = UIActivityIndicatorView(frame: CGRect(origin: .zero, size: CGSize(width: Screen.bounds.width, height: 56)))
        progress.hidesWhenStopped = true
        return progress
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.addSubview(textView)
        self.addSubview(progressView)
    }
    
    override func updateConstraints() {
        if !isConstraintInstall {
            isConstraintInstall = true
            self.frame = CGRect(origin: .zero, size: CGSize(width: Screen.bounds.width, height: 56))
            textView.autoPinEdgesToSuperviewEdges()
            progressView.autoCenterInSuperview()
        }
        super.updateConstraints()
    }
    
    func setStateViews(_ state: Int) {
        switch state {
        case 0:
            textView.isHidden = true
            progressView.isHidden = false
            progressView.startAnimating()
        case 1:
            textView.isHidden = false
            progressView.isHidden = true
            progressView.stopAnimating()
        case 2:
            textView.isHidden = true
            progressView.isHidden = true
            progressView.stopAnimating()
        default:
            break
        }
    }
    
    func setup(text: String, _ state: Int) {
        setStateViews(state)
        textView.text = text
    }
}
