//
//  PulseAdditionalView.swift
//  VK Tosters
//
//  Created by programmist_np on 31.03.2020.
//  Copyright © 2020 programmist_np. All rights reserved.
//

import Foundation
import Material
import UIKit

class PulseAdditionalView: PulseView {
    var isConstraintConnected: Bool = false
    let additionalIcon: UIImageView = {
        let icon = UIImageView()
        return icon
    }()
    let additionalText: UILabel = {
        let label = UILabel()
        label.textColor = .extendedGray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.addSubview(additionalIcon)
        self.addSubview(additionalText)
    }
    
    override open func updateConstraints() {
        if !isConstraintConnected {
            isConstraintConnected = true

            additionalIcon.autoPinEdge(.leading, to: .leading, of: self, withOffset: 16)
            additionalIcon.autoAlignAxis(toSuperviewAxis: .horizontal)
            additionalIcon.autoSetDimensions(to: CGSize(width: 14, height: 14))
            
            additionalText.autoPinEdge(.leading, to: .trailing, of: additionalIcon, withOffset: 8)
            additionalText.autoPinEdge(.trailing, to: .trailing, of: self, withOffset: -16)
            additionalText.autoAlignAxis(toSuperviewAxis: .horizontal)
        }
        super.updateConstraints()
    }
    
    func setupView(image: UIImage?, text: String?, tintColor: UIColor, font: UIFont = GoogleSansFont.regular(with: 14)) {
        additionalText.text = text
        additionalText.textColor = tintColor
        additionalText.font = font
        additionalIcon.image = image?.withRenderingMode(.alwaysTemplate)
        additionalIcon.tintColor = tintColor
    }
}
