//
//  OutgoingReadStateView.swift
//  VK Tosters
//
//  Created by programmist_np on 22.04.2020.
//  Copyright © 2020 programmist_np. All rights reserved.
//

import UIKit

class OutgoingReadStateView: UIView {
    private var isConstraintInstall: Bool = false
    private let rootView: UIView = {
        let rootView = UIView()
        rootView.backgroundColor = .adaptableWhite
        rootView.setCorners(radius: 8)
        return rootView
    }()
    private let readStateView: UIView = {
        let readStateView = UIView()
        readStateView.backgroundColor = .extendedBlue
        readStateView.setCorners(radius: 4)
        return readStateView
    }()
    open var isShowing: Bool = true

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.backgroundColor = .adaptableWhite
        self.addSubview(rootView)
        rootView.addSubview(readStateView)
        // self.isHidden = !isShowing
    }
    
    override func updateConstraints() {
        if !isConstraintInstall {
            isConstraintInstall = true
            self.rootView.autoPinEdge(.top, to: .top, of: self)
            self.rootView.autoPinEdge(.trailing, to: .trailing, of: self)
            self.rootView.autoPinEdge(.leading, to: .leading, of: self)
            self.rootView.autoPinEdge(.bottom, to: .bottom, of: self)
            
            self.readStateView.autoPinEdge(.top, to: .top, of: rootView, withOffset: 4)
            self.readStateView.autoPinEdge(.trailing, to: .trailing, of: rootView, withOffset: -4)
            self.readStateView.autoPinEdge(.leading, to: .leading, of: rootView, withOffset: 4)
            self.readStateView.autoPinEdge(.bottom, to: .bottom, of: rootView, withOffset: -4)
        }
        super.updateConstraints()
    }
}
