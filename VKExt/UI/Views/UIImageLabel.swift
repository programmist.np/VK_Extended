//
//  UIImageLabel.swift
//  VK Tosters
//
//  Created by programmist_np on 22.04.2020.
//  Copyright © 2020 programmist_np. All rights reserved.
//

import UIKit

class UIImageLabel: UILabel {
    var isConstraintConnected: Bool = false
    let label: UILabel = {
        let label = UILabel()
        label.textColor = .adaptableBlack
        label.font = GoogleSansFont.medium(with: 18)
        return label
    }()
    let icon1: UIImageView = {
        let icon = UIImageView()
        return icon
    }()
    let icon2: UIImageView = {
        let icon = UIImageView()
        return icon
    }()
    let icon3: UIImageView = {
        let icon = UIImageView()
        return icon
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.addSubview(label)
        self.addSubview(icon1)
        self.addSubview(icon2)
        self.addSubview(icon3)
    }
    
    override open func updateConstraints() {
        if !isConstraintConnected {
            isConstraintConnected = true
            self.translatesAutoresizingMaskIntoConstraints = true
            
            label.autoPinEdge(.leading, to: .leading, of: self)
            label.autoAlignAxis(toSuperviewAxis: .horizontal)
            
            icon1.autoPinEdge(.leading, to: .trailing, of: label, withOffset: 4)
            icon1.autoAlignAxis(toSuperviewAxis: .horizontal)
            icon1.autoSetDimensions(to: icon1.image != nil ? CGSize(width: self.frame.height / 1.8, height: self.frame.height / 1.8) : .zero)

            icon2.autoPinEdge(.leading, to: .trailing, of: icon1, withOffset: 4)
            icon2.autoAlignAxis(toSuperviewAxis: .horizontal)
            icon2.autoSetDimensions(to: icon1.image != nil ? CGSize(width: self.frame.height / 1.8, height: self.frame.height / 1.8) : .zero)
            
            icon3.autoPinEdge(.leading, to: .trailing, of: icon2, withOffset: 4)
            icon3.autoPinEdge(.trailing, to: .trailing, of: self, withOffset: 0, relation: .equal)
            icon3.autoAlignAxis(toSuperviewAxis: .horizontal)
            icon3.autoSetDimensions(to: icon1.image != nil ? CGSize(width: self.frame.height / 1.8, height: self.frame.height / 1.8) : .zero)
        }
        super.updateConstraints()
    }
    
    func setImageLabel(text: String) {
        label.attributedText = NSAttributedString(string: text)
    }
    
    func setImageLabel(icon1: UIImage?) {
        if let icon = icon1 {
            self.icon1.image = icon
        } else {
            self.icon1.image = nil
        }
    }
    
    func setImageLabel(icon2: UIImage?) {
        if let icon = icon2 {
            self.icon2.image = icon
        } else {
            self.icon2.image = nil
        }
    }
    
    func setImageLabel(icon3: UIImage?) {
        if let icon = icon3 {
            self.icon3.image = icon
        } else {
            self.icon3.image = nil
        }
    }
}
