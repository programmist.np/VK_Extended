//
//  OnlineView.swift
//  VK Tosters
//
//  Created by programmist_np on 30.03.2020.
//  Copyright © 2020 programmist_np. All rights reserved.
//

import Foundation
import Material
import UIKit

public enum OnlineApp: String {
    case mobile = "mobile-online"
    case android = "android-online"
    case iOS = "apple-online"
    case windows = "windows-online"
    case personalComputer = "online-pc"
}

open class OnlineView: UIView {
    var isConstraintConnected: Bool = false
    let onlineText: UILabel = {
        let label = UILabel()
        label.font = GoogleSansFont.regular(with: 14)
        label.textColor = .adaptableGrayVK
        return label
    }()
    let onlineImage: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.addSubview(onlineText)
        self.addSubview(onlineImage)
    }
    
    override open func updateConstraints() {
        if !isConstraintConnected {
            isConstraintConnected = true
            self.frame = CGRect(origin: .zero, size: CGSize(width: Screen.bounds.width, height: 24))
            self.onlineText.autoPinEdge(.leading, to: .leading, of: self)
            self.onlineText.autoAlignAxis(toSuperviewAxis: .horizontal)
            
            self.onlineImage.autoPinEdge(.leading, to: .trailing, of: onlineText, withOffset: 2)
            self.onlineImage.autoAlignAxis(toSuperviewAxis: .horizontal)
            self.onlineImage.autoSetDimensions(to: CGSize(width: 14, height: 14))
        }
        super.updateConstraints()
    }
    
    func setupOnline(onlineApp: Int?, isOnline: Bool?, timeLastOnline: Int?, sex: Int?) {
        switch isOnline {
        case false:
            guard let time = timeLastOnline else {
                self.onlineText.text = "Время последнего посещения скрыто"
                return
            }
            self.onlineText.text = CommonService.getLastSeen(sex: sex!, time: Date(timeIntervalSince1970: Double(time)))
        case true:
            self.onlineText.text = "online"
        default:
            return
        }
        guard let application = onlineApp else { return }
        switch application {
        case 1: // Mobile
            DispatchQueue.global(qos: .default).async {
                let image = UIImage(named: OnlineApp.mobile.rawValue)?.withRenderingMode(.alwaysTemplate)
                DispatchQueue.main.async {
                    self.onlineImage.image = image
                    self.onlineImage.tintColor = UIColor.extendedBlue.withAlphaComponent(0.3)
                }
            }
            
        case 2, 3: // iOS (iPhone/iPad)
            DispatchQueue.global(qos: .default).async {
                let image = UIImage(named: OnlineApp.iOS.rawValue)?.withRenderingMode(.alwaysTemplate)
                DispatchQueue.main.async {
                    self.onlineImage.image = image
                    self.onlineImage.tintColor = UIColor.extendedBlue.withAlphaComponent(0.3)
                }
            }
            
        case 4: // Android
            DispatchQueue.global(qos: .default).async {
                let image = UIImage(named: OnlineApp.android.rawValue)?.withRenderingMode(.alwaysTemplate)
                DispatchQueue.main.async {
                    self.onlineImage.image = image
                    self.onlineImage.tintColor = UIColor.extendedBlue.withAlphaComponent(0.3)
                }
            }

            
        case 5, 6: // Windows (10/Phone)
            DispatchQueue.global(qos: .default).async {
                let image = UIImage(named: OnlineApp.windows.rawValue)?.withRenderingMode(.alwaysTemplate)
                DispatchQueue.main.async {
                    self.onlineImage.image = image
                    self.onlineImage.tintColor = UIColor.extendedBlue.withAlphaComponent(0.3)
                }
            }
            
        case 7: // PC
            DispatchQueue.global(qos: .default).async {
                let image = UIImage(named: OnlineApp.personalComputer.rawValue)?.withRenderingMode(.alwaysTemplate)
                DispatchQueue.main.async {
                    self.onlineImage.image = image
                    self.onlineImage.tintColor = UIColor.extendedBlue.withAlphaComponent(0.3)
                }
            }
            
        default:
            self.onlineImage.image = UIImage()
        }
    }
}

class AvatarOnlineView: UIView {
    var isConstraintConnected: Bool = false
    let onlineImage: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.addSubview(onlineImage)
    }
    
    override open func updateConstraints() {
        if !isConstraintConnected {
            isConstraintConnected = true

            self.onlineImage.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2))
            self.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        }
        super.updateConstraints()
    }
    
    func setupView(isMobileOnline: Bool) {
        if isMobileOnline {
            self.onlineImage.image = UIImage(named: "online_mobile_composite_foreground_20")
        } else {
            self.onlineImage.image = UIImage(named: "online_composite_foreground_20")
        }
    }
}
