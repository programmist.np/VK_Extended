//
//  MiniPlayer.swift
//  VKExt
//
//  Created by programmist_NA on 23.05.2020.
//

import Foundation
import MaterialComponents
import Material
import UIKit

class MiniPlayer: PulseView {
    var isConstraintInstall: Bool = false
    let view: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let playButton: IconButton = {
        let button = IconButton(image: UIImage(named: "pause_48")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableBlue)?.resize(toWidth: 20)?.resize(toHeight: 20), tintColor: .adaptableBlue)
        return button
    }()
    let cancelButton: IconButton = {
        let button = IconButton(image: UIImage(named: "skip_next_48")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableBlue)?.resize(toWidth: 32)?.resize(toHeight: 32), tintColor: .adaptableBlue)
        return button
    }()
    let titleSong: UILabel = {
        let label = UILabel()
        label.textColor = .adaptableBlack
        label.font = GoogleSansFont.medium(with: 16)
        label.textAlignment = .center
        return label
    }()
    let artistSong: UILabel = {
        let label = UILabel()
        label.textColor = .adaptableDarkGrayVK
        label.font = GoogleSansFont.medium(with: 13)
        label.textAlignment = .center
        return label
    }()
    let dividerView: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableDivider
        return view
    }()
    weak var topAncorArtistLabel: NSLayoutConstraint?
    weak var centerXArtistLabel: NSLayoutConstraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        addSubview(view)
        view.addSubview(playButton)
        view.addSubview(cancelButton)
        view.addSubview(artistSong)
        view.addSubview(titleSong)
        view.addSubview(dividerView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func updateConstraints() {
        if !isConstraintInstall {
            isConstraintInstall = true
            
            view.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: -5, left: 0, bottom: 5, right: 0))
            
            playButton.autoPinEdge(.leading, to: .leading, of: view, withOffset: 8)
            playButton.autoSetDimensions(to: CGSize(width: 42, height: 42))
            playButton.autoAlignAxis(toSuperviewAxis: .horizontal)
            
            topAncorArtistLabel = titleSong.autoPinEdge(.top, to: .top, of: view, withOffset: 4)
            centerXArtistLabel = titleSong.autoAlignAxis(toSuperviewAxis: .horizontal)
            centerXArtistLabel?.isActive = false
            titleSong.autoPinEdge(.leading, to: .trailing, of: playButton, withOffset: 8)
            
            artistSong.autoPinEdge(.bottom, to: .bottom, of: view, withOffset: -4)
            artistSong.autoPinEdge(.leading, to: .trailing, of: playButton, withOffset: 8)
            
            cancelButton.autoPinEdge(.leading, to: .trailing, of: titleSong, withOffset: 8)
            cancelButton.autoPinEdge(.leading, to: .trailing, of: artistSong, withOffset: 8)
            cancelButton.autoPinEdge(.trailing, to: .trailing, of: view, withOffset: -8)
            cancelButton.autoSetDimensions(to: CGSize(width: 42, height: 42))
            cancelButton.autoAlignAxis(toSuperviewAxis: .horizontal)
            
            dividerView.autoPinEdge(.top, to: .top, of: view)
            dividerView.autoPinEdge(.leading, to: .leading, of: view)
            dividerView.autoPinEdge(.trailing, to: .trailing, of: view)
            dividerView.autoPinEdge(.bottom, to: .bottom, of: view, withOffset: -41.6)
        }
        super.updateConstraints()
    }
    
    func setup(title: String?, artist: String?) {
        if artist == "" {
            topAncorArtistLabel?.isActive = false
            centerXArtistLabel?.isActive = true
        } else {
            topAncorArtistLabel?.isActive = true
            centerXArtistLabel?.isActive = false
        }
        view.backgroundColor = .clear
        backgroundColor = .clear
        layer.backgroundColor = UIColor.clear.cgColor
        titleSong.text = title
        artistSong.text = artist
    }
}
