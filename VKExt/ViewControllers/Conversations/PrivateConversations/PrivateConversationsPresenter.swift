//
//  PrivateConversationsPresenter.swift
//  VKExt
//
//  Created programmist_NA on 07.06.2020.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit

class PrivateConversationsPresenter: PrivateConversationsPresenterProtocol {

    weak private var view: PrivateConversationsViewProtocol?
    var interactor: PrivateConversationsInteractorProtocol?
    private let router: PrivateConversationsWireframeProtocol

    init(interface: PrivateConversationsViewProtocol, interactor: PrivateConversationsInteractorProtocol?, router: PrivateConversationsWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    func start() {
        guard let interactor = self.interactor else { return }
        self.view?.finish()
    }
    
    func presentData(response: Conversations.Model.Response.ResponseType) {
        switch response {
        case .presentConversation(conversation: _):
            break
        case .presentConversations(conversations: let conversations):
            let cells = conversations.compactMap { (item) in
                conversationCellViewModel(from: item)
            }
            
            _ = ConversationViewModel.init(cells: cells, footerTitle: "")
        case .presentFooterLoader:
            break
        case .presentFooterError(message: _):
            break
        }
    }
    
    private func conversationCellViewModel(from conversation: Conversation) -> ConversationViewModel.Cell {
        let conversationCellViewModel = ConversationViewModel.Cell.init(isMuted: conversation.isMuted, unreadStatus: conversation.unreadStatus, isOutgoing: conversation.isOutgoing, senderId: conversation.senderId, displayName: conversation.displayName, peerId: conversation.peerId, type: conversation.type, localId: conversation.localId, lastMessageId: conversation.lastMessageId, inRead: conversation.inRead, outRead: conversation.outRead, canWriteAllowed: conversation.canWriteAllowed, canWriteReason: conversation.canWriteReason, isImportantDialog: conversation.isImportantDialog, isMarkedUnread: conversation.isMarkedUnread, membersCount: conversation.membersCount, title: conversation.title, state: conversation.state, activeIds: conversation.activeIds, isGroupChannel: conversation.isGroupChannel, disabledUntil: conversation.disabledUntil, disabledForever: conversation.disabledForever, noSound: conversation.noSound, unreadCount: conversation.unreadCount, date: conversation.date, dateInteger: conversation.dateInteger, fromId: conversation.fromId, messageId: conversation.messageId, out: conversation.out, text: conversation.text, conversationMessageId: conversation.conversationMessageId, isImportantMessage: conversation.isImportantMessage, randomId: conversation.randomId, isTyping: conversation.isTyping, attachmentType: conversation.secondType, forwardMessagesCount: conversation.forwardMessagesCount, hasReplyMessage: conversation.hasReplyMessage, hasAttachments: conversation.hasAttachments, hasForwardedMessages: conversation.hasForwardedMessages, interlocutorId: conversation.interlocutorId, interlocutorName: conversation.interlocutorName, interlocutorPhoto50: conversation.interlocutorPhoto50, interlocutorPhoto100: conversation.interlocutorPhoto100, interlocutorPhoto200: conversation.interlocutorPhoto200, isOnline: conversation.isOnline, isMobileOnline: conversation.isMobileOnline, sex: conversation.sex, lastSeen: conversation.lastSeen, verified: conversation.verified, isRemoved: conversation.isRemoved, removingFlag: conversation.removingFlag, isAggressiveTypingActive: conversation.isAggressiveTypingActive, aggressiveTypingType: conversation.aggressiveTypingType, isPrivateConversation: conversation.isPrivateConversation)
        return conversationCellViewModel
    }
}
