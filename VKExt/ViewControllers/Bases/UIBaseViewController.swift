//
//  UIBaseViewController.swift
//  VKExt
//
//  Created by programmist_NA on 20.05.2020.
//

import Foundation
import Reachability
import PureLayout
import Material
import SwiftMessages
import MaterialComponents
import BottomPopup
import AsyncDisplayKit
import EasyTipView

protocol UIBaseViewControllerProtocol: class {
    func setupToolbar(title: String, subtitle: String, leftViews: [UIView]?, rightViews: [UIView]?)
}

open class UIBaseViewController: ViewController {
    var toolbar: Toolbar = Toolbar(frame: CGRect(origin: .zero, size: CGSize(width: Screen.bounds.width, height: 52)))
    var statusBarView: UIView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: Screen.bounds.width, height: GlobalConstants.statusBarHeight)))
    let tableView: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableWhite
        return view
    }()
    let devView: PageDevView = PageDevView(frame: .zero)
    let panGesture = UIPanGestureRecognizer()
    var preferences = EasyTipView.Preferences()
    let reachability = try! Reachability()
    var statusConfig = SwiftMessages.defaultConfig
    lazy var networkEventProducer: NetworkEventProducer = {
        NetworkEventProducer(reachability: reachability)
    }()
    let blurView: UIVisualEffectView = {
        let view = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return view
    }()
    weak var topOffset: NSLayoutConstraint?
    
    override open func viewDidLoad() {
        self.viewDidLoad()
        prepareToolbar(fonts: [GoogleSansFont.bold(with: 21), GoogleSansFont.regular(with: 13)])
        self.navigationController?.invisible
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        networkEventProducer.eventListener = self
        networkEventProducer.startProducingEvents()
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        networkEventProducer.eventListener = nil
        networkEventProducer.stopProducingEvents()
    }
    
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    // Подготовка контейнера
    func prepareContainer() {
        self.view.addSubview(tableView)
        topOffset = tableView.autoPinEdge(.top, to: .top, of: self.view, withOffset: getCommonInset(by: [.toolbar, .statusbar]))
        tableView.autoPinEdge(.trailing, to: .trailing, of: self.view)
        tableView.autoPinEdge(.leading, to: .leading, of: self.view)
        tableView.autoPinEdge(.bottom, to: .bottom, of: self.view)
    }
    
    // Подготовка тулбара
    func prepareToolbar(fonts: [UIFont]) {
        self.view.addSubview(toolbar)
        toolbar.autoPinEdge(toSuperviewSafeArea: .top)
        toolbar.autoPinEdge(toSuperviewSafeArea: .trailing)
        toolbar.autoPinEdge(toSuperviewSafeArea: .leading)
        toolbar.autoSetDimension(.height, toSize: 52)
        toolbar.backgroundColor = .adaptableWhite
        toolbar.titleLabel.textColor = .adaptableBlack
        toolbar.titleLabel.font = fonts[0]
        toolbar.detailLabel.textColor = .adaptableDarkGrayVK
        toolbar.detailLabel.font = fonts[1]
    }
    
    // Инициализация ошибочного экрана
    func initialError() {
        self.tableView.addSubview(devView)
        devView.autoCenterInSuperview()
        devView.setup(message: """
    Требуется авторизация
    для получения токена
    """, image: UIImage(named: "touch_id_outline_56")!)
        devView.isHidden = true
    }
    
    // Показать ошибку
    func showError(message: String, image: UIImage?) {
        devView.isHidden = false
        devView.setup(message: """
        \(message)
        """, image: image)
    }
    
    // Убрать ошибку
    func hideError() {
        devView.isHidden = true
    }
    
    func prepareInteractivePopGesture() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        guard let systemGesture = navigationController?.interactivePopGestureRecognizer else { return }
        guard let gestureView = systemGesture.view else { return }
        let targets = systemGesture.value(forKey: "_targets") as? [NSObject]
        guard let targetObjc = targets?.first else { return }
        guard let target = targetObjc.value(forKey: "target") else { return }
        let action = Selector(("handleNavigationTransition:"))
        gestureView.addGestureRecognizer(panGesture)
        panGesture.addTarget(target, action: action)
    }
    
    // Инициализация меню
    func initialActionSheet(title: String, message: String, actions: [MDCActionSheetAction]) {
        let actionSheet = MDCActionSheetController()
        actionSheet.backgroundColor = UIColor.adaptableWhite.withAlphaComponent(0.0)
        actionSheet.view.layer.backgroundColor = UIColor.adaptableWhite.withAlphaComponent(0.2).cgColor

        actionSheet.titleFont = GoogleSansFont.bold(with: 20)
        actionSheet.messageFont = GoogleSansFont.medium(with: 16)
        actionSheet.actionFont = GoogleSansFont.medium(with: 15)
        
        actionSheet.titleTextColor = .adaptableBlack
        actionSheet.messageTextColor = UIColor.adaptableBlack.withAlphaComponent(0.5)
        actionSheet.actionTextColor = .adaptableBlack
        actionSheet.actionTintColor = .adaptableBlack
        
        blurView.frame = actionSheet.view.bounds
        actionSheet.view.insertSubview(blurView, at: 0)

        actionSheet.title = title
        actionSheet.message = message
        
        for action in actions {
            actionSheet.addAction(action)
        }
        present(actionSheet, animated: true, completion: nil)
    }
    
    // Установка свойств тултипа
    func setupTooltip() {
        preferences.drawing.font = GoogleSansFont.medium(with: 13)
        preferences.drawing.foregroundColor = .adaptableWhite
        preferences.drawing.backgroundColor = .adaptableBlue
        preferences.drawing.arrowPosition = .bottom
        preferences.drawing.cornerRadius = 8
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 0.7
        preferences.animating.dismissDuration = 0.7
        preferences.animating.dismissOnTap = true
        preferences.positioning.bubbleVInset = 8
    }
    
    func showStatusError(message: String = "", error: ErrorLocalize? = nil, _ style: MessageViewState, with isLoading: Bool = false) {
        statusConfig.presentationContext = .view(tableView)
        statusConfig.presentationStyle = .top
        let message = message == "" ? error?.rawValue : message
        switch style {
        case .normal:
            if #available(iOS 13.0, *) {
                Vibration.soft.vibrate()
            } else {
                Vibration.medium.vibrate()
            }
        case .success:
            Vibration.success.vibrate()
        case .warning:
            Vibration.warning.vibrate()
        case .error:
            Vibration.error.vibrate()
        }
        SwiftMessages.show(config: statusConfig, view: self.messageView(message: "\(message ?? "")", style, with: isLoading))
    }
    
    func handleNetworkEvent(from producer: EventProducer, with event: NetworkEventProducer.NetworkEvent) {
        SwiftMessages.hideAll()
        switch event {
        case .connectionLost:
            self.statusConfig.duration = .forever
            self.showStatusError(message: "Ожидание соединения", .error, with: true)

        case .connectionRetrieved:
            self.statusConfig.duration = .seconds(seconds: 3)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.showStatusError(message: "Подключено", .success)
            })

        case .networkChanged:
            self.statusConfig.duration = .seconds(seconds: 3)
            self.showStatusError(message: "Смена соединения c \(networkEventProducer.lastStatus.translate(with: .gen)) на \(networkEventProducer.reachability.connection.translate())", .normal)
        }
    }
}
extension UIBaseViewController: EventListener {
    func onEvent(_ event: Event, generetedBy eventProducer: EventProducer) {
        if let event = event as? NetworkEventProducer.NetworkEvent {
            handleNetworkEvent(from: eventProducer, with: event)
        }
    }
}

extension UIBaseTabsViewController: EventListener {
    func onEvent(_ event: Event, generetedBy eventProducer: EventProducer) {
        if let event = event as? NetworkEventProducer.NetworkEvent {
            handleNetworkEvent(from: eventProducer, with: event)
        }
    }
}

extension Reachability.Connection {
    func translate(with nameCase: NameCase = .acc) -> String {
        switch self {
        case .wifi:
            return "Wi-Fi"
        case .cellular:
            return nameCase == .gen ? "мобильной сети" : "мобильную сеть"
        default:
            return ""
        }
    }
}

open class UIBaseTabsViewController: TabsController {
    var toolbar: Toolbar = Toolbar(frame: CGRect(origin: .zero, size: CGSize(width: Screen.bounds.width, height: 52)))
    var statusBarView: UIView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: Screen.bounds.width, height: GlobalConstants.statusBarHeight)))
    let tableView: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableWhite
        return view
    }()
    let devView: PageDevView = PageDevView(frame: .zero)
    var actionSheet: MDCActionSheetController!
    let panGesture = UIPanGestureRecognizer()
    var preferences = EasyTipView.Preferences()
    let reachability = try! Reachability()
    var statusConfig = SwiftMessages.defaultConfig
    lazy var networkEventProducer: NetworkEventProducer = {
        NetworkEventProducer(reachability: reachability)
    }()
    let mainTable = UITableView(frame: .zero, style: .plain)
    let windowView: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableWhite
        return view
    }()
    var newsItem: TabItem = {
        let item = TabItem(title: "Новости")
        item.setTabItemColor(.adaptableBlack, for: .selected)
        item.setTabItemColor(.adaptableDarkGrayVK, for: .normal)
        item.titleLabel?.font = GoogleSansFont.medium(with: 17)
        return item
    }()
    var suggestionItem: TabItem = {
        let item = TabItem(title: "Интересное")
        item.setTabItemColor(.adaptableBlack, for: .selected)
        item.setTabItemColor(.adaptableDarkGrayVK, for: .normal)
        item.titleLabel?.font = GoogleSansFont.medium(with: 17)
        return item
    }()
    var cameraButton: IconButton = {
        let button = IconButton(image: UIImage(named: "camera_outline_28")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableBlue), tintColor: .adaptableBlue)
        return button
    }()
    var notificationButton: IconButton = {
        let button = IconButton(image: UIImage(named: "notification_outline_28")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableBlue), tintColor: .adaptableBlue)
        return button
    }()
    let blurView: UIVisualEffectView = {
        let view = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return view
    }()

    lazy var refreshControl = UIRefreshControl()
    
    public override init(viewControllers: [UIViewController], selectedIndex: Int = 0) {
        super.init(viewControllers: viewControllers)
        self.viewControllers = viewControllers
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    open override func prepare() {
        super.prepare()
        navigationController?.invisible
    }
    
    override open func viewDidLoad() {
        // prepareContainer()
        view.addSubview(windowView)
        windowView.autoPinEdge(.top, to: .top, of: self.view, withOffset: 0)
        windowView.autoPinEdge(.trailing, to: .trailing, of: self.view, withOffset: 0)
        windowView.autoPinEdge(.leading, to: .leading, of: self.view, withOffset: 0)
        windowView.autoSetDimension(.height, toSize: getCommonInset(by: [.statusbar]))
        // prepareInteractivePopGesture()
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        // prepareTable()
        setupTooltip()
        prepareToolbar(fonts: [GoogleSansFont.bold(with: 21), GoogleSansFont.regular(with: 13)])
        setupToolbar(title: "", subtitle: "", leftViews: [cameraButton], rightViews: [notificationButton])
        setupTabBar()
    }
    
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        networkEventProducer.eventListener = self
        networkEventProducer.startProducingEvents()
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        networkEventProducer.eventListener = nil
        networkEventProducer.stopProducingEvents()
    }
    
    func setupTabBar() {
        tabBar.frame.size.width = toolbar.centerViews.first?.bounds.width ?? 0
        tabBarAlignment = .top
        tabBar.backgroundColor = .adaptableWhite
        tabBar.dividerColor = .clear
        tabBar.tabItems.append(contentsOf: [newsItem, suggestionItem])
        toolbar.centerViews = [tabBar]
    }
    
    // Подготовка контейнера
    func prepareContainer() {
        self.view.addSubview(tableView)
        tableView.autoPinEdge(.top, to: .top, of: self.view, withOffset: getCommonInset(by: [.toolbar, .statusbar]))
        tableView.autoPinEdge(.trailing, to: .trailing, of: self.view)
        tableView.autoPinEdge(.leading, to: .leading, of: self.view)
        tableView.autoPinEdge(.bottom, to: .bottom, of: self.view)
    }
    
    // Подготовка тулбара
    func prepareToolbar(fonts: [UIFont]) {
        self.view.addSubview(toolbar)
        toolbar.autoPinEdge(toSuperviewSafeArea: .top)
        toolbar.autoPinEdge(toSuperviewSafeArea: .trailing)
        toolbar.autoPinEdge(toSuperviewSafeArea: .leading)
        toolbar.autoSetDimension(.height, toSize: 52)
        toolbar.backgroundColor = .adaptableWhite
        toolbar.titleLabel.textColor = .adaptableBlack
        toolbar.titleLabel.font = fonts[0]
        toolbar.detailLabel.textColor = .adaptableDarkGrayVK
        toolbar.detailLabel.font = fonts[1]
    }
    
    func setupToolbar(title: String, subtitle: String, leftViews: [UIView]?, rightViews: [UIView]?) {
        toolbar.title = title
        toolbar.detail = subtitle
        if let leftViews = leftViews {
            toolbar.leftViews = leftViews
        }
        if let rightViews = rightViews {
            toolbar.rightViews = rightViews
        }
    }
    
    // Инициализация ошибочного экрана
    func initialError() {
        self.tableView.addSubview(devView)
        devView.autoCenterInSuperview()
        devView.setup(message: """
    Требуется авторизация
    для получения токена
    """, image: UIImage(named: "touch_id_outline_56")!)
        devView.isHidden = true
    }
    
    // Показать ошибку
    func showError(message: String, image: UIImage?) {
        devView.isHidden = false
        devView.setup(message: """
        \(message)
        """, image: image)
    }
    
    // Убрать ошибку
    func hideError() {
        devView.isHidden = true
    }
    
    func prepareInteractivePopGesture() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        guard let systemGesture = navigationController?.interactivePopGestureRecognizer else { return }
        guard let gestureView = systemGesture.view else { return }
        let targets = systemGesture.value(forKey: "_targets") as? [NSObject]
        guard let targetObjc = targets?.first else { return }
        guard let target = targetObjc.value(forKey: "target") else { return }
        let action = Selector(("handleNavigationTransition:"))
        gestureView.addGestureRecognizer(panGesture)
        panGesture.addTarget(target, action: action)
    }
    
    // Инициализация меню
    func initialActionSheet(title: String, message: String, actions: [MDCActionSheetAction]) {
        blurView.frame = actionSheet.view.bounds
        actionSheet = MDCActionSheetController(title: title, message: message)
        actionSheet.backgroundColor = UIColor.adaptableWhite.withAlphaComponent(0.4)
        actionSheet.view.layer.backgroundColor = UIColor.adaptableWhite.withAlphaComponent(0.4).cgColor
        
        actionSheet.view.insertSubview(blurView, at: 0)
        
        actionSheet.titleFont = GoogleSansFont.bold(with: 20)
        actionSheet.messageFont = GoogleSansFont.medium(with: 16)
        actionSheet.actionFont = GoogleSansFont.medium(with: 15)
        
        actionSheet.titleTextColor = .adaptableBlack
        actionSheet.messageTextColor = UIColor.adaptableBlack.withAlphaComponent(0.5)
        actionSheet.actionTextColor = .adaptableBlack
        actionSheet.actionTintColor = .adaptableBlack
        
        for action in actions {
            actionSheet.addAction(action)
        }
        present(actionSheet, animated: true, completion: nil)
    }
    
    // Установка свойств тултипа
    func setupTooltip() {
        preferences.drawing.font = GoogleSansFont.medium(with: 13)
        preferences.drawing.foregroundColor = .adaptableWhite
        preferences.drawing.backgroundColor = .adaptableBlue
        preferences.drawing.arrowPosition = .bottom
        preferences.drawing.cornerRadius = 8
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 0.7
        preferences.animating.dismissDuration = 0.7
        preferences.animating.dismissOnTap = true
        preferences.positioning.bubbleVInset = 8
    }
    
    func showStatusError(message: String = "", error: ErrorLocalize? = nil, _ style: MessageViewState, with isLoading: Bool = false) {
        statusConfig.presentationContext = .view(tableView)
        statusConfig.presentationStyle = .top
        let message = message == "" ? error?.rawValue : message
        switch style {
        case .normal:
            if #available(iOS 13.0, *) {
                Vibration.soft.vibrate()
            } else {
                Vibration.medium.vibrate()
            }
        case .success:
            Vibration.success.vibrate()
        case .warning:
            Vibration.warning.vibrate()
        case .error:
            Vibration.error.vibrate()
        }
        SwiftMessages.show(config: statusConfig, view: self.messageView(message: "\(message ?? "")", style, with: isLoading))
    }
    
    func handleNetworkEvent(from producer: EventProducer, with event: NetworkEventProducer.NetworkEvent) {
        SwiftMessages.hideAll()
        switch event {
        case .connectionLost:
            self.statusConfig.duration = .forever
            self.showStatusError(message: "Ожидание соединения", .error, with: true)

        case .connectionRetrieved:
            self.statusConfig.duration = .seconds(seconds: 3)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.showStatusError(message: "Подключено", .success)
            })

        case .networkChanged:
            self.statusConfig.duration = .seconds(seconds: 3)
            self.showStatusError(message: "Смена соединения c \(networkEventProducer.lastStatus.translate(with: .gen)) на \(networkEventProducer.reachability.connection.translate())", .normal)
        }
    }

    func prepareTable() {
        tableView.addSubview(mainTable)
        mainTable.addSubview(refreshControl)
        mainTable.separatorStyle = .none
        mainTable.autoPinEdgesToSuperviewEdges()
        mainTable.backgroundColor = .adaptableWhite
        refreshControl.tintColor = .adaptableBlue
    }
}
