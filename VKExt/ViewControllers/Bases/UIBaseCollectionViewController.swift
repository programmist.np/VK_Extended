//
//  UIBaseCollectionViewController.swift
//  VKExt
//
//  Created by programmist_NA on 01.06.2020.
//

import Foundation
import MaterialComponents
import Material

protocol UIBaseCollectionViewControllerProtocol {
    func initialCollection()
    func refreshCollection(_ sender: Any?)
}

open class UIBaseCollectionViewController: UIBaseViewController {
    let mainCollection = CollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    let windowView: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableWhite
        return view
    }()
    let collectionView: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableWhite
        return view
    }()
    lazy var refreshControl = UIRefreshControl()
    var bottomConstraint: NSLayoutConstraint?

    override open func viewDidLoad() {
        navigationController?.invisible
        view.addSubview(windowView)
        windowView.autoPinEdge(.top, to: .top, of: self.view, withOffset: 0)
        windowView.autoPinEdge(.trailing, to: .trailing, of: self.view, withOffset: 0)
        windowView.autoPinEdge(.leading, to: .leading, of: self.view, withOffset: 0)
        windowView.autoSetDimension(.height, toSize: GlobalConstants.statusBarHeight)
        prepareInteractivePopGesture()
        // self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    func prepareCollection() {
        self.view.addSubview(collectionView)
        collectionView.autoPinEdge(.top, to: .top, of: self.view, withOffset: GlobalConstants.statusBarHeight + 52)
        collectionView.autoPinEdge(.trailing, to: .trailing, of: self.view)
        collectionView.autoPinEdge(.leading, to: .leading, of: self.view)
        bottomConstraint = collectionView.autoPinEdge(.bottom, to: .bottom, of: self.view, withOffset: -42)
        collectionView.addSubview(mainCollection)
        mainCollection.addSubview(refreshControl)
        mainCollection.transform = GlobalConstants.transform
        mainCollection.autoPinEdgesToSuperviewEdges()
        mainCollection.backgroundColor = .adaptableWhite
        refreshControl.tintColor = .adaptableBlue
    }
}
