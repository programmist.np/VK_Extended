//
//  UIBaseTableViewController.swift
//  VKExt
//
//  Created by programmist_NA on 20.05.2020.
//

import Foundation
import SwiftyVK
import PureLayout
import Material
import MaterialComponents
import SwiftMessages
import AsyncDisplayKit

protocol UIBaseTableViewControllerProtocol {
    func setupTable()
    func initialFooter()
    func refreshTable(_ sender: Any?)
}

open class UIBaseTableViewController: UIBaseViewController {
    let mainTable = UITableView(frame: .zero, style: .plain)
    let windowView: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableWhite
        return view
    }()

    lazy var refreshControl = UIRefreshControl()

    override open func viewDidLoad() {
        prepareContainer()
        navigationController?.invisible
        view.addSubview(windowView)
        windowView.autoPinEdge(.top, to: .top, of: self.view, withOffset: 0)
        windowView.autoPinEdge(.trailing, to: .trailing, of: self.view, withOffset: 0)
        windowView.autoPinEdge(.leading, to: .leading, of: self.view, withOffset: 0)
        windowView.autoSetDimension(.height, toSize: getCommonInset(by: [.statusbar]))
        prepareInteractivePopGesture()
        // navigationController?.interactivePopGestureRecognizer?.delegate = nil
        prepareTable()
        setupTooltip()
    }
    
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    func prepareTable() {
        tableView.addSubview(mainTable)
        mainTable.addSubview(refreshControl)
        mainTable.separatorStyle = .none
        mainTable.autoPinEdgesToSuperviewEdges()
        mainTable.backgroundColor = .adaptableWhite
        refreshControl.tintColor = .adaptableBlue
    }
}

open class UIBaseTableNodeController: UIBaseViewController {
    var mainTableNode = ASTableNode(style: .grouped)
    let windowView: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableWhite
        return view
    }()
    lazy var refreshControl = UIRefreshControl()
    weak var topConstraint: NSLayoutConstraint?

    override open func viewDidLoad() {
        view.addSubview(windowView)
        navigationController?.invisible
        windowView.autoPinEdge(.top, to: .top, of: self.view, withOffset: 0)
        windowView.autoPinEdge(.trailing, to: .trailing, of: self.view, withOffset: 0)
        windowView.autoPinEdge(.leading, to: .leading, of: self.view, withOffset: 0)
        windowView.autoSetDimension(.height, toSize: getCommonInset(by: [.statusbar]))
        // prepareInteractivePopGesture()
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
        prepareTable()
    }
    
    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    func prepareTable() {
        self.view.addSubview(tableView)
        topConstraint = tableView.autoPinEdge(.top, to: .top, of: self.view, withOffset: getCommonInset(by: [.toolbar, .statusbar]))
        tableView.autoPinEdge(.trailing, to: .trailing, of: self.view)
        tableView.autoPinEdge(.leading, to: .leading, of: self.view)
        tableView.autoPinEdge(.bottom, to: .bottom, of: self.view)
        tableView.addSubnode(mainTableNode)
        mainTableNode.view.addSubview(refreshControl)
        mainTableNode.view.separatorStyle = .none
        mainTableNode.view.autoPinEdgesToSuperviewEdges()
        mainTableNode.backgroundColor = .adaptableWhite
        refreshControl.tintColor = .adaptableBlue
    }
}
extension UIBaseTableNodeController {
    func handleError(at vkError: VKError) {
        switch vkError {
        case .api(let error):
            self.showStatusError(error: ErrorLocalize(rawValue: "error\(error.code)") ?? .error1, .error)
        case .authorizationDenied:
            self.showStatusError(error: .authorizationDenied, .error)
        case .urlRequestError:
            self.showStatusError(error: .urlRequestError, .error)
        default: break
        }
    }
    
    func handleError(at errorCode: Int) -> String {
        return Errors.getError(at: errorCode).rawValue
    }
}
