//
//  SampleViewControllerFirst.swift
//  VKExt
//
//  Created by programmist_NA on 12.07.2020.
//

import UIKit
import Material

class SampleTabsController: TabsController {
    var toolbar: Toolbar = Toolbar(frame: CGRect(origin: .zero, size: CGSize(width: Screen.bounds.width, height: 52)))
    var cameraButton: IconButton = {
        let button = IconButton(image: UIImage(named: "camera_outline_28")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableBlue), tintColor: .adaptableBlue)
        return button
    }()
    var notificationButton: IconButton = {
        let button = IconButton(image: UIImage(named: "notification_outline_28")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableBlue), tintColor: .adaptableBlue)
        button.addTarget(self, action: #selector(didChangeTabBarType), for: .touchUpInside)
        return button
    }()
    let windowView: UIView = {
        let view = UIView()
        view.backgroundColor = .adaptableWhite
        return view
    }()

    open override func prepare() {
        super.prepare()
        view.addSubview(windowView)
        navigationController?.invisible
        windowView.autoPinEdge(.top, to: .top, of: self.view, withOffset: 0)
        windowView.autoPinEdge(.trailing, to: .trailing, of: self.view, withOffset: 0)
        windowView.autoPinEdge(.leading, to: .leading, of: self.view, withOffset: 0)
        windowView.autoSetDimension(.height, toSize: getCommonInset(by: [.statusbar]))
        
        prepareToolbar(fonts: [GoogleSansFont.bold(with: 21), GoogleSansFont.regular(with: 13)])
        setupToolbar(title: "", subtitle: "", leftViews: [cameraButton], rightViews: [notificationButton])
        
        tabBar.frame.size.width = toolbar.centerViews.first?.bounds.width ?? 0
        tabBarAlignment = .top
        tabBar.backgroundColor = .adaptableWhite
        tabBar.dividerColor = .clear
        toolbar.centerViews = [tabBar]
    }
    
    // Подготовка тулбара
    func prepareToolbar(fonts: [UIFont]) {
        self.view.addSubview(toolbar)
        toolbar.autoPinEdge(toSuperviewSafeArea: .top)
        toolbar.autoPinEdge(toSuperviewSafeArea: .trailing)
        toolbar.autoPinEdge(toSuperviewSafeArea: .leading)
        toolbar.autoSetDimension(.height, toSize: 52)
        toolbar.backgroundColor = .adaptableWhite
        toolbar.titleLabel.textColor = .adaptableBlack
        toolbar.titleLabel.font = fonts[0]
        toolbar.detailLabel.textColor = .adaptableDarkGrayVK
        toolbar.detailLabel.font = fonts[1]
    }
    
    func setupToolbar(title: String, subtitle: String, leftViews: [UIView]?, rightViews: [UIView]?) {
        toolbar.title = title
        toolbar.detail = subtitle
        if let leftViews = leftViews {
            toolbar.leftViews = leftViews
        }
        if let rightViews = rightViews {
            toolbar.rightViews = rightViews
        }
    }
    
    @objc func didChangeTabBarType() {
        guard let rootViewController = UIApplication.shared.rootViewController as? InitialNavigationController,
            let bottomBarController = rootViewController.children.first as? BottomNavigationViewController else { return }
        
        if bottomBarController.style == .smallNoTitle {
            UserDefaults.standard.set(1, forKey: "tabBarStyle")
            bottomBarController.style = .normalWithTitle
        } else {
            UserDefaults.standard.set(0, forKey: "tabBarStyle")
            bottomBarController.style = .smallNoTitle
        }
    }
}
