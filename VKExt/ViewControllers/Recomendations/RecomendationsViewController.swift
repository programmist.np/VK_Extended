//
//  RecomendationsViewController.swift
//  VKExt
//
//  Created by programmist_NA on 25.05.2020.
//

import UIKit
import Material
import MaterialComponents

class RecomendationsViewController: UIBaseTableViewController, UIBaseViewControllerProtocol, UIBaseTableViewControllerProtocol {
    var images: [String] = ["users_3_outline_28", "music_outline_28", "video_outline_28", "live_outline_28", "smile_outline_28", "podcast_outline_28"]
    var titles: [String] = ["Сообщества", "Музыка", "Видео", "Трансляции", "Стикеры", "Подкасты"]
    var tints: [UIColor] = [.adaptableOrange, .extendedBlue, .adaptableViolet, .adaptableRed, .adaptableOrange, .adaptableBlack]
    var searchController: VKSearchBar = VKSearchBar(frame: CGRect(origin: CGPoint(x: 0, y: GlobalConstants.statusBarHeight), size: CGSize(width: Screen.bounds.width, height: 56)))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTable()
        setupTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        reload
    }
    
    func setupToolbar(title: String, subtitle: String, leftViews: [UIView]?, rightViews: [UIView]?) {
        
    }
    
    func setupTable() {
        mainTable.keyboardDismissMode = .onDrag
        mainTable.allowsMultipleSelectionDuringEditing = true
        mainTable.separatorStyle = .none
        mainTable.delegate = self
        mainTable.dataSource = self
        mainTable.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        mainTable.bounces = false
        
        self.view.addSubview(searchController)
        searchController.autoPinEdge(.top, to: .top, of: self.view)
        searchController.autoPinEdge(.trailing, to: .trailing, of: self.view)
        searchController.autoPinEdge(.leading, to: .leading, of: self.view)
        searchController.autoSetDimension(.height, toSize: 52)
        searchController.setup(placeholder: "Поиск")
    }
    
    var reload: Void {
        mainTable.reloadData()
    }
    
    func initialFooter() {
    }
    
    @objc func refreshTable(_ sender: Any?) {
        mainTable.reloadData()
    }
}
extension RecomendationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard images.count == titles.count else { return 0 }
        return images.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        cell.setupLayer(title: titles[indexPath.row], image: UIImage(named: images[indexPath.row])!, tint: tints[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        switch indexPath.row {
        case 1:
            let audioViewController = AudioViewController()
            self.navigationController?.pushViewController(audioViewController, animated: true)
        default:
            break
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
