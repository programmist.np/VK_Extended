//
//  FullInfoViewController.swift
//  VKExt
//
//  Created by Ярослав Стрельников on 30.07.2020.
//

import UIKit
import PanModal
import Material
import AsyncDisplayKit

class FullInfoViewController: UIBaseTableNodeController, PanModalPresentable {
    var dismissButton: IconButton = {
        let button = IconButton(image: UIImage(named: "dismiss_24")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableDarkGrayVK), tintColor: .adaptableDarkGrayVK)
        return button
    }()
    private var friendViewModel = FriendViewModel.init(cell: [], footerTitle: nil)
    private lazy var footerView = FooterView()
    var service: FriendsService?

    var panScrollable: UIScrollView? {
        return nil
    }
    
    var panModalBackgroundColor: UIColor {
        return UIColor.black.withAlphaComponent(0.35)
    }
    
//    var shortFormHeight: PanModalHeight {
//        return .contentHeight(screenHeight / 2)
//    }
    
    var longFormHeight: PanModalHeight {
        return .maxHeightWithTopInset(getCommonInset(by: [.statusbar]))
    }
    
    var showDragIndicator: Bool {
        return false
    }
    
    var anchorModalToLongForm: Bool {
        return true
    }
    
    var shouldRoundTopCorners: Bool {
        return true
    }
    
    var cornerRadius: CGFloat {
        return 14
    }
    
    var springDamping: CGFloat {
        return 1
    }
    
    var transitionDuration: Double {
        return 0.45
    }
    
    var transitionAnimationOptions: UIView.AnimationOptions {
        return [.curveEaseOut, .allowUserInteraction, .beginFromCurrentState, .preferredFramesPerSecond60]
    }

    init(userId: Int) {
        super.init(nibName: nil, bundle: nil)
        makeRequest(request: .getFollowers(userId: userId))
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .adaptableWhite
        
        prepareTable()
        setupTable()
        
        prepareToolbar(fonts: [GoogleSansFont.bold(with: 21), GoogleSansFont.regular(with: 13)])
    }
    
    func setupToolbar(title: String, subtitle: String, leftViews: [UIView]?, rightViews: [UIView]?) {
        toolbar.title = title
        toolbar.detail = subtitle
        if let leftViews = leftViews {
            toolbar.leftViews = leftViews
        }
        if let rightViews = rightViews {
            toolbar.rightViews = rightViews
        }
    }
    
    func setupTable() {
        mainTableNode.contentInset.top = -40
        mainTableNode.view.prepareBackground()
        mainTableNode.view.keyboardDismissMode = .onDrag
        mainTableNode.delegate = self
        mainTableNode.dataSource = self
        mainTableNode.view.tableFooterView = footerView
        refreshControl.addTarget(self, action: #selector(refreshTable(_:)), for: UIControl.Event.valueChanged)
        topConstraint?.constant = getCommonInset(by: [.toolbar])
    }
    
    func displayData(viewModel: FriendModel.ViewModel.ViewModelData) {
        switch viewModel {
        case .displayFriend(friendViewModel: let friendViewModel):
            self.friendViewModel = friendViewModel
            mainTableNode.reloadData()
            setupToolbar(title: "Подписчики", subtitle: "", leftViews: nil, rightViews: [dismissButton])
            refreshControl.endRefreshing()
            let friendsCount = friendViewModel.cell.count > 0 ? "\(friendViewModel.cell.count) \(getStringByDeclension(number: friendViewModel.cell.count, arrayWords: Localization.instance.friendsCount))" : "Нет друзей"
            footerView.setTitle(friendsCount)
        case .displayFooterLoader:
            footerView.setTitle("")
            footerView.showLoader()
        case .displayFooterError(message: let message):
            toolbar.title = "Ошибка загрузки"
            footerView.setTitle(message)
            mainTableNode.reloadData()
            refreshControl.endRefreshing()
        }
    }
    
    func presentData(response: FriendModel.Response.ResponseType) {
        switch response {
        case .presentFriend(response: let response):
            let cells = response.items?.compactMap { (item) in
                friendCellViewModel(from: item)
            }
            
            let friendViewModel = FriendViewModel.init(cell: cells ?? [], footerTitle: "")
            DispatchQueue.main.async {
                self.displayData(viewModel: .displayFriend(friendViewModel: friendViewModel))
            }
        case .presentFooterLoader:
            DispatchQueue.main.async {
                self.displayData(viewModel: .displayFooterLoader)
            }
        case .presentFooterError(message: let messageError):
            DispatchQueue.main.async {
                self.displayData(viewModel: .displayFooterError(message: messageError))
            }
        }
    }
    
    func makeRequest(request: FriendModel.Request.RequestType) {
        if service == nil {
            service = FriendsService()
        }

        switch request {
        case .getFollowers(let userId):
            presentData(response: .presentFooterLoader)
            service?.getFollowers(userId: userId, completion: { [weak self] (friends) in
                guard let self = self else { return }
                self.presentData(response: .presentFriend(response: friends))
                }, failed: { [weak self] error in
                    guard let self = self else { return }
                    self.presentData(response: .presentFooterError(message: error))
            })
        default:
            break
        }
    }
    
    private func friendCellViewModel(from friend: FriendItem) -> FriendViewModel.Cell {
        let friendCellViewModel = FriendViewModel.Cell.init(id: friend.id, firstName: friend.firstName, lastName: friend.lastName, isClosed: friend.isClosed, isOnline: friend.onlineInfo?.isOnline, isMobile: friend.onlineInfo?.isMobile, photo100: friend.photo100, school: friend.schools?[0].name, homeTown: friend.homeTown)
        return friendCellViewModel
    }
    
    @objc func refreshTable(_ sender: Any?) {
        makeRequest(request: .getFollowers())
    }
    
    func showErrorConnection() {
        mainTableNode.reloadData()
        mainTableNode.toggleNoMessagesNote(on: true, with: "Проблема с подключением", at: "no_connection")
    }
}
extension FullInfoViewController: ASTableDelegate, ASTableDataSource {
    func tableNode(_ tableNode: ASTableNode, numberOfRowsInSection section: Int) -> Int {
        return friendViewModel.cell.count
    }
    
    func numberOfSections(in tableNode: ASTableNode) -> Int {
        return 1
    }
    
    func tableNode(_ tableNode: ASTableNode, nodeBlockForRowAt indexPath: IndexPath) -> ASCellNodeBlock {
        var node: ASCellNode
        node = FollowerCellNode(by: friendViewModel.cell[indexPath.row])
        node.selectionStyle = .none
        return { () -> ASCellNode in
            return node
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableNode(_ tableNode: ASTableNode, constrainedSizeForRowAt indexPath: IndexPath) -> ASSizeRange {
        let screenSize = Screen.bounds.width
        let minSize: CGSize = CGSize(width: screenSize, height: 48)
        let maxSize: CGSize = minSize
        return ASSizeRange(min: minSize, max: maxSize)
    }
}
