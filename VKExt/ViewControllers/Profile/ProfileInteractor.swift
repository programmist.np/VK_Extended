//
//  ProfileInteractor.swift
//  VKExt
//
//  Created programmist_NA on 25.05.2020.
//  Copyright © 2020 ___ORGANIZATIONNAME___. All rights reserved.
//
//  Template generated by Juanpe Catalán @JuanpeCMiOS
//

import UIKit
import SwiftyVK
import SwiftyJSON

class ProfileInteractor: ProfileInteractorProtocol {
    weak var presenter: ProfilePresenterProtocol?
    var service: ProfileService?
    var profileResponse: ProfileResponse?

    func start(request: ProfileModel.Request.RequestType) {
        makeRequest(request: request)
    }
    
    func makeRequest(request: ProfileModel.Request.RequestType) {
        if service == nil {
            service = ProfileService()
        }

        switch request {
        case .getProfileFirstInfo(let userId):
            self.presenter?.presentData(response: .presentFooterLoader)
            service?.getProfileInfo(userId: userId, completion: { [weak self] (profileResponse) in
                guard let self = self else { return }
                self.profileResponse = profileResponse
                self.presenter?.presentData(response: .presentProfileFirstInfo(profile: profileResponse))
            }, failed: { [weak self] (errorMessage) in
                guard let self = self else { return }
                self.presenter?.presentData(response: .presentFooterError(message: errorMessage))
            })
        case .getProfileSecondInfo(let userId):
            guard let profileResponse = self.profileResponse else { return }
            self.presenter?.presentData(response: .presentProfileSecondInfo(profile: profileResponse))
        case .getProfilePhotos(let userId):
            service?.getPhotos(userId: userId, completion: { [weak self] (photos) in
                guard let self = self else { return }
                self.presenter?.presentData(response: .presentProfilePhotos(profile: photos))
            }, failed: { [weak self] (errorMessage) in
                guard let self = self else { return }
                self.presenter?.presentData(response: .presentFooterError(message: errorMessage))
            })
        case .getProfileWall(let userId):
            self.presenter?.presentData(response: .presentFooterLoader)
            service?.getWall(userId: userId, completion: { [weak self] (revealedPostIds, wall) in
                guard let self = self else { return }
                self.presenter?.presentData(response: .presentProfileWall(wall: wall, revealdedPostIds: revealedPostIds))
                }, failed: { [weak self] error in
                    guard let self = self else { return }
                    self.presenter?.presentData(response: .presentFooterError(message: error))
            })
        case .revealPostIds(postId: let postId):
            service?.revealPostIds(forPostId: postId, completion: { [weak self] (revealedPostIds, wall) in
                guard let self = self else { return }
                self.presenter?.presentData(response: .presentProfileWall(wall: wall, revealdedPostIds: revealedPostIds))
            })
        case .getNextBatch(let userId):
            self.presenter?.presentData(response: .presentFooterLoader)
            service?.getNextBatch(userId: userId, completion: { [weak self] (revealedPostIds, wall) in
                guard let self = self else { return }
                self.presenter?.presentData(response: .presentProfileWall(wall: wall, revealdedPostIds: revealedPostIds))
                }, failed: { [weak self] error in
                        guard let self = self else { return }
                        self.presenter?.presentData(response: .presentFooterError(message: error))
                })
        case .like(let postId, let sourceId, let type):
            service?.addLikePost(type: type, ownerId: sourceId, itemId: postId)
        case .unlike(let postId, let sourceId, let type):
            service?.deleteLikePost(type: type, ownerId: sourceId, itemId: postId)
        }
    }
}
