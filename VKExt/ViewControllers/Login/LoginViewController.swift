//
//  LoginViewController.swift
//  VK Tosters
//
//  Created by programmist_np on 12.04.2020.
//  Copyright © 2020 programmist_np. All rights reserved.
//

import UIKit
import Material
import SwiftyVK
import MaterialComponents
import SwiftMessages

class LoginViewController: UIViewController {
    @IBOutlet weak var authSelf: MDCFlatButton!
    let vkReference = VKDelegate.instance
    let devView: PageDevView = PageDevView(frame: .zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLoginButton()
        setupObservers()
        self.isMotionEnabled = true
        self.motionTransitionType = .zoom
        self.navigationController?.isMotionEnabled = true
        self.navigationController?.motionTransitionType = .cover(direction: .left)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func setupObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(setupApp), name: .onLogin, object: nil)
    }

    func setupLoginButton() {
        authSelf.setCorners(radius: 24)
        authSelf.setBackgroundColor(.extendedBlue, for: .normal)
        authSelf.setTitleColor(.white, for: .normal)
        authSelf.setTitleFont(GoogleSansFont.medium(with: 15), for: .normal)
    }
    
    func startLogin() {
        VK.sessions.default.logIn(onSuccess: { sessionInfo in
            print("auth success with \(sessionInfo)")
        }) { error in
            print("auth error with \(error)")
        }
    }
    
    @IBAction func onLoginWithSelf(_ sender: Any) {
        startLogin()
    }
    
    @objc func setupApp(_ notification: Notification) {
        vkReference.start()
        popToRootViewController()
    }
    
    func popToRootViewController() {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "TabBarStartController") as! BottomNavigationViewController
            (self.navigationController as? InitialNavigationController)?.setViewControllers([initialViewController], animated: true)
            (self.navigationController as? InitialNavigationController)?.pushViewController(initialViewController, animated: true)
        }
    }
}
