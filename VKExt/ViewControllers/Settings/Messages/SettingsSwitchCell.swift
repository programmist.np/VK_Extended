//
//  SettingsSwitchCell.swift
//  VKExt
//
//  Created by programmist_NA on 25.06.2020.
//

import UIKit
import Material

class SettingsSwitchCell: UITableViewCell {
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .adaptableBlack
        label.font = GoogleSansFont.medium(with: 16)
        label.numberOfLines = 1
        return label
    }()
    
    let subtitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .adaptableDarkGrayVK
        label.font = GoogleSansFont.medium(with: 13)
        label.numberOfLines = 1
        return label
    }()
    
    let controlSwitch: Switch = {
        let controlSwitch = Switch(state: .off, size: .small)
        controlSwitch.trackOffColor = UIColor.adaptableDarkGrayVK.withAlphaComponent(0.3)
        controlSwitch.trackOnColor = UIColor.extendedBlue.withAlphaComponent(0.3)
        controlSwitch.buttonOffColor = .adaptableDarkGrayVK
        controlSwitch.buttonOnColor = .extendedBlue
        return controlSwitch
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = .adaptableWhite
        
        addSubview(controlSwitch)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        
        titleLabel.autoPinEdge(.leading, to: .leading, of: self, withOffset: 16)
        titleLabel.autoPinEdge(.top, to: .top, of: self, withOffset: 12)
        titleLabel.autoPinEdge(.trailing, to: .trailing, of: controlSwitch, withOffset: -48)
        
        subtitleLabel.autoPinEdge(.leading, to: .leading, of: self, withOffset: 16)
        subtitleLabel.autoPinEdge(.bottom, to: .bottom, of: self, withOffset: -12)
        subtitleLabel.autoPinEdge(.trailing, to: .trailing, of: controlSwitch, withOffset: -48)
        
        controlSwitch.autoPinEdge(.trailing, to: .trailing, of: self, withOffset: -16)
        controlSwitch.autoAlignAxis(toSuperviewAxis: .horizontal)
    }
    
    func setupCell(title: String, subtitle: String = "", stateSwitch: Int = 0) {
        titleLabel.text = title
        subtitleLabel.text = subtitle
        controlSwitch.setSwitchState(state: SwitchState(rawValue: stateSwitch) ?? .off, animated: true, completion: nil)
    }
    
    func setSwitchState(state: SwitchState) {
        controlSwitch.setSwitchState(state: state, animated: true, completion: nil)
    }
}
