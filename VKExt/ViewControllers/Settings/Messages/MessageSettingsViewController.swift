//
//  MessageSettingsViewController.swift
//  VKExt
//
//  Created by programmist_NA on 25.06.2020.
//

import UIKit
import BottomPopup
import PureLayout
import Material

class MessageSettingsViewController: BottomPopupViewController {
    var toolbar: Toolbar = Toolbar(frame: CGRect(origin: .zero, size: CGSize(width: Screen.bounds.width, height: 56)))
    let mainTable = UITableView(frame: .zero, style: .plain)
    var commonSettingsSet: [(String, String, Int)] = []
    var UISettingsSet: [(String, String, Int)] = []
    
    override var popupHeight: CGFloat { return self.getPercentage(from: UIScreen.main.bounds.height.doubleValue, with: 85.0).cgFloatValue }
    
    override var popupTopCornerRadius: CGFloat { return CGFloat(0) }
    
    override var popupPresentDuration: Double { return 0.3 }
    
    override var popupDismissDuration: Double { return 0.3 }
    
    override var popupShouldDismissInteractivelty: Bool { return true }
    
    convenience init(toolbarTitle: String) {
        self.init()
        initialToolbar(title: "    \(toolbarTitle)", subtitle: "", leftViews: nil, rightViews: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.invisible
        appendSettings()
        prepareTable()
        prepareToolbar(fonts: [GoogleSansFont.bold(with: 21), GoogleSansFont.regular(with: 13)])
    }
    
    func prepareTable() {
        self.view.addSubview(mainTable)
        mainTable.autoPinEdge(.top, to: .top, of: self.view, withOffset: GlobalConstants.statusBarHeight + 52)
        mainTable.autoPinEdge(.trailing, to: .trailing, of: self.view)
        mainTable.autoPinEdge(.leading, to: .leading, of: self.view)
        mainTable.autoPinEdge(.bottom, to: .bottom, of: self.view)
        mainTable.separatorStyle = .none
        mainTable.delegate = self
        mainTable.dataSource = self
        mainTable.backgroundColor = .adaptableWhite
        mainTable.register(SettingsSwitchCell.self, forCellReuseIdentifier: "SettingsSwitchCell")
    }
    
    func appendSettings() {
        commonSettingsSet.append(("Нечиталка сообщений", "Собеседник не увидит, что вы прочитали его сообщение", SwitchState.on.rawValue))
        commonSettingsSet.append(("Отключение тайпинга", "Вы будете писать беспалевно)", SwitchState.off.rawValue))
        commonSettingsSet.append(("Скрытие диалогов", "Скройте важные диалолги подальше от посторонних глаз", SwitchState.on.rawValue))
        
        UISettingsSet.append(("Тёмная тема", "Переходи на темную (но не черную) сторону", UserDefaults.standard.integer(forKey: "overrideTheme")))
    }
    
    func prepareToolbar(fonts: [UIFont]) {
        self.view.addSubview(toolbar)
        self.view.backgroundColor = .adaptableWhite
        toolbar.autoPinEdge(toSuperviewSafeArea: .top)
        toolbar.autoPinEdge(toSuperviewSafeArea: .trailing)
        toolbar.autoPinEdge(toSuperviewSafeArea: .leading)
        toolbar.autoSetDimension(.height, toSize: 52)
        toolbar.dividerThickness = 0.4
        toolbar.dividerColor = .adaptableDivider
        toolbar.dividerContentEdgeInsets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        toolbar.backgroundColor = .adaptableWhite
        toolbar.titleLabel.textColor = .adaptableBlack
        toolbar.titleLabel.textAlignment = .left
        toolbar.titleLabel.font = fonts[0]
        toolbar.detailLabel.textColor = .adaptableDarkGrayVK
        toolbar.detailLabel.font = fonts[1]
    }
    
    func initialToolbar(title: String, subtitle: String, leftViews: [UIView]?, rightViews: [UIView]?) {
        toolbar.title = title
        toolbar.detail = subtitle
        if let leftViews = leftViews {
            toolbar.leftViews = leftViews
        }
        if let rightViews = rightViews {
            toolbar.rightViews = rightViews
        }
    }
}
extension MessageSettingsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if commonSettingsSet.count == 0 {
            self.mainTable.toggleNoMessagesNote(on: true, with: "Sorry, settings not implemented :(")
        }
        switch section {
        case 0:
            return commonSettingsSet.count
        case 1:
            return UISettingsSet.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsSwitchCell", for: indexPath) as! SettingsSwitchCell
        cell.controlSwitch.delegate = self
        switch indexPath.section {
        case 0:
            cell.controlSwitch.tag = "\(indexPath.section)\(indexPath.row)".intValue
            cell.setupCell(title: commonSettingsSet[indexPath.row].0, subtitle: commonSettingsSet[indexPath.row].1, stateSwitch: commonSettingsSet[indexPath.row].2)
        case 1:
            cell.controlSwitch.tag = "\(indexPath.section)\(indexPath.row)".intValue
            cell.setupCell(title: UISettingsSet[indexPath.row].0, subtitle: UISettingsSet[indexPath.row].1, stateSwitch: UISettingsSet[indexPath.row].2)
        default:
            return UITableViewCell()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 28))
        returnedView.backgroundColor = .adaptableWhite
        let label = UILabel()
        label.textColor = .adaptableDarkGrayVK
        label.font = GoogleSansFont.medium(with: 18)
        let dividerView = UIView()
        dividerView.backgroundColor = .adaptableGrayVK
        
        returnedView.addSubview(label)
        returnedView.addSubview(dividerView)
        
        dividerView.autoPinEdge(.top, to: .top, of: returnedView, withOffset: 1)
        dividerView.autoPinEdge(.trailing, to: .trailing, of: returnedView, withOffset: -16)
        dividerView.autoPinEdge(.leading, to: .leading, of: returnedView, withOffset: 16)
        dividerView.autoSetDimensions(to: CGSize(width: UIScreen.main.bounds.width - 32, height: 0.1))
        
        label.autoPinEdge(.top, to: .top, of: returnedView, withOffset: 8)
        label.autoPinEdge(.bottom, to: .bottom, of: returnedView, withOffset: -8)
        label.autoPinEdge(.trailing, to: .trailing, of: returnedView, withOffset: -16)
        label.autoPinEdge(.leading, to: .leading, of: returnedView, withOffset: 16)
        label.autoAlignAxis(toSuperviewAxis: .horizontal)
        switch section {
        case 0:
            label.textColor = .adaptableDarkGrayVK
            label.text = "Общие настройки"
        case 1:
            label.textColor = .adaptableRed
            label.text = "Настройка UI (тестовая функция)"
        default:
            label.text = nil
        }
        return returnedView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}
extension MessageSettingsViewController: SwitchDelegate {
    func switchDidChangeState(control: Switch, state: SwitchState) {
        switch control.tag {
        case 10:
            break // changeTheme(with: state)
        default:
            break
        }
    }
    
    func changeTheme(with state: SwitchState) {
        if #available(iOS 13.0, *) {
            DispatchQueue.main.async {
                UIView.transition(with: UIApplication.shared.keyWindow!, duration: 0.5, options: [.showHideTransitionViews], animations: {
                    UIApplication.shared.keyWindow?.overrideUserInterfaceStyle = state == .on ? .dark : .light
                }, completion: { completed in
                    UIApplication.shared.keyWindow?.overrideUserInterfaceStyle = state == .on ? .dark : .light
                    UserDefaults.standard.set(state.rawValue, forKey: "overrideTheme")
                })
            }
        }
    }
}
