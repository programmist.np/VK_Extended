//
//  TabBarStartController.swift
//  VK Tosters
//
//  Created by programmist_np on 30/01/2020.
//  Copyright © 2020 programmist_np. All rights reserved.
//

import UIKit
import SwiftyVK
import SwiftyJSON
import MaterialComponents
import EasyTipView
import RealmSwift
import Material

enum TabBarStyle: Int {
    case smallNoTitle = 0
    case normalWithTitle = 1
}

func clearDatabase() {
    let realm = try! Realm()
    let conversations = realm.objects(Conversation.self)
    try! realm.safeWrite {
        realm.delete(conversations)
    }
}

let blurView: UIVisualEffectView = {
    let view = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
    view.autoresizingMask = .flexibleWidth
    return view
}()

import SwiftMessages

class BottomNavigationViewController: BottomNavigationController {
    static let instance: BottomNavigationViewController = BottomNavigationViewController()
    var tabBarControllerPreviousController: UIViewController? = nil
    var tapCounter: Int = 0
    var previousViewController = UIViewController()
    let substrateView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    static let playerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    let blurPlayerView: UIVisualEffectView = {
        let view = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        return view
    }()
    weak var heightAnchor: NSLayoutConstraint?

    open override func prepare() {
        super.prepare()
        prepareObservers()
        navigationController?.invisible
        isMotionEnabled = true
        motionTabBarTransitionType = .fade
        prepareControllers()
        prepareTabBar()
        delegate = self
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        tabBar.layer.backgroundColor = UIColor.adaptableWhite.withAlphaComponent(0.0).cgColor
        substrateView.layer.backgroundColor = UIColor.adaptableWhite.withAlphaComponent(0.82).cgColor
    }
    
    fileprivate func prepareObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(onBadgeCountChanged(notification:)), name: .onCounterChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(popToRootViewController), name: .onLogout, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onPlayerStart(notification:)), name: NSNotification.Name("playerStart"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onPlayerStop(notification:)), name: NSNotification.Name("playerStop"), object: nil)
    }
    
    fileprivate func prepareControllers() {
        viewControllers = [
            NavigationController(rootViewController: SampleTabsController(viewControllers: [NewsfeedViewController(), SuggestionsViewController()])),
            NavigationController(rootViewController: RecomendationsViewController()),
            NavigationController(rootViewController: ConversationsNodeViewController()),
            NavigationController(rootViewController: FriendsViewController()),
            NavigationController(rootViewController: ProfileViewController())
        ]
        tabBar.items?[0].image = UIImage(named: "news")
        tabBar.items?[1].image = UIImage(named: "services_outline_28")
        tabBar.items?[2].image = UIImage(named: "message_outline_28")
        tabBar.items?[3].image = UIImage(named: "users_outline_28")
        tabBar.items?[4].image = UIImage(named: "smile_outline_28")
        
        switch style {
        case .smallNoTitle:
            tabBar.items?[0].title = ""
            tabBar.items?[1].title = ""
            tabBar.items?[2].title = ""
            tabBar.items?[3].title = ""
            tabBar.items?[4].title = ""
            
            tabBar.heightPreset = .custom(49)
        case .normalWithTitle:
            tabBar.items?[0].title = "Новости"
            tabBar.items?[1].title = "Сервисы"
            tabBar.items?[2].title = "Сообщения"
            tabBar.items?[3].title = "Друзья"
            tabBar.items?[4].title = "Профиль"
            
            tabBar.heightPreset = .custom(52)
        }
    }
    
    fileprivate func prepareTabBar() {
        isSwipeEnabled = false
        tabBar.depthPreset = .none
        tabBar.dividerColor = .adaptableDivider
        tabBar.dividerThickness = 0.5
        tabBar.isTranslucent = true
        tabBar.backgroundImage = UIImage()
        tabBar.shadowImage = UIImage()
        tabBar.barTintColor = .clear
        tabBar.backgroundColor = UIColor.adaptableWhite.withAlphaComponent(0.0)
        tabBar.layer.backgroundColor = UIColor.adaptableWhite.withAlphaComponent(0.0).cgColor
        tabBar.clipsToBounds = false
        
        view.insertSubview(substrateView, belowSubview: tabBar)
        
        substrateView.backgroundColor = UIColor.adaptableWhite.withAlphaComponent(0.82)
        substrateView.layer.backgroundColor = UIColor.adaptableWhite.withAlphaComponent(0.82).cgColor
        
        substrateView.autoPinEdge(toSuperviewSafeArea: .bottom)
        substrateView.autoPinEdge(.trailing, to: .trailing, of: self.view)
        substrateView.autoPinEdge(.leading, to: .leading, of: self.view)
        heightAnchor = substrateView.autoSetDimension(.height, toSize: style == .smallNoTitle ? 49 : 52)
        substrateView.isUserInteractionEnabled = false
        
        blurView.frame = CGRect(origin: tabBar.bounds.origin, size: CGSize(width: screenWidth, height: style == .smallNoTitle ? 91 : 94))
        substrateView.addSubview(blurView)
        substrateView.addSubview(BottomNavigationViewController.playerView)
        
        BottomNavigationViewController.playerView.autoPinEdge(.top, to: .top, of: substrateView)
        BottomNavigationViewController.playerView.autoPinEdge(.trailing, to: .trailing, of: substrateView)
        BottomNavigationViewController.playerView.autoPinEdge(.leading, to: .leading, of: substrateView)
        BottomNavigationViewController.playerView.autoSetDimension(.height, toSize: 42)
    }
    
    fileprivate func setBadgeLevel(at indexes: [Int], values: [Int]) {
        DispatchQueue.main.async {
            for index in indexes {
                let value = values[index] != 0 ? "\(values[index])" : nil
                if index == 2 {
                    self.tabBar.items?[index].badgeValue = value
                    self.tabBar.items?[index].badgeColor = .adaptableRed
                } else {
                    if value != nil && value != "0" {
                        self.createRedDot(at: index)
                    }
                }
            }
        }
    }
    
    @objc func onBadgeCountChanged(notification: Notification) {
        guard let counters = notification.userInfo?["counters"] as? [Int] else { return }
        setBadgeLevel(at: [0, 1, 2, 3, 4], values: counters)
    }
    
    @objc func popToRootViewController(_ notification: Notification) {
        DispatchQueue.main.async {
            let initialViewController = LoginViewController()
            self.navigationController?.setViewControllers([initialViewController], animated: true)
            initialViewController.isMotionEnabled = true
            initialViewController.motionTransitionType = .zoomOut
            self.navigationController?.popToViewController(initialViewController, animated: true)
        }
    }
}
extension BottomNavigationViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if tabBarControllerPreviousController == viewController {
            if viewController.responds(to: #selector(UIViewController.scrollToTop)) {
                viewController.scrollToTop()
            }
        }
        tabBarControllerPreviousController = viewController
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        tapCounter += 1
        var hasTappedTwiceOnOneTab = false
        if previousViewController == viewController {
            hasTappedTwiceOnOneTab = true
        }
        previousViewController = viewController
        if tapCounter == 2 && hasTappedTwiceOnOneTab {
            tapCounter = 0
            switch selectedIndex {
            case 4:
                clearDatabase()
                VK.sessions.default.logOut()
                NotificationCenter.default.post(name: .onLogout, object: nil)
            default:
                break
            }
            return false
        } else if tapCounter == 1 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.tapCounter = 0
            }
            return true
        }
        return true
    }
}
extension BottomNavigationViewController {
    @objc func onPlayerStart(notification: Notification) {
        heightAnchor?.constant = 91
        UIView.animate(withDuration: 0.4) {
            self.substrateView.layoutIfNeeded()
        }
        substrateView.isUserInteractionEnabled = true
    }
    
    @objc func onPlayerStop(notification: Notification) {
        heightAnchor?.constant = 49
        UIView.animate(withDuration: 0.2) {
            self.substrateView.layoutIfNeeded()
        }
        substrateView.isUserInteractionEnabled = false
    }
}
extension BottomNavigationController {
    var tabBarStyle: TabBarStyle {
        return TabBarStyle(rawValue: UserDefaults.standard.integer(forKey: "tabBarStyle")) ?? .smallNoTitle
    }
}
extension BottomNavigationViewController {
    var style: TabBarStyle {
        get {
            return TabBarStyle(rawValue: UserDefaults.standard.integer(forKey: "tabBarStyle")) ?? .smallNoTitle
        } set {
            setTabBarStyle(style: newValue)
        }
    }
    
    func setTabBarStyle(style: TabBarStyle) {
        switch style {
        case .smallNoTitle:
            self.heightAnchor?.constant = 49
            self.tabBar.items?[0].title = ""
            self.tabBar.items?[1].title = ""
            self.tabBar.items?[2].title = ""
            self.tabBar.items?[3].title = ""
            self.tabBar.items?[4].title = ""
            self.tabBar.heightPreset = .custom(49)

            UIView.animate(withDuration: 0.3, delay: 0, options: [.preferredFramesPerSecond60, .transitionFlipFromTop], animations: {
                self.tabBar.setNeedsLayout()
                self.tabBar.layoutIfNeeded()
                self.substrateView.setNeedsLayout()
                self.substrateView.layoutIfNeeded()
            })
            
        case .normalWithTitle:
            self.heightAnchor?.constant = 52
            self.tabBar.items?[0].title = "Новости"
            self.tabBar.items?[1].title = "Сервисы"
            self.tabBar.items?[2].title = "Сообщения"
            self.tabBar.items?[3].title = "Друзья"
            self.tabBar.items?[4].title = "Профиль"
            self.tabBar.heightPreset = .custom(52)

            UIView.animate(withDuration: 0.3, delay: 0, options: [.preferredFramesPerSecond60, .transitionFlipFromBottom], animations: {
                self.tabBar.setNeedsLayout()
                self.tabBar.layoutIfNeeded()
                self.substrateView.setNeedsLayout()
                self.substrateView.layoutIfNeeded()
            })
        }
    }
}
extension BottomNavigationController {
    func createRedDot(at index: Int) {
        for subview in tabBar.subviews {
            if subview.tag == -11225841 + index {
                subview.removeFromSuperview()
                break
            }
        }

        let dotRaduis: CGFloat = 3
        let dotDiameter = dotRaduis * 2

        let topMargin: CGFloat = tabBarStyle == .smallNoTitle ? 8 : 4

        let itemsCount = CGFloat(tabBar.items!.count)

        let halfItemWidth = screenWidth / (itemsCount * 2) // screenWidth / 10

        let xOffset = halfItemWidth * CGFloat(index * 2 + 1)

        let imageHalfWidth: CGFloat = (tabBar.items![index]).selectedImage!.size.width / 2

        let redDot = UIView(frame: CGRect(x: xOffset + imageHalfWidth - 3, y: topMargin, width: dotDiameter, height: dotDiameter))

        redDot.tag = -11225841 + index
        redDot.backgroundColor = .adaptableRed
        redDot.layer.cornerRadius = dotRaduis

        tabBar.addSubview(redDot)
    }
}
