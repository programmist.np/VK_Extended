//
//  MessagesApi.swift
//  VKExt
//
//  Created by programmist_NA on 24.06.2020.
//

import Foundation
import SwiftyVK
import SwiftyJSON

// Класс API запросов
class Api: NSObject {
    // MARK: Секция "Аккаунт"
    struct Account {
        // account.setSilenceMode
        static func setSilenceMode(peerId: Int, sound: Int, success: @escaping(() -> ()), error: @escaping(_ error: VKError) -> Void) {
            VK.API.Account.setSilenceMode([.peerId: peerId.stringValue, .deviceId: UIDevice.current.identifierForVendor?.uuidString])
                .configure(with: Config.init(httpMethod: .GET, language: .default))
                .onSuccess { response in
                    print(" - account.setSilenceMode ✅")
                    success()
            }
            .onError { vkError in
                print(" - account.setSilenceMode ❌")
                error(vkError)
            }
            .send()
        }
    }
    
    // MARK: Секция "Аккаунт"
    struct Audio {
        // account.setSilenceMode
        static func get(token: String, secret: String, ownerId: Int, response: @escaping (AudioObject?) -> Void, failed: @escaping (String) -> Void) {
            CommonService.instance.performRequest(method: "audio.get", postFields: ["owner_id" : "\(ownerId)", "access_token" : token, "v" : "5.90", "language" : "ru"], secret: secret, success: { (anyData) in
                let decoded = decodeJSON(type: AudioObject.self, from: anyData as! Data)
                response(decoded)
            }) { (error) in
                failed(error)
                response(nil)
            }
        }
    }
    
    // MARK: Секция "Сообщения"
    struct Messages {
        // messages.getConversations
        static func getConversations(filter: String = "all", count: Int = 200, isExtended: Bool = true, success: @escaping(_ response: JSON) -> Void, error: @escaping(_ error: VKError) -> Void) {
            VK.API.Messages.getConversations([.filter: filter, .count: count.stringValue, .extended: isExtended.intValue.stringValue, .fields: VKConstants.userFields])
                .configure(with: Config.init(httpMethod: .GET, language: .default))
            .onSuccess { response in
                print(" - messages.getConversations ✅")
                success(JSON(response))
            }
            .onError { vkError in
                print(" - messages.getConversations ❌")
                error(vkError)
            }
            .send()
        }
        
        // messages.getConversationsById
        static func getConversationById(peerId: Int, isExtended: Bool = true, success: @escaping(_ response: (JSON, JSON)) -> Void, error: @escaping(_ error: VKError) -> Void) {
            VK.API.Custom.method(name: "messages.getConversationsById", parameters: ["peer_ids": peerId.stringValue, "extended": isExtended.intValue.stringValue, "fields": VKConstants.userFields])
                .configure(with: Config.init(httpMethod: .GET, language: .default))
            .onSuccess { response in
                print(" - messages.getConversationsById ✅")
                success((JSON(response)["items"].arrayValue.first!, JSON(response)["profiles"].arrayValue.first!))
            }
            .onError { vkError in
                print(" - messages.getConversationsById ❌")
                error(vkError)
            }
            .send()
        }
        
        // messages.markAsRead
        static func markAsRead(peerId: Int, success: @escaping() -> Void, error: @escaping(_ error: VKError) -> Void) {
            VK.API.Messages.markAsRead([.peerId: "\(peerId)"])
                .configure(with: Config(httpMethod: .POST, language: .default))
                .onSuccess { _ in
                    print(" - messages.markAsRead ✅")
                    success()
                }
            .onError { vkError in
                print(" - messages.markAsRead ❌")
                error(vkError)
            }.send()
        }
        
        // messages.markAsUnreadConversation
        static func markAsUnread(peerId: Int, success: @escaping() -> Void, error: @escaping(_ error: VKError) -> Void) {
            VK.API.Custom.method(name: "messages.markAsUnreadConversation", parameters: ["peer_id": peerId.stringValue])
                .configure(with: Config(httpMethod: .POST, language: .default))
                .onSuccess { _ in
                    print(" - messages.markAsUnreadConversation ✅")
                    success()
                }
            .onError { vkError in
                print(" - messages.markAsUnreadConversation ❌")
                error(vkError)
            }.send()
        }
        
        // messages.deleteConversation
        static func deleteConversation(peerId: Int, success: @escaping(() -> ()), error: @escaping(_ error: VKError) -> Void) {
            VK.API.Messages.deleteConversation([.peerId: peerId.stringValue])
                .configure(with: Config(httpMethod: .POST, language: .default))
                .onSuccess { _ in
                    print(" - messages.deleteConversation ✅")
                    success()
                }
            .onError { vkError in
                print(" - messages.deleteConversation ❌")
                error(vkError)
            }.send()
        }
        
        // messages.searchConversations
        static func searchConversations(q: String, count: Int = 200, isExtended: Bool = true, success: @escaping(_ response: JSON) -> Void, error: @escaping(_ error: VKError) -> Void) {
            VK.API.Messages.searchConversations([.q: q, .count: count.stringValue, .extended: isExtended.intValue.stringValue, .fields: VKConstants.userFields])
            .configure(with: Config.init(httpMethod: .GET, language: .default))
            .onSuccess { response in
                print(" - messages.searchConversations ✅")
                success(JSON(response))
            }
            .onError { vkError in
                print(" - messages.searchConversations ❌")
                error(vkError)
            }
            .send()
        }
    }
    
    // MARK: Секция "Друзья"
    struct Friends {
        // friends.get
        static func get(userId: Int = VKConstants.currentUserId, response: @escaping (FriendResponse?) -> Void, failed: @escaping (String) -> Void) {
            VK.API.Friends.get([.userId: "\(userId)", .order: "hints", .fields: VKConstants.userFields])
            .configure(with: Config.init(httpMethod: .GET, language: .default))
                .onSuccess { data in
                    print(" - friends.get ✅")
                    if let decoded = decodeJSON(type: FriendResponse.self, from: data) {
                        response(decoded)
                    } else {
                        failed(ResponseError.getError(at: 10000).rawValue)
                        response(nil)
                    }
            }.onError { vkError in
                print(" - friends.get ❌")
                failed(ResponseError.getVKError(at: vkError))
                response(nil)
            }.send()
        }
    }
    
    // MARK: Секция "Лайки"
    struct Likes {
        static func isLiked(type: String, ownerId: Int, itemId: Int) -> Void {
            VK.API.Likes.isLiked([.type: type, .ownerId: ownerId.stringValue, .itemId: itemId.stringValue])
                .configure(with: Config.init(httpMethod: .GET, language: .default))
                .onSuccess { data in
                    print(" - likes.isLiked ✅")
            }.onError { vkError in
                print(" - likes.isLiked ❌")
            }.send()
        }

        static func add(type: String, ownerId: Int, itemId: Int, success: @escaping () -> ()) {
            VK.API.Likes.add([.type: type, .ownerId: ownerId.stringValue, .itemId: itemId.stringValue])
                .configure(with: Config.init(httpMethod: .GET, language: .default))
                .onSuccess { data in
                    print(" - likes.add ✅")
                    success()
            }.onError { vkError in
                print(" - likes.add ❌")
            }.send()
        }
        
        static func delete(type: String, ownerId: Int, itemId: Int, success: @escaping () -> ()) {
            VK.API.Likes.delete([.type: type, .ownerId: ownerId.stringValue, .itemId: itemId.stringValue])
                .configure(with: Config.init(httpMethod: .GET, language: .default))
                .onSuccess { data in
                    print(" - likes.delete ✅")
                    success()
            }.onError { vkError in
                print(" - likes.delete ❌")
            }.send()
        }
    }
    
    // MARK: Секция "Новости"
    struct NewsFeed {
        static func get(nextBatchFrom: String?, response: @escaping (FeedResponse?) -> Void, failed: @escaping (String) -> Void) {
            VK.API.NewsFeed.get([.filters: "post, photo", .count: 50.stringValue, .startFrom: nextBatchFrom])
            .configure(with: Config.init(httpMethod: .GET, language: .default))
                .onSuccess { data in
                    print(" - newsfeed.get ✅")
                    let decoded = decodeJSON(type: FeedResponse.self, from: data)
                    response(decoded)
            }.onError { vkError in
                failed(ResponseError.getVKError(at: vkError))
                print(" - newsfeed.get ❌", "(\(ResponseError.getVKError(at: vkError)))")
                response(nil)
            }.send()
        }
        
        static func getRecommended(nextBatchFrom: String?, response: @escaping (FeedResponse?) -> Void, failed: @escaping (String) -> Void) {
            VK.API.NewsFeed.getRecommended([.count: 50.stringValue, .startFrom: nextBatchFrom])
            .configure(with: Config.init(httpMethod: .GET, language: .default))
                .onSuccess { data in
                    print(" - newsfeed.getRecommended ✅")
                    let decoded = decodeJSON(type: FeedResponse.self, from: data)
                    response(decoded)
            }.onError { vkError in
                failed(ResponseError.getVKError(at: vkError))
                print(" - newsfeed.getRecommended ❌", "(\(ResponseError.getVKError(at: vkError)))")
                response(nil)
            }.send()
        }
    }
    
    // MARK: Секция "Фотографии"
    struct Photos {
        static func get(ownerId: Int = VKConstants.currentUserId, skipHidden: String = "profile", response: @escaping (PhotoResponse?) -> Void, failed: @escaping (String) -> Void) {
            VK.API.Photos.getAll([.ownerId: ownerId.stringValue, .extended: true.intValue.stringValue])
            .configure(with: Config.init(httpMethod: .GET, language: .default))
                .onSuccess { data in
                    print(" - photos.get ✅")
                    if let decoded = decodeJSON(type: PhotoResponse.self, from: data) {
                        response(decoded)
                    } else {
                        failed(ResponseError.getError(at: 10000).rawValue)
                        response(nil)
                    }
            }.onError { vkError in
                print(" - photos.get ❌", "(\(ResponseError.getVKError(at: vkError)))")
                failed(ResponseError.getVKError(at: vkError))
                response(nil)
            }.send()
        }
    }
    
    // MARK: Секция "Истории"
    struct Stories {
        static func get(response: @escaping (HistoryResponse?) -> Void) {
            VK.API.Custom.method(name: "stories.get", parameters: ["extended" : "1", "fields" : VKConstants.userFields])
            .configure(with: Config.init(httpMethod: .GET, apiVersion: "5.120", language: .default))
                .onSuccess { data in
                    print(" - stories.get ✅")
                    print(JSON(data))
                    let decoded = decodeJSON(type: HistoryResponse.self, from: data)
                    response(decoded)
            }.onError { vkError in
                print(" - stories.get ❌", "(\(ResponseError.getVKError(at: vkError)))")
                switch vkError {
                case .api(let apiError):
                    response(nil)
                default:
                    break
                }
            }.send()
        }
    }
    
    // MARK: Секция "Пользователь"
    struct Users {
        static func get(userId: Int = VKConstants.currentUserId, response: @escaping (ProfileResponse?) -> Void, failed: @escaping (String) -> Void) {
            VK.API.Users.get([.userId: userId.stringValue, .fields: VKConstants.userFields])
            .configure(with: Config.init(httpMethod: .GET, language: .default))
                .onSuccess { data in
                    print(" - users.get ✅")
                    let json = JSON(data)
                    if let encryptedData: Data = try? json.arrayValue.first!.rawData() {
                        print(JSON(encryptedData))
                        if let decoded = decodeJSON(type: ProfileResponse.self, from: encryptedData) {
                            response(decoded)
                        } else {
                            failed(ResponseError.getError(at: 10000).rawValue)
                            response(nil)
                        }
                    }
            }.onError { vkError in
                print(" - users.get ❌", "(\(ResponseError.getVKError(at: vkError)))")
                failed(ResponseError.getVKError(at: vkError))
                response(nil)
            }.send()
        }
        
        static func getFollowers(userId: Int = VKConstants.currentUserId, offset: Int = 0, response: @escaping (FriendResponse?) -> Void, failed: @escaping (String) -> Void) {
            VK.API.Users.getFollowers([.userId: userId.stringValue, .count: 50.stringValue, .offset: offset.stringValue, .fields: VKConstants.userFields])
            .configure(with: Config.init(httpMethod: .GET, language: .default))
                .onSuccess { data in
                    print(" - users.getFollowers ✅")
                    if let decoded = decodeJSON(type: FriendResponse.self, from: data) {
                        response(decoded)
                    } else {
                        failed(ResponseError.getError(at: 10000).rawValue)
                        response(nil)
                    }
            }.onError { vkError in
                print(" - users.getFollowers ❌", "(\(ResponseError.getVKError(at: vkError)))")
                failed(ResponseError.getVKError(at: vkError))
                response(nil)
            }.send()
        }
    }
    
    // MARK: Секция "Стена"
    struct Wall {
        static func get(ownerId: Int = VKConstants.currentUserId, offset: Int = 0, response: @escaping (WallResponse?) -> Void, failed: @escaping (String) -> Void) {
            VK.API.Wall.get([.ownerId: ownerId.stringValue, .offset: offset.stringValue, .filter: "all", .extended: true.intValue.stringValue])
            .configure(with: Config.init(httpMethod: .GET, language: .default))
                .onSuccess { data in
                    print(" - wall.get ✅")
                    let decoded = decodeJSON(type: WallResponse.self, from: data)
                    response(decoded)
            }.onError { vkError in
                print(" - wall.get ❌")
                response(nil)
            }.send()
        }

        static func getById(posts: String, isExtended: Bool = true, response: @escaping (FeedItem?) -> Void) {
            VK.API.Wall.getById([.posts: posts, .extended: isExtended.intValue.stringValue])
            .configure(with: Config.init(httpMethod: .GET, language: .default))
                .onSuccess { data in
                    print(" - wall.getById ✅")
                    let decoded = decodeJSON(type: FeedItem.self, from: data)
                    response(decoded)
            }.onError { vkError in
                print(" - wall.getById ❌")
                response(nil)
            }.send()
        }
    }
}
func decodeJSON<T: Decodable>(type: T.Type, from: Data?) -> T? {
    let decoder = JSONDecoder()
    decoder.keyDecodingStrategy = .convertFromSnakeCase
    guard let data = from, let response = try? decoder.decode(type.self, from: data) else { return nil }
    return response
}
