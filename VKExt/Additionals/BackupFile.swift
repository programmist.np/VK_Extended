////
////  NewsFeedCell.swift
////  VKExt
////
////  Created by programmist_NA on 11.07.2020.
////
//
//import Foundation
//import UIKit
//import Material
//
//protocol NewsFeedCellDelegate: class {
//    func revealPost(for cell: NewsFeedCell)
//    func likePost(for cell: NewsFeedCell)
//    func unlikePost(for cell: NewsFeedCell)
//    func openPhoto(for cell: NewsFeedCell, with url: String?)
//}
//
//final class NewsFeedCell: UITableViewCell {
//    
//    static let reuseId = "NewsFeedCell"
//    
//    weak var delegate: NewsFeedCellDelegate?
//    
//    var isLiked: Bool = false
//
//    // Первый слой
//    
//    let cardView: UIView = {
//       let view = UIView()
//        view.backgroundColor = .adaptableWhite
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    
//    // Второй слой
//    
//    let topView: UIView = {
//       let view = UIView()
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    
//    let topRepostView: UIView = {
//       let view = UIView()
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    
//    let postlabel: UITextView = {
//       let textView = UITextView()
//        textView.backgroundColor = .adaptableWhite
//        textView.font = Constants.postLabelFont
//        textView.isScrollEnabled = false
//        textView.isSelectable = true
//        textView.isUserInteractionEnabled = true
//        textView.isEditable = false
//        
//        let padding = textView.textContainer.lineFragmentPadding
//        textView.textContainerInset = UIEdgeInsets.init(top: 0, left: -padding, bottom: 0, right: -padding)
//        
//        textView.dataDetectorTypes = UIDataDetectorTypes.all
//        return textView
//    }()
//    
//    let moreTextButton: UIButton = {
//       let button = UIButton()
//        button.titleLabel?.font = GoogleSansFont.medium(with: 14)
//        button.setTitleColor(.extendedBlue, for: .normal)
//        button.contentHorizontalAlignment = .left
//        button.contentVerticalAlignment = .center
//        button.setTitle("Показать полностью...", for: .normal)
//        return button
//    }()
//    
//    let galleryCollectionView = GalleryCollectionView()
//    
//    let postImageView: WebImageView = {
//        let imageView = WebImageView()
//        imageView.backgroundColor = .adaptableBackground
//        imageView.isUserInteractionEnabled = true
//        let tapRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onOpenPhoto))
//        tapRecognizer.numberOfTapsRequired = 1
//        imageView.addGestureRecognizer(tapRecognizer)
//        return imageView
//    }()
//    var postUrl: String? = ""
//    
//    let postButton: IconButton = {
//        let imageView = IconButton()
//        imageView.backgroundColor = .clear
//        return imageView
//    }()
//    
//    let bottomView: UIView = {
//       let view = UIView()
//        return view
//    }()
//    
//    // Третий слой на topView
//    
//    let iconImageView: WebImageView = {
//        let imageView = WebImageView()
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//        imageView.backgroundColor = .adaptableBackground
//        return imageView
//    }()
//    
//    let nameLabel: UILabel = {
//       let label = UILabel()
//        label.translatesAutoresizingMaskIntoConstraints = false
//        label.font = GoogleSansFont.medium(with: 16)
//        label.numberOfLines = 0
//        label.textColor = .adaptableBlack
//        return label
//    }()
//    
//    let dateLabel: UILabel = {
//       let label = UILabel()
//        label.translatesAutoresizingMaskIntoConstraints = false
//        label.textColor = .adaptableDarkGrayVK
//        label.font = GoogleSansFont.medium(with: 13)
//        return label
//    }()
//    
//    // Четвертый слой на topView
//    
//    let iconRepostImageView: WebImageView = {
//        let imageView = WebImageView()
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//        imageView.backgroundColor = .adaptableBackground
//        return imageView
//    }()
//    
//    let nameRepostLabel: UILabel = {
//       let label = UILabel()
//        label.translatesAutoresizingMaskIntoConstraints = false
//        label.font = GoogleSansFont.medium(with: 15)
//        label.numberOfLines = 0
//        label.textColor = .adaptableBlack
//        return label
//    }()
//    
//    let dateRepostLabel: UILabel = {
//       let label = UILabel()
//        label.translatesAutoresizingMaskIntoConstraints = false
//        label.textColor = .adaptableDarkGrayVK
//        label.font = GoogleSansFont.medium(with: 12)
//        return label
//    }()
//    
//    // Третий слой на bottomView
//
//    let likesView: UIView = {
//        let view = UIView()
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    
//    let commentsView: UIView = {
//        let view = UIView()
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    
//    let sharesView: UIView = {
//        let view = UIView()
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    
//    let viewsView: UIView = {
//        let view = UIView()
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
//    
//    // Четвертый слой на bottomView
//    
//    let likesImage: UIImageView = {
//        let imageView = UIImageView()
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//        imageView.image = UIImage(named: "like_outline_24")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableDarkGrayVK)
//        return imageView
//    }()
//    
//    let likesButton: IconButton = {
//        let button = IconButton()
//        button.autoSetDimensions(to: CGSize(width: 80, height: 44))
//        return button
//    }()
//    
//    let commentsImage: UIImageView = {
//        let imageView = UIImageView()
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//        imageView.image = UIImage(named: "comment_outline_24")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableDarkGrayVK)
//        return imageView
//    }()
//    
//    let sharesImage: UIImageView = {
//        let imageView = UIImageView()
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//        imageView.image = UIImage(named: "share_outline_24")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableDarkGrayVK)
//        return imageView
//    }()
//    
//    let viewsImage: UIImageView = {
//        let imageView = UIImageView()
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//        imageView.image = UIImage(named: "view_outline_24")?.withRenderingMode(.alwaysTemplate).tint(with: .adaptableDarkGrayVK)
//        return imageView
//    }()
//    
//    let likesLabel: UILabel = {
//        let label = UILabel()
//        label.translatesAutoresizingMaskIntoConstraints = false
//        label.font = GoogleSansFont.medium(with: 14)
//        label.lineBreakMode = .byClipping
//        return label
//    }()
//    
//    let commentsLabel: UILabel = {
//        let label = UILabel()
//        label.translatesAutoresizingMaskIntoConstraints = false
//        label.textColor = .adaptableDarkGrayVK
//        label.font = GoogleSansFont.medium(with: 14)
//        label.lineBreakMode = .byClipping
//        return label
//    }()
//    
//    let sharesLabel: UILabel = {
//        let label = UILabel()
//        label.translatesAutoresizingMaskIntoConstraints = false
//        label.textColor = .adaptableDarkGrayVK
//        label.font = GoogleSansFont.medium(with: 14)
//        label.lineBreakMode = .byClipping
//        return label
//    }()
//    
//    let viewsLabel: UILabel = {
//        let label = UILabel()
//        label.translatesAutoresizingMaskIntoConstraints = false
//        label.textColor = .adaptableDarkGrayVK
//        label.font = GoogleSansFont.medium(with: 14)
//        label.lineBreakMode = .byClipping
//        return label
//    }()
//    
//    override func prepareForReuse() {
//        iconImageView.set(imageURL: nil)
//        iconRepostImageView.set(imageURL: nil)
//        postImageView.set(imageURL: nil)
//    }
//    
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//        
//        iconImageView.layer.cornerRadius = Constants.topViewHeight / 2
//        iconImageView.clipsToBounds = true
//        
//        iconRepostImageView.layer.cornerRadius = Constants.topRepostViewHeight / 2
//        iconRepostImageView.clipsToBounds = true
//        
//        backgroundColor = .clear
//        selectionStyle = .none
//        
//        cardView.layer.cornerRadius = 0
//        cardView.clipsToBounds = true
//        
//        moreTextButton.addTarget(self, action: #selector(moreTextButtonTouch), for: .touchUpInside)
//        postButton.addTarget(self, action: #selector(onOpenPhoto), for: .touchUpInside)
//        
//        overlayFirstLayer() // первый слой
//        overlaySecondLayer() // второй слой
//        overlaySecondRepostLayer()
//        overlayLayerOnRepostView()
//        overlayThirdLayerOnTopView() // третий слой на topView
//        overlayThirdLayerOnBottomView() // третий слой на bottomView
//        overlayFourthLayerOnBottomViewViews() // четвертый слой на bottomViewViews
//    }
//    
//    @objc func onOpenPhoto() {
//        delegate?.openPhoto(for: self, with: postUrl)
//    }
//    
//    @objc func moreTextButtonTouch() {
//        delegate?.revealPost(for: self)
//    }
//    
//    @objc func onLikePost(_ sender: IconButton) {
//        !isLiked ? delegate?.likePost(for: self) : delegate?.unlikePost(for: self)
//    }
//    
//    @objc func onUnLikePost(_ sender: IconButton) {
//        delegate?.unlikePost(for: self)
//    }
//    
//    func set(viewModel: FeedCellViewModel) {
//        isLiked = viewModel.userLikes == 1
//        iconRepostImageView.isHidden = viewModel.repost?.first == nil
//        nameRepostLabel.isHidden = viewModel.repost?.first == nil
//        dateRepostLabel.isHidden = viewModel.repost?.first == nil
//
//        iconImageView.set(imageURL: viewModel.iconUrlString)
//        nameLabel.text = viewModel.name
//        dateLabel.text = viewModel.date
//        postlabel.text = viewModel.text
//        likesLabel.text = viewModel.likes
//        commentsLabel.text = viewModel.comments
//        sharesLabel.text = viewModel.shares
//        viewsLabel.text = viewModel.views
//        
//        postlabel.frame = viewModel.sizes.postLabelFrame
//        
//        bottomView.frame = viewModel.sizes.bottomViewFrame
//        moreTextButton.frame = viewModel.sizes.moreTextButtonFrame
//        
//        likesLabel.textColor = viewModel.userLikes == 1 ? .adaptableRed : .adaptableDarkGrayVK
//        likesImage.image = UIImage(named: viewModel.userLikes == 1 ? "like_24" : "like_outline_24")?.withRenderingMode(.alwaysTemplate).tint(with: viewModel.userLikes == 1 ? .adaptableRed : .adaptableDarkGrayVK)
//        
//        likesButton.addTarget(self, action: #selector(onLikePost), for: .touchUpInside)
//
//        if let repost = viewModel.repost?.first {
//            iconRepostImageView.set(imageURL: repost.iconUrlString)
//            nameRepostLabel.attributedText = setLabelImage(image: "repost_12")! + NSAttributedString(string: " \(repost.name)", attributes: [.font: GoogleSansFont.medium(with: 15), .foregroundColor: UIColor.adaptableBlack])
//            dateRepostLabel.text = repost.date
//            postlabel.text = repost.text
//            
//            if let photoAttachment = repost.photoAttachements.first, repost.photoAttachements.count == 1 {
//                postImageView.isHidden = false
//                postButton.isHidden = false
//                galleryCollectionView.isHidden = true
//                postImageView.set(imageURL: photoAttachment.photoUrlString)
//                postUrl = photoAttachment.photoMaxUrl
//                postImageView.frame = viewModel.sizes.attachmentFrame
//                postButton.frame = viewModel.sizes.attachmentFrame
//            } else if repost.photoAttachements.count > 1 {
//                postImageView.isHidden = true
//                postButton.isHidden = true
//                galleryCollectionView.isHidden = false
//                galleryCollectionView.frame = viewModel.sizes.attachmentFrame
//                galleryCollectionView.set(photos: repost.photoAttachements)
//            }
//            else {
//                postImageView.isHidden = true
//                postButton.isHidden = true
//                galleryCollectionView.isHidden = true
//            }
//        } else {
//            if let photoAttachment = viewModel.photoAttachements.first, viewModel.photoAttachements.count == 1 {
//                postImageView.isHidden = false
//                postButton.isHidden = false
//                galleryCollectionView.isHidden = true
//                postImageView.set(imageURL: photoAttachment.photoUrlString)
//                postUrl = photoAttachment.photoMaxUrl
//                postImageView.frame = viewModel.sizes.attachmentFrame
//                postButton.frame = viewModel.sizes.attachmentFrame
//            } else if viewModel.photoAttachements.count > 1 {
//                postImageView.isHidden = true
//                postButton.isHidden = true
//                galleryCollectionView.isHidden = false
//                galleryCollectionView.frame = viewModel.sizes.attachmentFrame
//                galleryCollectionView.set(photos: viewModel.photoAttachements)
//            }
//            else {
//                postImageView.isHidden = true
//                postButton.isHidden = true
//                galleryCollectionView.isHidden = true
//            }
//        }
//    }
//    
//    private func overlayFourthLayerOnBottomViewViews() {
//        likesView.addSubview(likesImage)
//        likesView.addSubview(likesLabel)
//        likesView.addSubview(likesButton)
//        
//        commentsView.addSubview(commentsImage)
//        commentsView.addSubview(commentsLabel)
//        
//        sharesView.addSubview(sharesImage)
//        sharesView.addSubview(sharesLabel)
//        
//        viewsView.addSubview(viewsImage)
//        viewsView.addSubview(viewsLabel)
//        
//        helpInFourthLayer(view: likesView, imageView: likesImage, label: likesLabel)
//        helpInFourthLayer(view: commentsView, imageView: commentsImage, label: commentsLabel)
//        helpInFourthLayer(view: sharesView, imageView: sharesImage, label: sharesLabel)
//        helpInFourthLayer(view: viewsView, imageView: viewsImage, label: viewsLabel)
//        
//        likesButton.fillSuperview()
//    }
//    
//    private func helpInFourthLayer(view: UIView, imageView: UIImageView, label: UILabel) {
//        
//        // imageView constraints
//        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
//        imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
//        imageView.widthAnchor.constraint(equalToConstant: Constants.bottomViewViewsIconSize).isActive = true
//        imageView.heightAnchor.constraint(equalToConstant: Constants.bottomViewViewsIconSize).isActive = true
//        
//        // label constraints
//        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
//        label.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 4).isActive = true
//        label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 16).isActive = true
//    }
//    
//    private func overlayThirdLayerOnBottomView() {
//        bottomView.addSubview(likesView)
//        bottomView.addSubview(commentsView)
//        bottomView.addSubview(sharesView)
//        bottomView.addSubview(viewsView)
//        
//        // likesView constraints
//        likesView.anchor(top: bottomView.topAnchor,
//                         leading: bottomView.leadingAnchor,
//                         bottom: nil,
//                         trailing: nil,
//                         size: CGSize(width: Constants.bottomViewViewWidth, height: Constants.bottomViewViewHeight))
//        
//        // commentsView constraints
//        commentsView.anchor(top: bottomView.topAnchor,
//                         leading: likesView.trailingAnchor,
//                         bottom: nil,
//                         trailing: nil,
//                         size: CGSize(width: Constants.bottomViewViewWidth, height: Constants.bottomViewViewHeight))
//        
//        // sharesView constraints
//        sharesView.anchor(top: bottomView.topAnchor,
//                            leading: commentsView.trailingAnchor,
//                            bottom: nil,
//                            trailing: nil,
//                            size: CGSize(width: Constants.bottomViewViewWidth, height: Constants.bottomViewViewHeight))
//        
//        // viewsView constraints
//        viewsView.anchor(top: bottomView.topAnchor,
//                          leading: nil,
//                          bottom: nil,
//                          trailing: bottomView.trailingAnchor,
//                          size: CGSize(width: Constants.bottomViewViewWidth, height: Constants.bottomViewViewHeight))
//    }
//    
//    private func overlayThirdLayerOnTopView() {
//        topView.addSubview(iconImageView)
//        topView.addSubview(nameLabel)
//        topView.addSubview(dateLabel)
//        
//        // iconImageView constraints
//        iconImageView.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
//        iconImageView.topAnchor.constraint(equalTo: topView.topAnchor).isActive = true
//        iconImageView.heightAnchor.constraint(equalToConstant: Constants.topViewHeight).isActive = true
//        iconImageView.widthAnchor.constraint(equalToConstant: Constants.topViewHeight).isActive = true
//        
//        // nameLabel constraints
//        nameLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 8).isActive = true
//        nameLabel.trailingAnchor.constraint(equalTo: topView.trailingAnchor, constant: -8).isActive = true
//        nameLabel.topAnchor.constraint(equalTo: topView.topAnchor, constant: 4).isActive = true
//        nameLabel.heightAnchor.constraint(equalToConstant: Constants.topViewHeight / 2 - 2).isActive = true
//        
//        // dateLabel constraints
//        dateLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 8).isActive = true
//        dateLabel.trailingAnchor.constraint(equalTo: topView.trailingAnchor, constant: -8).isActive = true
//        dateLabel.bottomAnchor.constraint(equalTo: topView.bottomAnchor, constant: -4).isActive = true
//        dateLabel.heightAnchor.constraint(equalToConstant: 14).isActive = true
//    }
//
//    private func overlaySecondLayer() {
//        cardView.addSubview(topView)
//        cardView.addSubview(postlabel)
//        cardView.addSubview(moreTextButton)
//        cardView.addSubview(postImageView)
//        cardView.addSubview(galleryCollectionView)
//        cardView.addSubview(bottomView)
//        cardView.addSubview(postButton)
//        
//        postImageView.setCorners(radius: 10)
//        
//        // topView constraints
//        topView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 12).isActive = true
//        topView.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -12).isActive = true
//        topView.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 10).isActive = true
//        topView.heightAnchor.constraint(equalToConstant: Constants.topViewHeight).isActive = true
//    }
//    
//    private func overlaySecondRepostLayer() {
//        cardView.addSubview(topRepostView)
//        topRepostView.leadingAnchor.constraint(equalTo: cardView.leadingAnchor, constant: 12).isActive = true
//        topRepostView.trailingAnchor.constraint(equalTo: cardView.trailingAnchor, constant: -12).isActive = true
//        topRepostView.topAnchor.constraint(equalTo: topView.bottomAnchor, constant: 8).isActive = true
//        topRepostView.heightAnchor.constraint(equalToConstant: Constants.topRepostViewHeight).isActive = true
//        
//        overlayLayerOnRepostView()
//    }
//    
//    private func overlayLayerOnRepostView() {
//        topRepostView.addSubview(iconRepostImageView)
//        topRepostView.addSubview(nameRepostLabel)
//        topRepostView.addSubview(dateRepostLabel)
//        
//        // iconImageView constraints
//        iconRepostImageView.leadingAnchor.constraint(equalTo: topRepostView.leadingAnchor).isActive = true
//        iconRepostImageView.topAnchor.constraint(equalTo: topRepostView.topAnchor).isActive = true
//        iconRepostImageView.heightAnchor.constraint(equalToConstant: Constants.topRepostViewHeight).isActive = true
//        iconRepostImageView.widthAnchor.constraint(equalToConstant: Constants.topRepostViewHeight).isActive = true
//        
//        // nameLabel constraints
//        nameRepostLabel.leadingAnchor.constraint(equalTo: iconRepostImageView.trailingAnchor, constant: 8).isActive = true
//        nameRepostLabel.trailingAnchor.constraint(equalTo: topRepostView.trailingAnchor, constant: -8).isActive = true
//        nameRepostLabel.topAnchor.constraint(equalTo: topRepostView.topAnchor, constant: 4).isActive = true
//        nameRepostLabel.heightAnchor.constraint(equalToConstant: Constants.topRepostViewHeight / 2 - 2).isActive = true
//        
//        // dateLabel constraints
//        dateRepostLabel.leadingAnchor.constraint(equalTo: iconRepostImageView.trailingAnchor, constant: 8).isActive = true
//        dateRepostLabel.trailingAnchor.constraint(equalTo: topRepostView.trailingAnchor, constant: -8).isActive = true
//        dateRepostLabel.bottomAnchor.constraint(equalTo: topRepostView.bottomAnchor, constant: -4).isActive = true
//        dateRepostLabel.heightAnchor.constraint(equalToConstant: 14).isActive = true
//    }
//    
//    private func overlayFirstLayer() {
//        addSubview(cardView)
//        
//        // cardView constraints
//        cardView.fillSuperview(padding: Constants.cardInsets)
//        cardView.drawBorder(10, width: 0.5, color: .adaptableDivider)
//    }
//    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        cardView.drawBorder(10, width: 0.5, color: .adaptableDivider)
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//}
