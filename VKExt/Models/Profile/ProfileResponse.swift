//
//  ProfileResponse.swift
//  VKExt
//
//  Created by programmist_NA on 15.07.2020.
//

import Foundation
import SwiftyJSON

struct ProfileResponse: Decodable {
    let id: Int?
    let firstName, lastName: String?
    let isClosed, canAccessClosed: Bool?
    let sex: Int?
    let screenName: String?
    let bdate: String?
    let nickname, domain: String?
    let country: Country?
    let city: City?
    let counters: Counters?
    let timezone: Int?
    let instagram: String?
    let photo100: String?
    let photo50, photo200, photo200Orig: String?
    let photoMax, photo400Orig, photoMaxOrig: String?
    let photoId: String?
    let hasPhoto, hasMobile, isFriend, friendStatus: Int?
    let canPost, canSeeAllPosts, canSeeAudio: Int?
    let canWritePrivateMessage, canSendFriendRequest: Int?
    let mobilePhone, homePhone, site, status: String?
    let lastSeen: LastSeen?
    let onlineInfo: OnlineInfo?
    let cropPhoto: CropPhoto?
    let verified: Int?
    let canBeInvitedGroup: Bool?
    let followersCount: Int?
    let blacklisted, blacklistedByMe, isFavorite: Int?
    let isHiddenFromFeed, commonCount: Int?
    let occupation: Occupation?
    let career: [Career]?
    let military: [Military]?
    let university: Int?
    let universityName: String?
    let faculty: Int?
    let facultyName: String?
    let graduation: Int?
    let homeTown: String?
    let relation: Int?
    let personal: Personal?
    let interests, music, activities, movies: String?
    let tv, books, games: String?
    let universities: [Universities]?
    let schools: [Schools]?
    let about: String?
    let relatives: [Relatives]?
    let quotes: String?
}

// MARK: - Response
struct FriendResponse: Decodable {
    var count: Int?
    var items: [FriendItem]?
}

// MARK: - Item
struct FriendItem: Decodable {
    let id: Int?
    let firstName, lastName: String?
    let isClosed, canAccessClosed: Bool?
    let photo100: String?
    let onlineInfo: OnlineInfo?
    let online: Int?
    let trackCode, homeTown: String?
    let schools: [Schools]?
}

// MARK: - Country
struct Country: Decodable {
    let id: Int?
    let title: String?
}

// MARK: - City
struct City: Decodable {
    let id: Int?
    let title: String?
}

// MARK: - Counters
struct Counters: Decodable {
    let photos: Int?
    let onlineFriends: Int?
    let subscriptions: Int?
    let albums: Int?
    let clipsFollowers: Int?
    let mutualFriends: Int?
    let followers: Int?
    let pages: Int?
    let friends: Int?
    let gifts: Int?
    let audios: Int?
    let videos: Int?
}

// MARK: - Occupation
struct Occupation: Decodable {
    let type: String?
    let id: Int?
    let name: String?
}

// MARK: - OnlineInfo
struct OnlineInfo: Decodable {
    let visible: Bool?
    let lastSeen: Int?
    let isOnline: Bool?
    let appId: Int?
    let isMobile: Bool?
}

// MARK: - CropPhoto
struct CropPhoto: Decodable {
    let photo: ProfilePhoto?
    let crop, rect: Crop?
}

// MARK: - Crop
struct Crop: Decodable {
    let x: Double?
    let y: Double?
    let x2: Double?
    let y2: Double?
}

// MARK: - Photo
struct ProfilePhoto: Decodable {
    let albumId, date, id, ownerId: Int?
    let hasTags: Bool?
    let postId: Int?
    let sizes: [Size]?
    let text: String?
}

// MARK: - Size
struct Size: Decodable {
    let height: Int?
    let url: String?
    let type: String?
    let width: Int?
}

// MARK: - LastSeen
struct LastSeen: Decodable {
    let time, platform: Int?
}

// MARK: - Personal
struct Personal: Decodable {
    let peopleMain, lifeMain, smoking, alcohol: Int?
    let religionId: Int?
}

struct Career: Decodable {
    let groupId: Int?
    let company: String?
    let countryId: Int?
    let cityId: Int?
    let cityName: String?
    let from: Int?
    let until: Int?
    let position: String?
}

struct Military: Decodable {
    let unit: String?
    let unitId: Int?
    let countryId: Int?
    let from: Int?
    let until: Int?
}

struct Universities: Decodable {
    let id: Int?
    let country: Int?
    let city: Int?
    let name: String?
    let faculty: Int?
    let facultyName: String?
    let chair: Int?
    let chairName: String?
    let graduation: Int?
    let educationForm: String?
    let educationStatus: String?
}

struct Schools: Decodable {
    let id: Int?
    let country: Int?
    let city: Int?
    let name: String?
    let yearFrom: Int?
    let yearTo: Int?
    let yearGraduated: Int?
    let `class`: String?
    let speciality: String?
    let type: Int?
    let typeStr: String?
}

struct Relatives: Decodable {
    let id: Int?
    let name: String?
    let type: String?
}
