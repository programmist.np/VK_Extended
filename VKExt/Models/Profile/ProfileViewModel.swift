//
//  ProfileViewModel.swift
//  VKExt
//
//  Created by programmist_NA on 15.07.2020.
//

import Foundation
import UIKit

public enum FriendAction: String {
    case notFriend = "Добавить"
    case notFriendWithNoRequest = "Подписаться"
    case requestSend = "Заявка отправлена"
    case incomingRequest = "Подписан на Вас"
    case isFriend = "У Вас в друзьях"
    
    func setImage() -> String {
        switch self {
        case .notFriend, .notFriendWithNoRequest:
            return "user_add_outline_28"
        case .requestSend:
            return "user_outgoing_outline_28"
        case .incomingRequest:
            return "user_incoming_outline_28"
        case .isFriend:
            return "users_outline_28"
        }
    }
}

enum ProfileModel {
    struct Request {
        enum RequestType {
            case getProfileFirstInfo(userId: Int = VKConstants.currentUserId)
            case getProfileSecondInfo(userId: Int = VKConstants.currentUserId)
            case getProfilePhotos(userId: Int = VKConstants.currentUserId)
            case getProfileWall(userId: Int = VKConstants.currentUserId)
            case revealPostIds(postId: Int)
            case getNextBatch(userId: Int = VKConstants.currentUserId)
            case like(postId: Int, sourceId: Int, type: String)
            case unlike(postId: Int, sourceId: Int, type: String)
        }
    }
    struct Response {
        enum ResponseType {
            case presentProfileFirstInfo(profile: ProfileResponse)
            case presentProfileSecondInfo(profile: ProfileResponse)
            case presentProfilePhotos(profile: PhotoResponse)
            case presentProfileWall(wall: WallResponse, revealdedPostIds: [Int])
            case presentFooterLoader
            case presentFooterError(message: String)
        }
    }
    struct ViewModel {
        enum ViewModelData {
            case displayProfileFirstInfo(profileViewModel: ProfileViewModel)
            case displayProfileSecondInfo(profileViewModel: ProfileViewModel)
            case displayProfilePhotos(profileViewModel: PhotoViewModel)
            case displayWall(wallViewModel: FeedViewModel)
            case displayFooterLoader
            case displayFooterError(message: String)
        }
    }
}

struct ProfileViewModel {
    struct Cell: ProfileCellViewModel {
        var isCurrentProfile: Bool
        var friendActionType: FriendAction
        var type: ProfileActionType
        var id: Int?
        var firstName: String?
        var lastName: String?
        var isClosed: Bool?
        var canAccessClosed: Bool?
        var canPost: Bool?
        var blacklisted: Bool?
        var sex: Int?
        var screenName: String?
        var bdate: String?
        var isOnline: Bool?
        var isMobile: Bool?
        var photo100: String?
        var photoMaxOrig: String?
        var status: String?
        var friendsCount: Int?
        var followersCount: Int?
        var school: String?
        var work: String?
    }

    var cell: Cell?
    let footerTitle: String?
}

protocol ProfileCellViewModel {
    var id: Int? { get }
    var firstName: String? { get }
    var lastName: String? { get }
    var isClosed: Bool? { get }
    var canAccessClosed: Bool? { get }
    var canPost: Bool? { get }
    var blacklisted: Bool? { get }
    var sex: Int? { get }
    var screenName: String? { get }
    var bdate: String? { get }
    var isOnline: Bool? { get }
    var isMobile: Bool? { get }
    var photo100: String? { get }
    var photoMaxOrig: String? { get }
    var status: String? { get }
    // ---
    var friendsCount: Int? { get }
    var followersCount: Int? { get }
    var school: String? { get }
    var work: String? { get }
    var type: ProfileActionType { get }
    var friendActionType: FriendAction { get }
    var isCurrentProfile: Bool { get }
}

enum ProfileActionType {
    case actionFriendWithMessage
    case actionFriend
}

enum FriendModel {
    struct Request {
        enum RequestType {
            case getFriend(userId: Int = VKConstants.currentUserId)
            case getFollowers(userId: Int = VKConstants.currentUserId)
            case getNextFriends(userId: Int = VKConstants.currentUserId)
            case getNextFollowers(userId: Int = VKConstants.currentUserId)
        }
    }
    struct Response {
        enum ResponseType {
            case presentFriend(response: FriendResponse)
            case presentFooterLoader
            case presentFooterError(message: String)
        }
    }
    struct ViewModel {
        enum ViewModelData {
            case displayFriend(friendViewModel: FriendViewModel)
            case displayFooterLoader
            case displayFooterError(message: String)
        }
    }
}

struct FriendViewModel {
    struct Cell: FriendCellViewModel {
        var id: Int?
        var firstName: String?
        var lastName: String?
        var isClosed: Bool?
        var isOnline: Bool?
        var isMobile: Bool?
        var photo100: String?
        var school: String?
        var homeTown: String?
    }
    
    var cell: [Cell]
    let footerTitle: String?
}

protocol FriendCellViewModel {
    var id: Int? { get }
    var firstName: String? { get }
    var lastName: String? { get }
    var isClosed: Bool? { get }
    var isOnline: Bool? { get }
    var isMobile: Bool? { get }
    var photo100: String? { get }
    var school: String? { get }
    var homeTown: String? { get }
}

typealias Animation = (UITableViewCell, IndexPath, UITableView) -> Void

final class Animator {
    private var hasAnimatedAllCells = false
    private let animation: Animation
    
    init(animation: @escaping Animation) {
        self.animation = animation
    }
    
    func animate(cell: UITableViewCell, at indexPath: IndexPath, in tableView: UITableView) {
        guard !hasAnimatedAllCells else {
            return
        }
        
        animation(cell, indexPath, tableView)
        
        hasAnimatedAllCells = tableView.isLastVisibleCell(at: indexPath)
    }
}

enum AnimationFactory {
    static func makeMoveUpWithBounce(rowHeight: CGFloat, duration: TimeInterval, delayFactor: Double) -> Animation {
        return { cell, indexPath, tableView in
            cell.transform = CGAffineTransform(translationX: 0, y: rowHeight)
            
            UIView.animate(
                withDuration: duration,
                delay: delayFactor * Double(indexPath.row),
                usingSpringWithDamping: 0.4,
                initialSpringVelocity: 0.1,
                options: [.curveEaseInOut],
                animations: {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0)
                })
        }
    }
    
    static func makeFadeAnimation(duration: TimeInterval, delayFactor: Double) -> Animation {
        return { cell, indexPath, _ in
            cell.alpha = 0
            
            UIView.animate(
                withDuration: duration,
                delay: delayFactor * Double(indexPath.row),
                animations: {
                    cell.alpha = 1
                })
        }
    }
    
    static func makeMoveUpWithFade(rowHeight: CGFloat, duration: TimeInterval, delayFactor: Double) -> Animation {
        return { cell, indexPath, _ in
            cell.transform = CGAffineTransform(translationX: 0, y: rowHeight / 2)
            cell.alpha = 0
            
            UIView.animate(
                withDuration: duration,
                delay: delayFactor * Double(indexPath.row),
                options: [.curveEaseInOut],
                animations: {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0)
                    cell.alpha = 1
                })
        }
    }
}
