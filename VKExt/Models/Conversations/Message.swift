//
//  Message.swift
//  VKExt
//
//  Created by programmist_NA on 01.06.2020.
//

import Foundation
import RealmSwift
import SwiftyJSON
import UIKit

final class MessagesList: Object {
    let items = List<Message>()
}

class Message: Object {
    @objc dynamic var dateInt: Int = 0
    @objc dynamic var date: String = "0"
    @objc dynamic var fromId: Int = 0
    @objc dynamic var id: Int = 0
    @objc dynamic var out: Int = 0
    @objc dynamic var inRead: Int = 0
    @objc dynamic var outRead: Int = 0
    @objc dynamic var unreadCount: Int = 0
    @objc dynamic var peerId: Int = 0
    @objc dynamic var text: String = "0"
    @objc dynamic var conversationMessageId: Int = 0
    @objc dynamic var isImportant: Bool = false
    @objc dynamic var randomId: Int64 = 0
    @objc dynamic var isTyping: Bool = false
    @objc dynamic var isRemoved: Bool = false
    @objc dynamic var removingFlag: Int = 0
    @objc dynamic var attachmentType: String = ""
    @objc dynamic var forwardMessagesCount: Int = 0
    @objc dynamic var hasReplyMessage: Bool = false
    @objc dynamic var hasAttachments: Bool = false
    @objc dynamic var hasForwardedMessages: Bool = false
    @objc dynamic var primaryMessageKey: String = ""
    dynamic var attachments = List<Attachments>()
    
    convenience init(message: JSON, conversation: JSON) {
        self.init()
        self.dateInt = message["date"].intValue
        self.date = ConversationService.messageTime(time: message["date"].intValue)
        self.fromId = message["from_id"].intValue
        self.id = message["id"].intValue
        self.out = message["out"].intValue
        self.inRead = conversation["in_read"].intValue
        self.outRead = conversation["out_read"].intValue
        self.unreadCount = conversation["unread_count"].intValue
        self.peerId = message["peer_id"].intValue
        self.text = message["text"].stringValue
        self.conversationMessageId = message["conversation_message_id"].intValue
        self.isImportant = message["important"].boolValue
        self.randomId = Int64(message["random_id"].intValue)
        self.isTyping = false
        self.isRemoved = UserDefaults.standard.bool(forKey: "isRemoved_\(message["conversation_message_id"].intValue)")
        self.removingFlag = 0
        if let attachments = message["attachments"].array {
            for attachment in attachments {
                self.attachments.append(Attachments(json: attachment))
            }
            if let firstAttachmentType = attachments.first?["type"].stringValue {
                self.attachmentType = firstAttachmentType
                self.hasAttachments = true
            }
        } else {
            self.attachmentType = "none"
            self.attachments.removeAll()
        }
        if let forwardMessages = message["fwd_messages"].array {
            if forwardMessages.count > 0 {
                self.forwardMessagesCount = forwardMessages.count
                self.hasForwardedMessages = true
            }
        }
        if !(message["reply_message"] == JSON.null) {
            self.hasReplyMessage = true
        }
        self.primaryMessageKey = "\(message["peer_id"].intValue + message["from_id"].intValue + message["date"].intValue)"
    }
    
    convenience init(date: Date, fromId: Int, peerId: Int, text: String, conversationMessageId: Int, randomId: Int64, hasAttachments: Bool, attachmentType: String, forwardMessagesCount: Int, hasForwardedMessages: Bool, hasReplyMessage: Bool, conversation: Conversation) {
        self.init()
        self.dateInt = NSNumber(value: date.timeIntervalSince1970).intValue
        self.date = ConversationService.messageTime(time: NSNumber(value: date.timeIntervalSince1970).intValue)
        self.fromId = fromId
        self.id = 0
        self.out = 1
        self.inRead = conversation.inRead
        self.outRead = conversation.outRead
        self.unreadCount = conversation.unreadCount
        self.peerId = peerId
        self.text = text
        self.conversationMessageId = conversationMessageId
        self.isImportant = false
        self.randomId = randomId
        self.isTyping = false
        self.isRemoved = UserDefaults.standard.bool(forKey: "isRemoved_\(conversationMessageId)")
        self.removingFlag = 0
        self.attachmentType = attachmentType
        self.hasAttachments = hasAttachments
        self.forwardMessagesCount = forwardMessagesCount
        self.hasForwardedMessages = hasForwardedMessages
        self.hasReplyMessage = hasReplyMessage
        self.primaryMessageKey = "\(peerId + fromId + NSNumber(value: date.timeIntervalSince1970).intValue)"
    }
    
    convenience init(message: JSON, conversation: Conversation) {
        self.init()
        self.dateInt = message["date"].intValue
        self.date = ConversationService.messageTime(time: message["date"].intValue)
        self.fromId = message["from_id"].intValue
        self.id = message["id"].intValue
        self.out = message["out"].intValue
        self.inRead = conversation.inRead
        self.outRead = conversation.outRead
        self.unreadCount = conversation.unreadCount
        self.peerId = message["peer_id"].intValue
        self.text = message["text"].stringValue == "" ? "Вложение..." : message["text"].stringValue
        self.conversationMessageId = message["conversation_message_id"].intValue
        self.isImportant = message["important"].boolValue
        self.randomId = Int64(message["random_id"].intValue)
        self.isTyping = false
        self.isRemoved = UserDefaults.standard.bool(forKey: "isRemoved_\(message["conversation_message_id"].intValue)")
        self.removingFlag = 0
        if let attachments = message["attachments"].array {
            for attachment in attachments {
                self.attachments.append(Attachments(json: attachment))
            }
            if let firstAttachment = attachments.first?["type"].stringValue {
                self.attachmentType = firstAttachment
                self.hasAttachments = true
            }
        } else {
            self.attachmentType = "none"
            self.attachments.removeAll()
        }
        if let forwardMessages = message["fwd_messages"].array {
            if forwardMessages.count > 0 {
                self.forwardMessagesCount = forwardMessages.count
                self.hasForwardedMessages = true
            }
        }
        if !(message["reply_message"] == JSON.null) {
            self.hasReplyMessage = true
        }
        self.primaryMessageKey = "\(message["peer_id"].intValue + message["from_id"].intValue + message["date"].intValue)"
    }
    
    open var unreadStatus: ReadStatus {
        get {
            if isOutgoing {
                if self.id > self.outRead {
                    return .unreadOut
                } else {
                    return .readOut
                }
            } else {
                if self.id > self.inRead {
                    return .unreadIn
                } else {
                    return .readIn
                }
            }
        }
    }
    
    var isOutgoing: Bool {
        get {
            return self.out == 1
        }
    }
    
    var isSending: Bool {
        get {
            return self.id == 0
        }
    }
    
    override class func primaryKey() -> String? {
        return "primaryMessageKey"
    }
}
extension Message: Comparable {
    static func < (lhs: Message, rhs: Message) -> Bool {
        return lhs.dateInt < rhs.dateInt
    }
}
