//
//  MessageAttachment.swift
//  VKExt
//
//  Created by programmist_NA on 12.06.2020.
//

import Foundation
import UIKit
import SwiftyJSON
//
//struct MessageAttachment {
//    let type: String
//    let photo: MessagePhoto?
//    let sticker: MessageSticker?
//    let voiceMessage: VoiceMessage?
//    let music: MessageAudio?
//    
//    init(attachment: JSON?) {
//        guard let attachment = attachment else {
//            self.type = ""
//            self.sticker = nil
//            self.photo = nil
//            self.voiceMessage = nil
//            self.music = nil
//            return
//        }
//        self.type = attachment["type"].stringValue
//        if !(attachment["sticker"] == JSON.null) {
//            self.sticker = MessageSticker(sticker: attachment["sticker"])
//        } else {
//            self.sticker = nil
//        }
//        if !(attachment["photo"] == JSON.null) {
//            self.photo = MessagePhoto(photo: attachment["photo"])
//        } else {
//            self.photo = nil
//        }
//        if !(attachment["audio_message"] == JSON.null) {
//            self.voiceMessage = VoiceMessage(audioMessage: attachment["audio_message"])
//        } else {
//            self.voiceMessage = nil
//        }
//        if !(attachment["audio"] == JSON.null) {
//            self.music = MessageAudio(audio: attachment["audio"])
//        } else {
//            self.music = nil
//        }
//    }
//}
//
//struct PhotoSizes {
//    let type: String
//    let url: String
//    let width: Int
//    let height: Int
//    
//    init(sizes: JSON) {
//        self.type = sizes["type"].stringValue
//        self.url = sizes["url"].stringValue
//        self.width = sizes["width"].intValue
//        self.height = sizes["height"].intValue
//    }
//}
//
//struct MessagePhoto: MediaItem {
//    let url: URL?
//    let image: UIImage?
//    let placeholderImage: UIImage
//    let id: Int
//    let albumId: Int
//    let ownerId: Int
//    let userId: Int
//    let text: String
//    let date: Int
//    let sizes: [PhotoSizes]?
//    let height: Int
//    let width: Int
//    let size: CGSize
//    
//    init(photo: JSON) {
//        self.id = photo["id"].intValue
//        self.albumId = photo["albumId"].intValue
//        self.ownerId = photo["ownerId"].intValue
//        self.userId = photo["userId"].intValue
//        self.text = photo["text"].stringValue
//        self.date = photo["date"].intValue
//        if let sizes = photo["sizes"].array {
//            self.sizes = sizes.map { PhotoSizes(sizes: $0) }
//        } else {
//            self.sizes = nil
//        }
//        self.height = photo["sizes"].arrayValue.first(where: { $0["type"].stringValue == "q" })!["height"].intValue
//        self.width = photo["sizes"].arrayValue.first(where: { $0["type"].stringValue == "q" })!["width"].intValue
//        self.url = URL(string: photo["url"].stringValue)
//        self.image = UIImage(data: try! Data(contentsOf: URL(string: photo["sizes"].arrayValue.first(where: { $0["type"].stringValue == "q" })!["url"].stringValue)!))
//        self.placeholderImage = UIImage()
//        self.size = CGSize(width: photo["width"].intValue, height: photo["height"].intValue)
//    }
//}
//
//struct MessageSticker: MediaItem {
//    let url: URL?
//    let image: UIImage?
//    let placeholderImage: UIImage
//    let productId: Int
//    let stickerId: Int
//    let height: Int
//    let width: Int
//    var size: CGSize {
//        return CGSize(width: self.width, height: self.height)
//    }
//    
//    init(sticker: JSON) {
//        self.productId = sticker["productId"].intValue
//        self.stickerId = sticker["stickerId"].intValue
//        self.height = sticker["images"].arrayValue.first(where: { $0["height"].intValue == 128 })!["height"].intValue
//        self.width = sticker["images"].arrayValue.first(where: { $0["width"].intValue == 128 })!["width"].intValue
//        self.url = URL(string: sticker["url"].stringValue)
//        self.image = UIImage(data: try! Data(contentsOf: URL(string: sticker["images"].arrayValue.first(where: { $0["height"].intValue == 512 })!["url"].stringValue)!))
//        self.placeholderImage = UIImage()
//    }
//}
//
//struct VoiceMessage: AudioItem {
//    let id: Int
//    let owmerId: Int
//    let waveForm: [Int]
//    let url: URL
//    let duration: Float
//    let size: CGSize
//    
//    init(audioMessage: JSON) {
//        self.id = audioMessage["id"].intValue
//        self.owmerId = audioMessage["owmerId"].intValue
//        self.waveForm = audioMessage["waveForm"].arrayValue.map { $0.intValue }
//        self.url = URL(string: audioMessage["link_mp3"].stringValue)!
//        self.duration = audioMessage["duration"].floatValue
//        self.size = CGSize(width: 150, height: 52)
//    }
//}
//struct MessageAudio: AudioItem {
//    let id: Int
//    let owmerId: Int
//    let artist: String
//    let title: String
//    let url: URL
//    let duration: Float
//    let size: CGSize
//    
//    init(audio: JSON) {
//        self.id = audio["id"].intValue
//        self.owmerId = audio["owmerId"].intValue
//        self.artist = audio["artist"].stringValue
//        self.title = audio["title"].stringValue
//        self.url = URL(string: audio["url"].stringValue)!
//        self.duration = audio["duration"].floatValue
//        self.size = CGSize(width: 150, height: 52)
//    }
//}
