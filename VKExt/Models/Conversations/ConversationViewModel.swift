//
//  ConversationViewModel.swift
//  VKExt
//
//  Created by programmist_NA on 24.07.2020.
//

import Foundation
import UIKit

enum Conversations {
    enum Model {
        struct Response {
            enum ResponseType {
                case presentConversation(conversation: Conversation)
                case presentConversations(conversations: [Conversation])
                case presentFooterLoader
                case presentFooterError(message: String)
            }
        }
        struct ViewModel {
            enum ViewModelData {
                case displayConversation(conversationViewModel: ConversationViewModel)
                case displayFooterLoader
                case displayFooterError(message: String)
            }
        }
    }
}

struct ConversationViewModel {
    struct Cell: ConversationCellViewModel {
        var isMuted: Bool
        var unreadStatus: ReadStatus
        var isOutgoing: Bool
        var senderId: String
        var displayName: String
        var peerId: Int
        var type: String
        var localId: Int
        var lastMessageId: Int
        var inRead: Int
        var outRead: Int
        var canWriteAllowed: Bool
        var canWriteReason: Int
        var isImportantDialog: Bool
        var isMarkedUnread: Bool
        var membersCount: Int
        var title: String?
        var state: String?
        var activeIds: [Int]
        var isGroupChannel: Bool
        var disabledUntil: Int
        var disabledForever: Bool
        var noSound: Bool
        var unreadCount: Int
        var date: String
        var dateInteger: Int
        var fromId: Int
        var messageId: Int
        var out: Int
        var text: String
        var conversationMessageId: Int
        var isImportantMessage: Bool
        var randomId: Int
        var isTyping: Bool
        var attachmentType: String
        var forwardMessagesCount: Int
        var hasReplyMessage: Bool
        var hasAttachments: Bool
        var hasForwardedMessages: Bool
        var interlocutorId: Int
        var interlocutorName: String
        var interlocutorPhoto50: String?
        var interlocutorPhoto100: String?
        var interlocutorPhoto200: String?
        var isOnline: Bool
        var isMobileOnline: Bool
        var sex: Int
        var lastSeen: Int
        var verified: Int
        var isRemoved: Bool
        var removingFlag: Int
        var isAggressiveTypingActive: Bool
        var aggressiveTypingType: String
        var isPrivateConversation: Bool
    }
    var cells: [Cell]
    let footerTitle: String?
}

protocol ConversationCellViewModel {
    // MARK: Conversation
    var senderId: String { get }
    var displayName: String { get }
    var peerId: Int { get }
    var type: String { get }
    var localId: Int { get }
    var lastMessageId: Int { get }
    var inRead: Int { get }
    var outRead: Int { get }
    var canWriteAllowed: Bool { get }
    var canWriteReason: Int { get }
    var isImportantDialog: Bool { get }
    var isMarkedUnread: Bool { get }
    // Chat Settings
    var membersCount: Int { get }
    var title: String? { get }
    // var pinnedMessage: DBMessage?
    var state: String? { get }
    var activeIds: [Int] { get }
    var isGroupChannel: Bool { get }
    // Push Settings
    var disabledUntil: Int { get }
    var disabledForever: Bool { get }
    var noSound: Bool { get }
    var unreadCount: Int { get }
    
    // MARK: LastMessage
    var date: String { get }
    var dateInteger: Int { get }
    var fromId: Int { get }
    var messageId: Int { get }
    var out: Int { get }
    var text: String { get }
    var conversationMessageId: Int { get }
    var isImportantMessage: Bool { get }
    var randomId: Int { get }
    var isTyping: Bool { get }
    
    var attachmentType: String { get }
    var forwardMessagesCount: Int { get }
    var hasReplyMessage: Bool { get }
    var hasAttachments: Bool { get }
    var hasForwardedMessages: Bool { get }
    // MARK: Interlocutor
    var interlocutorId: Int { get }
    var interlocutorName: String { get }
    var interlocutorPhoto50: String? { get }
    var interlocutorPhoto100: String? { get }
    var interlocutorPhoto200: String? { get }
    var isOnline: Bool { get }
    var isMobileOnline: Bool { get }
    var sex: Int { get }
    var lastSeen: Int { get }
    var verified: Int { get }
    // MARK: NEW FEATURES
    // Stored removing messages
    var isRemoved: Bool { get }
    var removingFlag: Int { get }
    // Aggressive Typing
    var isAggressiveTypingActive: Bool { get }
    var aggressiveTypingType: String { get }
    // Private conversation
    var isPrivateConversation: Bool { get }
    
    var isOutgoing: Bool { get }
    var isMuted: Bool { get }
    var unreadStatus: ReadStatus { get }
}
