//
//  Conversation.swift
//  VK Exteded
//
//  Created by programmist_np on 06.05.2020.
//  Copyright © 2020 programmist_np. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift
import SwiftyVK

enum ReadStatus: String {
    case unreadIn
    case unreadOut
    case readIn
    case readOut
    case markedUnread
}

enum PrimatyConversationType: String {
    case text
    case sticker
    case empty
}

enum SecondaryConversationType: String {
    case none
    case image
    case music
    case audioMessage
    case doc
    case wall
    case link
    case video
}

class Conversation: Object {
    // MARK: Conversation
    @objc dynamic var senderId: String = ""
    @objc dynamic var displayName: String = ""
    @objc dynamic var peerId: Int = 0
    @objc dynamic var type: String = ""
    @objc dynamic var localId: Int = 0
    @objc dynamic var lastMessageId: Int = 0
    @objc dynamic var inRead: Int = 0
    @objc dynamic var outRead: Int = 0
    @objc dynamic var canWriteAllowed: Bool = false
    @objc dynamic var canWriteReason: Int = 0
    @objc dynamic var isImportantDialog: Bool = false
    @objc dynamic var isMarkedUnread: Bool = false
    // Chat Settings
    @objc dynamic var membersCount: Int = 0
    @objc dynamic var title: String?
    // var pinnedMessage: DBMessage?
    @objc dynamic var state: String?
    var activeIds: [Int] = []
    @objc dynamic var isGroupChannel: Bool = false
    // Push Settings
    @objc dynamic var disabledUntil: Int = 0
    @objc dynamic var disabledForever: Bool = false
    @objc dynamic var noSound: Bool = false
    @objc dynamic var unreadCount: Int = 0
    
    // MARK: LastMessage
    @objc dynamic var date: String = "0"
    @objc dynamic var dateInteger: Int = 0
    @objc dynamic var fromId: Int = 0
    @objc dynamic var messageId: Int = 0
    @objc dynamic var out: Int = 0
    @objc dynamic var text: String = "0"
    @objc dynamic var conversationMessageId: Int = 0
    @objc dynamic var isImportantMessage: Bool = false
    @objc dynamic var randomId: Int = 0
    @objc dynamic var isTyping: Bool = false
    
    // @objc dynamic var attachmentType: MessageAttachmentType.RawValue = MessageAttachmentType.none.rawValue
    @objc dynamic var primaryType: PrimatyConversationType.RawValue = PrimatyConversationType.empty.rawValue
    @objc dynamic var secondType: MessageAttachmentType.RawValue = MessageAttachmentType.none.rawValue
    @objc dynamic var forwardMessagesCount: Int = 0
    @objc dynamic var hasReplyMessage: Bool = false
    @objc dynamic var hasAttachments: Bool = false
    @objc dynamic var hasForwardedMessages: Bool = false
    // MARK: Interlocutor
    @objc dynamic var interlocutorId: Int = 0
    @objc dynamic var interlocutorName: String = ""
    @objc dynamic var interlocutorPhoto50: String?
    @objc dynamic var interlocutorPhoto100: String?
    @objc dynamic var interlocutorPhoto200: String?
    @objc dynamic var isOnline: Bool = false
    @objc dynamic var isMobileOnline: Bool = false
    @objc dynamic var sex: Int = 0
    @objc dynamic var lastSeen: Int = 0
    @objc dynamic var verified: Int = 0
    // MARK: NEW FEATURES
    // Stored removing messages
    @objc dynamic var isRemoved: Bool = false
    @objc dynamic var removingFlag: Int = 0
    // Aggressive Typing
    @objc dynamic var isAggressiveTypingActive: Bool = false
    @objc dynamic var aggressiveTypingType: String = "none"
    // Private conversation
    @objc dynamic var isPrivateConversation: Bool = false
    
    convenience init(conversation: JSON, lastMessage: JSON, representable: ConversationSenderRepresenatable? = nil) {
        self.init()
        // MARK: Conversation
        self.peerId = conversation["peer"]["id"].intValue
        self.senderId = conversation["peer"]["id"].stringValue
        self.type = conversation["peer"]["type"].stringValue
        self.localId = conversation["peer"]["local_id"].intValue
        self.lastMessageId = conversation["last_message_id"].intValue
        self.inRead = conversation["in_read"].intValue
        self.outRead = conversation["out_read"].intValue
        self.canWriteAllowed = conversation["can_write"]["allowed"].boolValue
        self.canWriteReason = conversation["can_write"]["reason"].intValue
        self.isMarkedUnread = conversation["is_marked_unread"].boolValue
        self.isImportantDialog = UserDefaults.standard.bool(forKey: "importantState_from_\(self.peerId)")
        self.isAggressiveTypingActive = UserDefaults.standard.bool(forKey: "permanent_typing_from\(self.peerId)")
        self.isPrivateConversation = UserDefaults.standard.bool(forKey: "private_conversation_from\(self.peerId)")
        self.membersCount = conversation["chat_settings"]["members_count"].intValue
        self.title = conversation["chat_settings"]["title"].stringValue
        self.displayName = conversation["chat_settings"]["title"].stringValue
        self.unreadCount = conversation["unread_count"].intValue
        // self.pinnedMessage = DBMessage(message: conversation["chat_settings"]["pinned_message"])
        self.state = conversation["chat_settings"]["state"].stringValue
        self.activeIds.append(contentsOf: conversation["chat_settings"]["active_ids"].arrayValue.map { $0.intValue })
        self.isGroupChannel = conversation["chat_settings"]["is_group_channel"].boolValue
        self.aggressiveTypingType = self.isAggressiveTypingActive ? UserDefaults.standard.string(forKey: "permanent_typing_type_from\(self.peerId)") ?? "none" : "none"
        self.disabledUntil = conversation["push_settings"].dictionary?["disabled_until"]?.intValue ?? 0
        self.disabledForever = conversation["push_settings"].dictionary?["disabled_forever"]?.boolValue ?? false
        self.noSound = conversation["push_settings"].dictionary?["no_sound"]?.boolValue ?? false
        // MARK: LastMessage
        self.date = ConversationService.messageTime(time: lastMessage["date"].intValue)
        self.dateInteger = lastMessage["date"].intValue
        self.fromId = lastMessage["from_id"].intValue
        self.out = lastMessage["out"].intValue
        self.peerId = lastMessage["peer_id"].intValue
        self.text = lastMessage["text"].stringValue
        self.primaryType = lastMessage["text"].stringValue != "" ? PrimatyConversationType.text.rawValue : PrimatyConversationType.empty.rawValue
        self.conversationMessageId = lastMessage["conversation_message_id"].intValue
        self.isImportantMessage = lastMessage["important"].boolValue
        self.randomId = lastMessage["random_id"].intValue
        self.isTyping = false
        self.isRemoved = false
        self.removingFlag = 0
        self.secondType = Attachments.typeAttachment(string: lastMessage["attachments"].array?.first?["type"].stringValue).rawValue
        self.hasAttachments = lastMessage["attachments"].array?.first?["type"].stringValue != nil
        self.forwardMessagesCount = lastMessage["fwd_messages"].arrayValue.count
        self.hasForwardedMessages = lastMessage["fwd_messages"].arrayValue.count > 0
        self.hasReplyMessage = !(lastMessage["reply_message"] == JSON.null)
        if let representable = representable {
            self.interlocutorId = representable.id
            self.interlocutorName = representable.name
            self.interlocutorPhoto100 = representable.photo
            self.isOnline = representable.isOnline ?? false
            self.isMobileOnline = self.isOnline ? representable.isMobileOnline ?? false : false
            self.sex = representable.sex ?? 0
            self.lastSeen = representable.lastSeen ?? 0
            self.verified = representable.verified ?? 0
        } else {
            self.interlocutorId = conversation["peer"]["id"].intValue
            self.interlocutorName = conversation["chat_settings"]["title"].stringValue
            self.interlocutorPhoto50 = conversation["chat_settings"]["photo"]["photo_50"].stringValue
            self.interlocutorPhoto100 = conversation["chat_settings"]["photo"]["photo_100"].stringValue
            self.interlocutorPhoto200 = conversation["chat_settings"]["photo"]["photo_200"].stringValue
        }
    }

    var isOutgoing: Bool {
        return self.out == 1 ? true : false
    }
    
    open var isMuted: Bool {
        return disabledUntil > 0 || disabledForever
    }
    
    open var unreadStatus: ReadStatus {
        if outRead != lastMessageId {
            return .unreadOut
        } else if inRead == outRead && isOutgoing {
            return .readOut
        } else if outRead == lastMessageId && unreadCount == 0 && !isMarkedUnread {
            return .readOut
        } else if isMarkedUnread && unreadCount < 1 {
            return .markedUnread
        } else if unreadCount > 0 {
            return .unreadIn
        } else {
            return .readIn
        }
    }

    class func getAvatarAcronymColor(at idIndex: Int, completion: @escaping(_ gradient: GradientColors?) -> Void) {
        for index in 0...idIndex {
            if index < 6 {
                completion(UIColor.getRandomGradient(at: index))
            } else {
                continue
            }
        }
    }
    
    func getCanWriteMessage(by reason: Int) -> String {
        switch reason {
        case 18: return "Пользователь заблокирован или удален"
        case 900: return "Пользователь в черном списке"
        case 901: return "Пользователь запретил сообщения от сообщества"
        case 902: return "Пользователь ограничил круг лиц, которые ему могут написать"
        case 915: return "В сообществе отключены сообщения"
        case 916: return "В сообществе заблокированы сообщения"
        case 917: return "Нет доступа к чату"
        case 918: return "Нет доступа к e-mail"
        case 203: return "Нет доступа к сообществу"
        default: return "Запрещено (неизвестный код)"
        }
    }
    
    override class func primaryKey() -> String? {
        return "peerId"
    }
}

@objc protocol ConversationSenderRepresenatable {
    var id: Int { get }
    var name: String { get }
    var photo: String { get }
    @objc optional var sex: Int { get }
    @objc optional var verified: Int { get }
    @objc optional var isOnline: Bool { get }
    @objc optional var isMobileOnline: Bool { get }
    @objc optional var lastSeen: Int { get }
}

class ConversationProfile: Decodable, ConversationSenderRepresenatable {
    let id: Int
    let firstName: String
    let lastName: String
    let photo100: String
    let onlineInfo: OnlineInfo
    var sex: Int
    var verified: Int
    var name: String { return firstName + " " + lastName }
    var photo: String { return photo100 }
    var isOnline: Bool { return onlineInfo.isOnline ?? false }
    var isMobileOnline: Bool { return onlineInfo.isMobile ?? false }
    var lastSeen: Int { return onlineInfo.lastSeen ?? 0 }
    
    init(profileJSON: JSON) {
        self.id = profileJSON["id"].intValue
        self.firstName = profileJSON["first_name"].stringValue
        self.lastName = profileJSON["last_name"].stringValue
        self.photo100 = profileJSON["photo_100"].stringValue
        self.onlineInfo = OnlineInfo(
            visible: profileJSON["online_info"]["visible"].boolValue,
            lastSeen: profileJSON["online_info"]["last_seen"].intValue,
            isOnline: profileJSON["online_info"]["is_online"].boolValue,
            appId: profileJSON["online_info"]["app_id"].intValue,
            isMobile: profileJSON["online_info"]["is_mobile"].boolValue
        )
        self.sex = profileJSON["sex"].intValue
        self.verified = profileJSON["verified"].intValue
    }
}

class ConversationGroup: Decodable, ConversationSenderRepresenatable {
    let id: Int
    let name: String
    let photo100: String
    var verified: Int
    var photo: String { return photo100 }
    
    init(groupJSON: JSON) {
        self.id = groupJSON["id"].intValue
        self.name = groupJSON["name"].stringValue
        self.photo100 = groupJSON["photo_100"].stringValue
        self.verified = groupJSON["verified"].intValue
    }
}
