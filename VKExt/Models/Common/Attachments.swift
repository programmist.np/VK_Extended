//
//  Attachments.swift
//  VK Tosters
//
//  Created by programmist_np on 31/01/2020.
//  Copyright © 2020 programmist_np. All rights reserved.
//

import Foundation
import SwiftyJSON
import Kingfisher
import RealmSwift

enum MessageAttachmentType: String {
    case photo = "Фото"
    case video = "Видео"
    case audio = "Аудио"
    case audio_message = "Голосовое сообщение"
    case doc = "Документ"
    case link = "Ссылка"
    case market = "Товар"
    case market_album = "Подборка товаров"
    case wall = "Запись"
    case wall_reply = "Комментарий"
    case sticker = "Стикер"
    case gift = "Подарок"
    case graffity = "Граффити"
    case call = "Звонок"
    case unknown = "Неизвестное вложение"
    case none = ""
}

class Attachments: Object {
    @objc dynamic var type: String = ""
    @objc dynamic var attachType: String = ""
    @objc dynamic var photo: PhotoAttachment? = PhotoAttachment()
    @objc dynamic var sticker: Sticker? = Sticker()
    @objc dynamic var voiceMessage: AudioMessage? = AudioMessage()
    @objc dynamic var music: Music? = Music()
    
    convenience init(json: JSON) {
        self.init()
        self.type = Attachments.typeAttachment(string: json["type"].stringValue).rawValue
        self.attachType = json["type"].stringValue
        if !(json["photo"] == JSON.null) {
            self.photo = PhotoAttachment(json: json["photo"], sizes: self.getSize(sizes: json["photo"]["sizes"].arrayValue))
        }
        if !(json["sticker"] == JSON.null) {
            self.sticker = Sticker(json: json["sticker"], images: self.getImage(images: json["sticker"]["images"].arrayValue))
        }
        if !(json["audio_message"] == JSON.null) {
            self.voiceMessage = AudioMessage(json: json["audio_message"])
        }
        if !(json["audio"] == JSON.null) {
            self.music = Music(json: json["audio"])
        }
    }
    
    func getImage(images: [JSON]) -> Images {
        if let image128 = images.first(where: { $0["height"].intValue == 128 }) {
            let image = Images(json: image128)
            return image
        } else {
            let fallbackImage = images.last!
            let image = Images(json: fallbackImage)
            return image
        }
    }
    
    func getSize(sizes: [JSON]) -> Sizes {
        if let sizeX = sizes.first(where: { $0["type"].stringValue == "x" }) {
            let size = Sizes(JSON: sizeX)
            return size
        } else {
            let fallbackSize = sizes.last!
            let size = Sizes(JSON: fallbackSize)
            return size
        }
    }
    
    class func typeAttachment(string: String?) -> MessageAttachmentType {
        switch string {
        case "photo":
            return .photo
        case "video":
            return .video
        case "audio":
            return .audio
        case "audio_message":
            return .audio_message
        case "doc":
            return .doc
        case "link":
            return .link
        case "market":
            return .market
        case "market_album":
            return .market_album
        case "wall":
            return .wall
        case "wall_reply":
            return .wall_reply
        case "sticker":
            return .sticker
        case "gift":
            return .gift
        case "graffiti":
            return .graffity
        case "call":
            return .call
        case "":
            return .none
        default:
            return .unknown
        }
    }
}

class Sizes: NSObject {
    @objc var type: String = ""
    @objc var url: String = ""
    @objc var width: Int = 0
    @objc var height: Int = 0
    
    convenience init(JSON: JSON) {
        self.init()
        self.type = JSON["type"].stringValue
        self.url = JSON["url"].stringValue
        self.width = JSON["width"].intValue
        self.height = JSON["height"].intValue
    }
}

class PhotoAttachment: Object {
    @objc var id: Int = 0
    @objc var albumId: Int = 0
    @objc var ownerId: Int = 0
    @objc var userId: Int = 0
    @objc var text: String = ""
    @objc dynamic var urlString: String = ""
    @objc var date: Int = 0
    var sizes: Sizes?
    @objc dynamic var height: Int = 0
    @objc dynamic var width: Int = 0
    
    convenience init(json: JSON, sizes: Sizes?) {
        self.init()
        self.id = json["id"].intValue
        self.albumId = json["album_id"].intValue
        self.ownerId = json["owner_id"].intValue
        self.userId = json["user_id"].intValue
        self.text = json["text"].stringValue
        self.date = json["date"].intValue
        self.sizes = sizes
        self.height = (sizes?.height ?? 0) / 2
        self.width = (sizes?.width ?? 0) / 2
        self.urlString = json["sizes"].arrayValue.first(where: { $0["type"].stringValue == "x" })!["url"].stringValue
    }
}

class Sticker: Object {
    @objc dynamic var productId: Int = 0
    @objc dynamic var stickerId: Int = 0
    @objc dynamic var height: Int = 0
    @objc dynamic var width: Int = 0
    @objc dynamic var urlString: String = ""
    @objc dynamic var animationUrlString: String = ""
    
    convenience init(json: JSON, images: Images?) {
        self.init()
        self.productId = json["productId"].intValue
        self.stickerId = json["stickerId"].intValue
        self.height = (images?.height ?? 0) + 32
        self.width = (images?.width ?? 0) + 32
        self.urlString = json["images"].arrayValue.first(where: { $0["height"].intValue == 512 })!["url"].stringValue
        self.animationUrlString = json["animation_url"].stringValue
    }
}

class Images: NSObject {
    @objc var url: String = ""
    @objc var width: Int = 0
    @objc var height: Int = 0
    
    convenience init(json: JSON) {
        self.init()
        self.url = json["url"].stringValue
        self.width = json["width"].intValue
        self.height = json["height"].intValue
    }
}

class AudioMessage: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var owmerId: Int = 0
    @objc dynamic var urlString = ""
    dynamic var waveForm = List<Int>()
    @objc dynamic var duration: Double = 0.0
    
    convenience init(json: JSON) {
        self.init()
        self.id = json["id"].intValue
        self.owmerId = json["owmer_id"].intValue
        for wave in json["waveform"].arrayValue {
            self.waveForm.append(wave.intValue)
        }
        self.urlString = json["link_mp3"].stringValue
        self.duration = json["duration"].doubleValue
    }
}
class Music: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var owmerId: Int = 0
    @objc dynamic var artist: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var urlString = ""
    @objc dynamic var duration: Double = 0.0
    
    convenience init(json: JSON) {
        self.init()
        self.id = json["id"].intValue
        self.owmerId = json["owmer_id"].intValue
        self.artist = json["artist"].stringValue
        self.title = json["title"].stringValue
        self.urlString = json["url"].stringValue
        self.duration = json["duration"].doubleValue
    }
}
